package com.condordoc.doctor;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.condordoc.doctor.facebook.FacebookHelper;
import com.condordoc.doctor.facebook.FacebookListener;
import com.condordoc.doctor.fcm.FCMHandler;
import com.condordoc.doctor.model.user.sigin.sigin.SignInModel;
import com.condordoc.doctor.model.user.siginup.SignUpModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
public class SignUpActivity extends BaseActivity implements FacebookListener {

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = SignUpActivity.class.getSimpleName();
    @BindView(R.id.input_firstname)
    EditText firstNameEditText;
    @BindView(R.id.input_lastname)
    EditText lastNameEditText;
    @BindView(R.id.input_email)
    EditText emailEditText;
    @BindView(R.id.input_layout_email)
    TextInputLayout input_layout_email;
    @BindView(R.id.input_layout_password)
    TextInputLayout input_layout_password;
    @BindView(R.id.input_password)
    EditText passwordEditTextView;
    @BindView(R.id.input_layout_mobile)
    TextInputLayout mobile_layout;
    @BindView(R.id.input_mobile)
    EditText phoneNumberEditText;
    @BindView(R.id.input_referal)
    EditText refCodeEditText;
    @BindView(R.id.spinner)
    Spinner countrycodeSpinner;
    @BindView(R.id.checkboxs)
    CheckBox checkBox;
    GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;
    private String countryCode;

    @Nullable
    @OnClick(R.id.login)
    void onlogin() {
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String emailId = emailEditText.getText().toString();
        String password = passwordEditTextView.getText().toString();
        String phoneNumber = phoneNumberEditText.getText().toString();
        String refCode = refCodeEditText.getText().toString();
        if (!isNetworkConnected()) {
            Toast.makeText( this, getString( R.string.no_internet_connected ) + "", Toast.LENGTH_SHORT ).show();
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateMobile()) {
            return;
        }
        String finalPhoneNumber = countryCode + "" + phoneNumber;
        CommonUtils.showProgressDialog( getApplicationContext() );
        findViewById( R.id.login ).setEnabled( false );
//        showProgress();
        ProgressDialog pd = new ProgressDialog( SignUpActivity.this );
        pd.show();
        pd.setMessage( "Loading..." );
        pd.setCancelable( false );
        apiManager.register( firstName, lastName, emailId, "doctor", password, finalPhoneNumber ).subscribeOn( Schedulers.io() ).observeOn( AndroidSchedulers.mainThread() ).subscribe( new SingleObserver<SignUpModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                pd.dismiss();

            }

            @Override
            public void onSuccess(SignUpModel signUpModel) {
                CommonUtils.disMissProgressDialog( getApplicationContext() );
                findViewById( R.id.login ).setEnabled( true );
                pd.dismiss();
                try {
                    if (signUpModel != null) {
                        if (signUpModel.getStatus().equalsIgnoreCase( "true" )) {
                            Log.d( TAG, signUpModel.getMessage() );
                            Intent in = new Intent( SignUpActivity.this, AccountVerificationActivity.class );
                            in.putExtra( AppConstant.PERSON_EMAIL, emailId );
                            in.putExtra( AppConstant.PERSON_PHONE, phoneNumber );
                            startActivity( in );
                            IntroActivity.introActivity.finish();
                            finish();
                        } else {
                            String message = signUpModel.getMessage();
                            String error = signUpModel.getError();
                            Toast.makeText( SignUpActivity.this, "" + error, Toast.LENGTH_SHORT ).show();
                        }
                    } else {
                        Toast.makeText( SignUpActivity.this, "Something went wrong", Toast.LENGTH_SHORT ).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                findViewById( R.id.login ).setEnabled( true );
                pd.dismiss();
                CommonUtils.disMissProgressDialog( getApplicationContext() );
                try {
                    Toast.makeText( SignUpActivity.this, "Something went wrong", Toast.LENGTH_SHORT ).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } );
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty( email ) && android.util.Patterns.EMAIL_ADDRESS.matcher( email ).matches();
    }

    private boolean validateEmail() {
        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail( email )) {
            input_layout_email.setError( getString( R.string.err_msg_email ) );
            requestFocus( emailEditText );
            return false;
        } else {
            input_layout_email.setErrorEnabled( false );
        }
        return true;
    }

    private boolean validatePassword() {
        if (passwordEditTextView.getText().toString().trim().isEmpty()) {
            input_layout_password.setError( getString( R.string.err_msg_password ) );
            requestFocus( passwordEditTextView );
            return false;
        } else {
            input_layout_password.setErrorEnabled( false );
        }
        return true;
    }

    private boolean validateMobile() {
        if (phoneNumberEditText.getText().toString().trim().isEmpty()) {
            phoneNumberEditText.setError( "Please enter valid Mobile Number" );
            requestFocus( phoneNumberEditText );
            return false;
        } else if (phoneNumberEditText.getText().toString().length() > 0) {
            if (phoneNumberEditText.getText().toString().length() < 10 || phoneNumberEditText.getText().toString().length() > 10) {
                phoneNumberEditText.setError( "Mobile number should have 10 digits" );
                return false;
            }
        } else {
            mobile_layout.setErrorEnabled( false );
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE );
        }
    }

    @Nullable
    @OnClick(R.id.ackn2)
    void onackn2() {
        //
    }

    @Nullable
    @OnClick(R.id.ackn)
    void onackn() {
        //
        if (checkBox.isChecked()) {
            checkBox.setChecked( false );
        } else {
            checkBox.setChecked( true );
        }
    }

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    @Nullable
    @OnClick(R.id.facebookLogin)
    void facebookLogin() {
        facebookHelper.performSignIn(this);


    }

    @Nullable
    @OnClick(R.id.googleSignUp)
    void googleLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult( signInIntent, RC_SIGN_IN );
    }

    FacebookHelper facebookHelper;
    ApiManager apiManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_sign_up );
        ButterKnife.bind( this );
        Retrofit retrofit = DoctorApp.retrofit( null );
        apiManager = retrofit.create( ApiManager.class );
        facebookHelper = new FacebookHelper( this );
        gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN )
                .requestEmail()
                .requestIdToken( getString( R.string.client_deep ) )
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient( this, gso );
        final ArrayAdapter adapter = ArrayAdapter.createFromResource( this, R.array.country_code, R.layout.itemlayoutforcountrycode );
        countrycodeSpinner.setAdapter( adapter );
        countrycodeSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = parent.getItemAtPosition( position ).toString();
                Log.d( TAG, "Country Code" + countryCode );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                countryCode = countrycodeSpinner.getSelectedItem().toString();
            }
        } );
        countryCode = countrycodeSpinner.getSelectedItem().toString();
        emailEditText.addTextChangedListener( new MyTextWatcher( emailEditText ) );
        passwordEditTextView.addTextChangedListener( new MyTextWatcher( passwordEditTextView ) );
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount( this );
        updateUI( account );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            facebookHelper.onActivityResult(requestCode, resultCode, data);

        } catch (Exception e) {

        }
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent( data );
            handleSignInResult( task );
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult( ApiException.class );
            // Signed in successfully, show authenticated UI.
            updateUI( account );

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w( TAG, "signInResult:failed code=" + e.getStatusCode() );
            updateUI( null );
        }
    }

    private void updateUI(GoogleSignInAccount account) {

        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
            Intent intent = new Intent( this, MainActivity.class );
            intent.putExtra( AppConstant.PERSON_NAME, personName );
            intent.putExtra( AppConstant.PERSON_GIVEN_NAME, personGivenName );
            intent.putExtra( AppConstant.PERSON_FAMILY, personFamilyName );
            intent.putExtra( AppConstant.PERSON_EMAIL, personEmail );
            intent.putExtra( AppConstant.PERSON_ID, personId );
            intent.putExtra( AppConstant.PERSON_URL, personPhoto.toString() );
            startActivity( intent );
            finish();
            String idToken = account.getIdToken();
            Log.d( TAG, "" + idToken );
            Log.e("token",""+account.getIdToken());
            apiManager.googleLogin( idToken, "google" )
                    .subscribeOn( Schedulers.io() )
                    .observeOn( AndroidSchedulers.mainThread() )
                    .subscribe( new SingleObserver<SignInModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(SignInModel signInModel) {
                            try {
                                if (signInModel != null) {
                                    if (signInModel.getStatus().equalsIgnoreCase( "true" )) {
                                        String personName = account.getDisplayName();
                                        String personGivenName = account.getGivenName();
                                        String personFamilyName = account.getFamilyName();
                                        String personEmail = account.getEmail();
                                        String personId = account.getId();
                                        Uri personPhoto = account.getPhotoUrl();
                                        Intent intent = new Intent( SignUpActivity.this, MainActivity.class );
                                        intent.putExtra( AppConstant.PERSON_NAME, personName );
                                        intent.putExtra( AppConstant.PERSON_GIVEN_NAME, personGivenName );
                                        intent.putExtra( AppConstant.PERSON_FAMILY, personFamilyName );
                                        intent.putExtra( AppConstant.PERSON_EMAIL, personEmail );
                                        intent.putExtra( AppConstant.PERSON_ID, personId );
                                        intent.putExtra( AppConstant.PERSON_URL, personPhoto.toString() );
                                        FCMHandler fcmHandler = new FCMHandler();
                                        fcmHandler.enableFCM();
                                        Log.d( TAG, signInModel.getData().getOriginal().getAccessToken() );
                                        AppPreferences.newInstance().setToken( SignUpActivity.this, signInModel.getData().getOriginal().getAccessToken() );
                                        Toast.makeText( getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT ).show();
                                        startActivity( intent );
                                        try {
                                            IntroActivity.introActivity.finish();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        finish();
                                    } else {
                                        Toast.makeText( SignUpActivity.this, "" + signInModel.getError(), Toast.LENGTH_SHORT ).show();
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText( SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT ).show();
                            Log.e( TAG, "onError: " + e.getMessage() );
                        }
                    } );
        }
    }

    @Override
    public void onFbSignInFail(String errorMessage)
    {
        Log.e("error",errorMessage);
    }

    @Override
    public void onFbSignInSuccess(String id, String name, String email, String phone, String imgUrl, String token) {
        // No check verification code
//        Intent in = new Intent(this, MainActivity.class);
//        startActivity(in);
        Log.e( TAG, "onFbSignInSuccess: token" + token );
        apiManager.facebookLogin( token, "doctor" )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<SignInModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(SignInModel signInModel) {
                        try {
                            if (signInModel != null) {
                                if (signInModel.getStatus().equalsIgnoreCase( "true" )) {
                                    Log.e( TAG, "onFbSignInSuccess: " + name + " " );
                                    Intent intent = new Intent( SignUpActivity.this, MainActivity.class );
                                    intent.putExtra( AppConstant.PERSON_NAME, name );
                                    //        intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
                                    //        intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
                                    intent.putExtra( AppConstant.PERSON_EMAIL, email );
                                    intent.putExtra( AppConstant.PERSON_ID, id );
                                    intent.putExtra( AppConstant.PERSON_URL, imgUrl );
                                    startActivity( intent );
                                    Log.d( "Facebook Token Doctor  ", "" + token );
                                    FCMHandler fcmHandler = new FCMHandler();
                                    fcmHandler.enableFCM();
                                    AppPreferences.newInstance().setLoggedIn( SignUpActivity.this, true );
                                    Log.d( TAG, signInModel.getData().getOriginal().getAccessToken() );
                                    AppPreferences.newInstance().setToken( SignUpActivity.this, signInModel.getData().getOriginal().getAccessToken() );
                                    Toast toast=Toast.makeText( getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SignUpActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                    startActivity( intent );
                                    try {
                                        IntroActivity.introActivity.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                } else {
                                    Toast toast=Toast.makeText( SignUpActivity.this, "" + signInModel.getError(), Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SignUpActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }
                        } catch (Exception e) {
                            Toast toast=Toast.makeText( SignUpActivity.this, "Something went wrong.", Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SignUpActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                } );
    }

    @Override
    public void onFBSignOut() {
    }
}