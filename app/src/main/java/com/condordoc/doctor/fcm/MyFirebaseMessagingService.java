package com.condordoc.doctor.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.condordoc.doctor.DoctorChatActivity;
import com.condordoc.doctor.OngoingActivity;
import com.condordoc.doctor.YourJobsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.condordoc.doctor.DialogJobsActivity;
import com.condordoc.doctor.MainActivity;
import com.condordoc.doctor.R;
import com.condordoc.doctor.event.MessageEvent;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;

import org.greenrobot.eventbus.EventBus;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";


    Handler handler = new Handler(Looper.getMainLooper());
    private RemoteMessage.Notification notification;
    String jobId = "0";

    /*
    *
    * {
  "to":"some_device_token",
  "content_available": true,
  "notification": {
      "title": "hello",
      "body": "test message",
      "click_action": "OPEN_ACTIVITY_1"
  },
  "data": {
      "extra":"juice"
  }
}
    * */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG,"natificatin data"+remoteMessage.getData());
        try {
             if(AppPreferences.newInstance().isLoggedIn(this)) {
                 if (remoteMessage.getNotification() != null) {
                     Log.d(TAG, "From: " + remoteMessage.getFrom());
                     Log.d(TAG, "title: " + remoteMessage.getNotification().getTitle());
                     Log.d(TAG, "action: " + remoteMessage.getNotification().getClickAction());
                     Log.d(TAG, "body: " + remoteMessage.getNotification().getBody());
                     Log.d(TAG, "Hash: " + remoteMessage.getData().toString());



                     if (remoteMessage.getData().containsKey("action") &&  remoteMessage.getData().get("action").equalsIgnoreCase("New Message")) {
                         try {

                             EventBus.getDefault().post(new MessageEvent(remoteMessage));
                         } catch (Exception e) {

                         }
                     }
                     if (AppPreferences.newInstance().isDoctorOnline(this)) {
                         handler.post(new Runnable() {
                             @Override
                             public void run() {
                                 try {
                                     if (remoteMessage.getData() != null) {
                                         jobId = remoteMessage.getData().get("jobId");
                                         Log.e("jobid",jobId);
                                         AppPreferences.newInstance().setJobNotificationPop(MyFirebaseMessagingService.this, jobId);
                                     } else {
                                         jobId = "0";
                                     }
                                 } catch (Exception e) {
                                     e.printStackTrace();
                                     jobId = "0";
                                 }

                                 if (jobId != null && Integer.valueOf(jobId) > 0) {
                                     Intent intent = new Intent();
                                     intent.setClassName(getPackageName(), "com.condordoc.doctor.DialogJobsActivity");
//                                     Intent intent = new Intent(MyFirebaseMessagingService.this, "com.condordoc.doctor.DialogJobsActivity");
                                     intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                     intent.putExtra(AppConstant.START_JOB_ID, jobId);
                                     startActivity(intent);
                                 } else {
                                     try {
                                         if (remoteMessage.getNotification() != null) {
                                             String title = remoteMessage.getNotification().getTitle() != null ? remoteMessage.getNotification().getTitle() : "Push Notification";
                                             String message = remoteMessage.getNotification().getBody() != null ? remoteMessage.getNotification().getBody() : "";
                                             sendNotification(title, message);
                                         }
                                     } catch (Exception e) {

                                     }

                                 }
                             }
                         });
                     }

                     Log.d(TAG, "Message data payload: " + remoteMessage.getNotification().getTitle());

                     if (/* Check if data needs to be processed by long running job */ false) {
                         // For long-running tasks (10 seconds or more) use WorkManager.
                         scheduleJob();
                     } else {
                         // Handle message within 10 seconds
                         handleNow(remoteMessage);
                     }

                 }

                 if (remoteMessage.getNotification() != null) {
                     Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
                 }
             }else {
                 notificationLoginActivity();
             }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void notificationLoginActivity() {
        Intent intent = new Intent(this, DoctorChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("Login Activity")
                        .setContentText("Notification")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        AppPreferences.newInstance().setFirebaseToken(this, token);
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
//                .build();
//        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow(RemoteMessage remoteMessage) {
        Log.d(TAG, "Short lived task is done.");
        try {
            if (remoteMessage.getNotification() != null) {
                String title = remoteMessage.getNotification().getTitle() != null ? remoteMessage.getNotification().getTitle() : "Push Notification";
                String message = remoteMessage.getNotification().getBody() != null ? remoteMessage.getNotification().getBody().replace("+"," ") : "";
                sendNotification(title, message);
            }
        }catch (Exception e){

        }
    }
    
    private void sendNotification(String title,String messageBody) {
        title = title.replace("+", " ");
        messageBody = messageBody.replace("+", " ");
        Intent intent;
     if(title.equalsIgnoreCase("New Appointment"))
           {
                 intent= new Intent(this, YourJobsActivity.class);
           }
            else if (title.equalsIgnoreCase("Appointment Started")){
                intent = new Intent(this, OngoingActivity.class);
            }
           else if (title.equalsIgnoreCase("New Message")){
               intent = new Intent(this, ChatBlank.class);
               
               intent.putExtra(AppConstant.START_JOB_ID,jobId);
           }
            else {
                intent = new Intent(this, MainActivity.class);
            }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("" + title)
                        .setContentText("" + messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }



}