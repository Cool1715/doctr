package com.condordoc.doctor.fcm;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.DoctorApp;
import com.condordoc.doctor.DoctorChatActivity;
import com.condordoc.doctor.R;
import com.condordoc.doctor.SpecificJobActivity;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
public class ChatBlank extends LocalizationActivity {
    int doctorid;
    ApiManager apiManager;
    JobByIdModel jobByIdModel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_chat_blank);
        doctorid=Math.abs(Integer.parseInt(getIntent().getStringExtra(AppConstant.START_JOB_ID)));
        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this));
        apiManager = retrofit.create(ApiManager.class);

        apiManager.getJobs()
                .subscribeOn(Schedulers.io())
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.START_DOCTOR
                                    || dataItem.getStatus() == AppConstant.DOCTOR_ARRIVED
                                    || dataItem.getStatus() == AppConstant.DOCTOR_JOB_COMPLETED)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        CommonUtils.disMissProgressDialog( getBaseContext());
                        try {
                            Log.e("vallll", "" + dataItems.toString());
                            for (DataItem data:dataItems) {
                                if(data.getPatientId()==doctorid)
                                {
                                    Intent intent = new Intent( ChatBlank.this, DoctorChatActivity.class);
                                    intent.putExtra(AppConstant.START_JOB_ID, data.getId()+ "");
                                    startActivity(intent);
                                    finish();
                                    
                                }
                                
                            }
                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent( getApplicationContext(), SpecificJobActivity.class );
        startActivity( intent );
    }
}