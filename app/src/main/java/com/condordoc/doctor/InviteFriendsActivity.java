package com.condordoc.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.utils.AppPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InviteFriendsActivity extends LocalizationActivity {

    @BindView(R.id.refer_token)
    TextView refer_token;

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @Nullable
    @OnClick(R.id.invite)
    void oninvite() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out my app at: https://play.google.com/store/apps/details?id=" + getPackageName() +"\nReferral code : "
                        + AppPreferences.newInstance().getReferralToken(this));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        ButterKnife.bind(this);

        refer_token.setText(AppPreferences.newInstance().getReferralToken(this));

    }
}

