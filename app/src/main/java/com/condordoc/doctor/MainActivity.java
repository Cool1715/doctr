package com.condordoc.doctor;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.ResultReceiver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.condordoc.doctor.others.CommonUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.facebook.FacebookHelper;
import com.condordoc.doctor.facebook.FacebookListener;
import com.condordoc.doctor.fcm.FCMHandler;
import com.condordoc.doctor.model.fcm.FCMTokenModel;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.model.job.JobCountModel;
import com.condordoc.doctor.model.location.SetLocationModel;
import com.condordoc.doctor.model.user.logout.LogoutModel;
import com.condordoc.doctor.model.user.profile.Data;
import com.condordoc.doctor.model.user.profile.MyProfileModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.services.LocationService;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.GetAddressUitls;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import pl.kitek.timertextview.TimerTextView;
import retrofit2.Retrofit;


public class MainActivity extends BaseActivity implements FacebookListener {

    private static final String TAG = "mainactivity";
    boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.switch1)
    Switch switchoffonline;

    @BindView(R.id.nav_profilename)
    TextView nav_profileName;


    @OnClick(R.id.settings)
    void onSettings(){
        Intent intent = new Intent(this, JobStatisticsActivity.class);
        startActivity(intent);
    }

    @BindView(R.id.gooffon)
    TextView gooffon;

    @BindView(R.id.profilename)
    TextView profileName;

    @BindView(R.id.wallet)
    TextView wallet;
    @BindView(R.id.showText)
    LinearLayout showErr;
    @BindView(R.id.verifyDOCActi)
    LinearLayout verifyDOCActi;
    @BindView(R.id.dp_user)
    CircleImageView dpUser;

    @BindView(R.id.nav_dp_user)
    CircleImageView navdpUser;

    @BindView(R.id.pendingJobs)
    TextView pendingJobs;

    @BindView(R.id.upcomingJobs)
    TextView upcomingJobs;
    private String addressOutput;
    private Location lastKnownLocation;

    @Nullable
    @OnClick(R.id.navhead)
    void onnavhead() {
        //
    }

    @BindView(R.id.currentLocationDoctor)
    TextView currentLocation;

    TextView update_radius;
    ApiManager apiManager;
    FacebookHelper facebookHelper;
    private GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;
    Geocoder geocoder;
    List<Address> addresses;
    private FusedLocationProviderClient fusedLocationClient;


    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultData == null) {
                return;
            }

            // Display the address string
            // or an error message sent from the intent service.
            addressOutput = resultData.getString(AppConstant.RESULT_DATA_KEY);
            if (addressOutput == null) {
                addressOutput = "";
            }

            // Show a toast message if an address was found.
            if (resultCode == AppConstant.SUCCESS_RESULT) {
                Toast toast=Toast.makeText(MainActivity.this, "Address" + addressOutput, Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
                toast.show();

            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);


        getCurrentLocation();

        ImageView update_location = findViewById(R.id.update_location);
        update_radius = findViewById(R.id.update_radius);
        update_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRadius();
            }
        });

        facebookHelper = new FacebookHelper(this);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        mGoogleSignInClient = GoogleSignIn.getClient(MainActivity.this, gso);
        AppPreferences.newInstance().setDoctorOnline(MainActivity.this, true);
        gooffon.setText("GO ONLINE");

        switchoffonline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gooffon.setText("GO ONLINE");
                    ProgressDialog pd=new ProgressDialog(MainActivity.this);
                    pd.show();
                    pd.setMessage("Loading...");
                    pd.setCancelable(false);

                    String doctor_lat = AppPreferences.newInstance().getCurrentLocationLat(MainActivity.this);
                    String doctor_long = AppPreferences.newInstance().getCurrentLocationLong(MainActivity.this);

                    AppPreferences.newInstance().setDoctorOnline(MainActivity.this, true);

                    apiManager.setLocation("" + doctor_lat, "" + doctor_long).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<SetLocationModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            pd.dismiss();

                        }
                        @Override
                        public void onSuccess(SetLocationModel setLocationModel) {
                            pd.dismiss();
                            try {
                                if (setLocationModel != null && setLocationModel.getStatus().equalsIgnoreCase("true")) {
                                    Log.d(TAG, "onSuccess: " + setLocationModel.getMessage());

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
//                                                showuserrequestdialog();
                                           Toast toast= Toast.makeText(MainActivity.this, "Push Notification dialog, When job created patient then will appear dialog", Toast.LENGTH_LONG);
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
                                            toast.show();
                                        }
                                    }, 2000);
                                }
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            pd.dismiss();
                        }
                    });
                } else {
                    AppPreferences.newInstance().setDoctorOnline(MainActivity.this, false);
                    gooffon.setText("Go OFFLINE");
                }
            }
        });
        setListeneronMenu();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.containsKey(AppConstant.PERSON_NAME)) {
            String name = bundle.getString(AppConstant.PERSON_NAME);
            profileName.setText(name);
        }
        if (bundle != null && bundle.containsKey(AppConstant.PERSON_EMAIL)) {
            String person_emailID = bundle.getString(AppConstant.PERSON_EMAIL);
        }
        if (bundle != null && bundle.containsKey(AppConstant.PERSON_URL)) {
            String uriPhoto = bundle.getString(AppConstant.PERSON_URL);
            Picasso.with(this).load(uriPhoto).into(dpUser);
            Picasso.with(this).load(uriPhoto).into(navdpUser);

        }
        if (!isNetworkConnected()) {
            Toast toast=Toast.makeText(this, getString(R.string.no_internet_connected) + "", Toast.LENGTH_SHORT);
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
            toast.show();
            return;
        }




        if (TextUtils.isEmpty(AppPreferences.newInstance().getFirebaseToken(this)+"")) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "getInstanceId failed", task.getException());
                                return;
                            }

                            // Get new Instance ID token
                            if (task.getResult() != null) {
                                String token = task.getResult().getToken();
                                AppPreferences.newInstance().setFirebaseToken(MainActivity.this, token);
                            }

                            // Log and toast
                        }
                    });
        }



    }

    private void getCurrentLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        Log.i("Current Location",location.toString());
                        if (location != null) {
                            double latitude = location.getLatitude();
                            double longitude = location.getLongitude();
                            // LatLng latLng=new LatLng(latitude,longitude);
                            String lat= String.valueOf(latitude);
                            String lon= String.valueOf(longitude);
                            SharedPreferences sharedPreferences=getSharedPreferences("LatLon", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor=sharedPreferences.edit();
                            editor.putString("lat",lat);
                            editor.putString("lon",lon);
                            editor.apply();
                            editor.commit();
                        }
                    }
                });
    }



    @Override
    public void onResume() {
        super.onResume();
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.myProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<MyProfileModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(MyProfileModel myProfileModel) {
                CommonUtils.disMissProgressDialog(getBaseContext());
                try {
                    if (myProfileModel != null && myProfileModel.getStatus().equalsIgnoreCase("true")) {

                        Data data = myProfileModel.getData();
                        AppPreferences.newInstance().setUserId(MainActivity.this, data.getId());
                        Log.e(TAG, ""+myProfileModel.getData());

                        AppPreferences.newInstance().setUserId(MainActivity.this,data.getId());
                        profileName.setText(String.format("%s %s", data.getFirstName(), data.getLastName()));
                        wallet.setText(String.format("Wallet Balance: $%s", myProfileModel.getData().getBalance()));
                        nav_profileName.setText(String.format("%s %s", data.getFirstName(), data.getLastName()));
                        if(data.getVerified())
                        {
                            showErr.setVisibility(View.GONE);
                            verifyDOCActi.setVisibility(View.VISIBLE);
                        }
                        if (data.getReferToken() != null)
                            AppPreferences.newInstance().setReferralToken(MainActivity.this, data.getReferToken());

                        Glide.with(MainActivity.this).load(APIURL.IMAGE_USER_URL + myProfileModel.getData().getImage()).error(R.drawable.user).into(dpUser);
                        Glide.with(MainActivity.this).load(APIURL.IMAGE_USER_URL + myProfileModel.getData().getImage()).error(R.drawable.user).into(navdpUser);
                          } else {
                        if (myProfileModel != null && myProfileModel.getError() != null) {
                            Toast toast=Toast.makeText(MainActivity.this, "" + myProfileModel.getError(), Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(Throwable e) {
                CommonUtils.disMissProgressDialog(getBaseContext());
                Log.d(TAG, e.getMessage());
//                            Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        apiManager.getJobs().subscribeOn(Schedulers.io()).map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            if (jobGetModel.getStatus().equalsIgnoreCase("true")) return jobGetModel.getData();
            return new ArrayList<>();
        }).flatMapIterable(dataItems -> dataItems).filter(dataItem -> {
            Log.i(TAG, String.valueOf(dataItem.getDoctorId()));
//                    return dataItem.getDoctorId() == AppPreferences.newInstance().getUserId(MainActivity.this);
            return dataItem.getStatus() == AppConstant.PENDING_JOB || dataItem.getStatus() == AppConstant.ACCEPT_DOCTOR || dataItem.getStatus() == AppConstant.START_DOCTOR;
        }).toList().map(dataItemList -> {
            int pendingCount = 0;
            int upcomingJob = 0;
            for (DataItem item : dataItemList) {
                if (item.getStatus() == AppConstant.PENDING_JOB) {
                    pendingCount++;
                }

                if (item.getStatus() == AppConstant.ACCEPT_DOCTOR) {
                    upcomingJob++;
                }
            }
            return new JobCountModel(pendingCount, upcomingJob);
        }).subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<JobCountModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(JobCountModel dataItems) {
                try {
                    pendingJobs.setText(String.format("%s", dataItems.getPendingJob()));
                    upcomingJobs.setText(String.format("%s", dataItems.getUpcomingJob()));
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
            }
        });


        sendRegistrationToServer(AppPreferences.newInstance().getFirebaseToken(this));

    }

    private void sendRegistrationToServer(String token) {
        apiManager.sendFCMToekn(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<FCMTokenModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(FCMTokenModel s) {
                try {
                    Log.d(TAG, "onSuccess: " + s.toString());
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
            }
        });

        Log.d( "Firebase Token: ", AppPreferences.newInstance().getFirebaseToken(this)+"");
        Log.d(TAG, "Server: " + AppPreferences.newInstance().getToken(this)+"");

    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            Intent intent = new Intent(this, LocationService.class);
            ContextCompat.startForegroundService(this, intent);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "remove location service");
                    stopService(intent);
                }
            }, 1000 * 30);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(AppPreferences.newInstance().getCurrentLocationLat(this))) {


            Single.just(GetAddressUitls.getCompleteAddressString(this, Double.valueOf(AppPreferences.newInstance().getCurrentLocationLat(this)), Double.valueOf(AppPreferences.newInstance().getCurrentLocationLong(this)))).delay(10000, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<String>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onSuccess(String s) {
                    try {
                        currentLocation.setText(s);
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onError(Throwable e) {
                    currentLocation.setText("No Address");
                }
            });

        }
    }


    public void updateRadius() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.update_location_radius);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        final EditText input_radius = dialog.findViewById(R.id.input_radius);

        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button save = dialog.findViewById(R.id.ok);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final String radius = input_radius.getText().toString().trim();
                    update_radius.setText("Within " + radius + " km Work Radius");
                    AppPreferences.newInstance().setDoctorRadius(MainActivity.this, Float.valueOf(radius));
                } catch (Exception e) {

                }

                dialog.dismiss();

            }

        });


    }


    // TODO
    private void showuserrequestdialog() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogforjobrequest);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        long futureTimestamp = System.currentTimeMillis() + (37000);
        TimerTextView timerText = (TimerTextView) dialog.findViewById(R.id.timerText);
        timerText.setEndTime(futureTimestamp);

        TextView cancel = (TextView) dialog.findViewById(R.id.decline);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.accept);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }

        });


    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    private void setListeneronMenu() {

        findViewById(R.id.menu_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
//                }
            }
        });

        findViewById(R.id.homell).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {

//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
//                }
            }
        });
        findViewById(R.id.onmyavailabiltyll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, MyAvailabilityActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onmanageservicesll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, ManageServicesActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onmydocumentsll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, UploadDocumentActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.ongoingll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, OngoingActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onprofilell).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, MyProfileActivity.class);
                startActivity(in);
//                }
            }
        });

        findViewById(R.id.bankfee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, BankDetailActivity.class);
                startActivity(in);
//                }
            }
        });

        findViewById(R.id.onpaymentll).setVisibility(View.GONE);
        findViewById(R.id.onpaymentll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.DoctorFee).setVisibility(View.GONE);
        findViewById(R.id.DoctorFee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, CollectPaymentActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onemergencyll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, EmergencyContactActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.oninvitefriendsll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, InviteFriendsActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onsupportll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, SupportActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onyourjobsll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, YourJobsActivity.class);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onyourjobsll2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // TODO
                Intent in = new Intent(MainActivity.this, YourJobsActivity.class);
                in.putExtra(AppConstant.JOB_TYPE, 1);
                startActivity(in);
//                }
            }
        });
        findViewById(R.id.onyourupcoming).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, YourJobsActivity.class);
                in.putExtra(AppConstant.JOB_TYPE, 2);
                startActivity(in);

            }
        });

        findViewById(R.id.onwalletll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                Intent in = new Intent(MainActivity.this, WalletActivity.class);
                startActivity(in);
//                }
            }
        });

        findViewById(R.id.onRating).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//                if(lang.equals("ar")){
//                    drawer.openDrawer(Gravity.RIGHT);
//                }
//                else {
//                apiManager.execution_method_get_jsonarray(HOME,APIURL.Home);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }

                Intent in = new Intent(MainActivity.this, Rating.class);
                startActivity(in);
//                }
            }
        });

        findViewById(R.id.onlogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (AppPreferences.newInstance().getLanguage(MainActivity.this).equalsIgnoreCase(AppConstant.ARBIC)) {
                    drawer.openDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                showlogoutdialog();
//                }
            }
        });


    }

    private void showlogoutdialog() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogforlogout);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById(R.id.no);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.yes);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try {
                    signOut(); // Google SignOut

                    FCMHandler fcmHandler = new FCMHandler();
                    fcmHandler.disableFCM();


                    AppPreferences.newInstance().setToken(MainActivity.this, "");
                    AppPreferences.newInstance().setLoggedIn(MainActivity.this,false);
                    facebookHelper.performSignOut(); // Facebook logout
                    AppPreferences.newInstance().clear(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                logout(); // Custom server logout
                Intent in = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(in);
                finish();


            }

        });

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast toast=Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT);
        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
        toast.show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void logout() {
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.logout().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<LogoutModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(LogoutModel logoutModel) {
                CommonUtils.disMissProgressDialog(getBaseContext());
                try {
                    Log.d(TAG, logoutModel.getMessage());
                    AppPreferences.newInstance().setToken(MainActivity.this, "");
                    Toast  toast=Toast.makeText(MainActivity.this, logoutModel.getMessage(), Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
                    toast.show();
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                CommonUtils.disMissProgressDialog(getBaseContext());
                AppPreferences.newInstance().setToken(MainActivity.this, "");
            }
        });
    }

    private void signOut() {
        try {
            mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Intent in = new Intent(MainActivity.this, SplashActivity.class);
                    startActivity(in);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Intent in = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(in);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        facebookHelper.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFbSignInFail(String errorMessage) {

    }

    @Override
    public void onFbSignInSuccess(String id, String name, String email, String phone, String imgUrl, String token) {

    }

    @Override
    public void onFBSignOut() {
        Toast toast=Toast.makeText(this, "Logout!", Toast.LENGTH_SHORT);
        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MainActivity.this, android.R.color.holo_red_dark));
        toast.show();

    }
}
