package com.condordoc.doctor.viewmodel;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.condordoc.doctor.R;
public class OfficeLocation extends AppCompatActivity {

    Button OfficeAddress_up_to_Date;
    EditText OfficeAddress_postal, OfficeAddress_country, OfficeAddress_street, OfficeAddress_location;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_office_location );
        findViewById( R.id.OfficeAddress_back ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );
        OfficeAddress_location = findViewById( R.id.OfficeAddress_location );
        OfficeAddress_street = findViewById( R.id.OfficeAddress_street );
        OfficeAddress_country = findViewById( R.id.OfficeAddress_country );
        OfficeAddress_postal = findViewById( R.id.OfficeAddress_postal );
        OfficeAddress_up_to_Date = findViewById( R.id.OfficeAddress_up_to_Date );
        OfficeAddress_up_to_Date.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OfficeAddress_location.getText().toString().isEmpty()) {
                    OfficeAddress_location.setError( "Please provide Address" );
                } else if (OfficeAddress_street.getText().toString().isEmpty()) {
                    OfficeAddress_street.setError( "Please provide Street Number" );
                } else if (OfficeAddress_country.getText().toString().isEmpty()) {
                    OfficeAddress_country.setError( "Please provide Country name" );
                } else if (OfficeAddress_postal.getText().toString().isEmpty()) {
                    OfficeAddress_postal.setError( "Please provide Postal code" );
                } else {
                    String address = OfficeAddress_location.getText().toString();
                    String street = OfficeAddress_street.getText().toString();
                    String country = OfficeAddress_country.getText().toString();
                    String postal_code = OfficeAddress_postal.getText().toString();
                    sharedPreferences = getSharedPreferences( "OFFICE_ADDRESS", Context.MODE_PRIVATE );
                    editor = sharedPreferences.edit();
                    editor.putString( "address", address );
                    editor.putString( "street", street );
                    editor.putString( "country", country );
                    editor.putString( "postal_code", postal_code );
                    editor.apply();
                    Toast.makeText( OfficeLocation.this, "Address has been updated", Toast.LENGTH_SHORT ).show();
                }
            }
        } );
        try {
            sharedPreferences = getSharedPreferences( "OFFICE_ADDRESS", Context.MODE_PRIVATE );
            String address = sharedPreferences.getString( "address", "" );
            String street = sharedPreferences.getString( "street", "" );
            String country = sharedPreferences.getString( "country", "" );
            String postal_code = sharedPreferences.getString( "postal_code", "" );
            OfficeAddress_location.setText( address );
            OfficeAddress_street.setText( street );
            OfficeAddress_country.setText( country );
            OfficeAddress_postal.setText( postal_code );

        } catch (Exception e) {
        }
    }
}
