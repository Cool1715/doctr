package com.condordoc.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.MyAvailabilityGridAdapter;
import com.condordoc.doctor.utils.AppConstant;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAvailabilityActivity extends LocalizationActivity {

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @BindView(R.id.simpleGridView)
    GridView gridView;

    String days[]={"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
    String abc2[]={"0","0","0","0","0","0","0"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_availability);
        ButterKnife.bind(this);

        final MyAvailabilityGridAdapter customAdapter = new MyAvailabilityGridAdapter(getApplicationContext(), days,abc2);
        gridView.setAdapter(customAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent(MyAvailabilityActivity.this, SetTimeAvailabilityActivity.class);
                in.putExtra(AppConstant.DAY, days[position]);
                startActivity(in);
            }
        });
    }
}
