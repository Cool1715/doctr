package com.condordoc.doctor;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.ManageServiceAdapter;
import com.condordoc.doctor.model.manage.ManageService;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ManageServicesActivity extends LocalizationActivity {

    private ManageServiceAdapter topDownAdapter;
    public static ManageServicesActivity manageServicesActivity;

    @BindView(R.id.main_recycler_view)
    RecyclerView ManageServiceRecyclerView;
    private ApiManager apiManager;

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_services);
        ButterKnife.bind(this);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);
        manageServicesActivity = ManageServicesActivity.this;

        ManageServiceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        topDownAdapter = new ManageServiceAdapter(new ArrayList<>());
        ManageServiceRecyclerView.setAdapter(topDownAdapter);
        ProgressDialogUtils progressDialogUtils =   ProgressDialogUtils.newInstance(ManageServicesActivity.this);
        progressDialogUtils.showDialog("");
        apiManager.getManageService()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ManageService>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(ManageService manageService) {
                        progressDialogUtils.hideDialog();
                        try {
                            if (manageService != null && manageService.getStatus().equalsIgnoreCase("true")) {
                                topDownAdapter.addItem(manageService.getData());
                            }
                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialogUtils.hideDialog();
                    }
                });
    }
}
