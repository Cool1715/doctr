package com.condordoc.doctor.facebook;

public interface FacebookListener {
  void onFbSignInFail(String errorMessage);

  void onFbSignInSuccess(String id, String name, String email, String phone, String imgUrl, String token);

  void onFBSignOut();
}
