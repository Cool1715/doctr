package com.condordoc.doctor.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class FacebookHelper {
  private FacebookListener mListener;
  private CallbackManager mCallBackManager;

  public FacebookHelper(@NonNull FacebookListener facebookListener) {
    mListener = facebookListener;
    mCallBackManager = CallbackManager.Factory.create();
    FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
      @Override public void onSuccess(LoginResult loginResult) {

        Log.e("idfb",""+loginResult.getAccessToken().getUserId());

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                  @Override
                  public void onCompleted(JSONObject object, GraphResponse response) {
                    // Application code
                    try {
                      Log.i("DoctorChatModel",response.toString());

                      String email = response.getJSONObject().getString("email");
                      String name = response.getJSONObject().getString("name");
                      String id = response.getJSONObject().getString("id");

//                      Profile profile = Profile.getCurrentProfile();
//                      String id = profile.getId();
//                      String name = profile.getName();
                     String imgUrl = "https://graph.facebook.com/" + id + "/picture?type=large";



                      mListener.onFbSignInSuccess(id,
                              name,email,"0",imgUrl, loginResult.getAccessToken().getToken());

                      Log.i("API_AUTH_LOGIN" + "Email", "");
                      Log.i("API_AUTH_LOGIN"+ "FirstName", name);
                      Log.i("API_AUTH_LOGIN" + "Id", id);



                    } catch (JSONException e) {
                      e.printStackTrace();
                      Log.e("errorfb",""+e);
                    }
                  }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,link");
        request.setParameters(parameters);
        request.executeAsync();

      }

      @Override public void onCancel() {
        mListener.onFbSignInFail("User cancelled operation");
      }

      @Override public void onError(FacebookException e) {
        mListener.onFbSignInFail(e.getMessage());
      }
    };
    LoginManager.getInstance().registerCallback(mCallBackManager, mCallBack);
  }

  @NonNull @CheckResult
  public CallbackManager getCallbackManager() {
    return mCallBackManager;
  }

  public void performSignIn(Activity activity) {
    LoginManager.getInstance()
        .logInWithReadPermissions(activity,
            Arrays.asList("public_profile", "email"));
  }

  public void performSignIn(Fragment fragment) {
    LoginManager.getInstance()
        .logInWithReadPermissions(fragment,
            Arrays.asList("public_profile", "email"));
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    mCallBackManager.onActivityResult(requestCode, resultCode, data);
  }

  public void performSignOut() {
    LoginManager.getInstance().logOut();
    mListener.onFBSignOut();
  }
}
