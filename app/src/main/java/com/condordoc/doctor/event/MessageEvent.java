package com.condordoc.doctor.event;

import android.os.Bundle;

import com.google.firebase.messaging.RemoteMessage;

public class MessageEvent {

    private RemoteMessage remoteMessage;

    public MessageEvent(){
        this.remoteMessage = new RemoteMessage(new Bundle());
    }

    public MessageEvent(RemoteMessage remoteMessage){

        this.remoteMessage = remoteMessage;
    }

    public RemoteMessage getRemoteMessage() {
        return remoteMessage;
    }

    public void setRemoteMessage(RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }
}
