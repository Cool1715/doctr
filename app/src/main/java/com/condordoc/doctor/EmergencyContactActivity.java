package com.condordoc.doctor;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.EmergencyAdapter;
import com.condordoc.doctor.adapter.OnEmergencyTakeActionListener;
import com.condordoc.doctor.model.emergency.CreateEmergencyModel;
import com.condordoc.doctor.model.emergency.DataItem;
import com.condordoc.doctor.model.emergency.DeleteEmergencyModel;
import com.condordoc.doctor.model.emergency.GetEmergencyModel;
import com.condordoc.doctor.model.emergency.UpdateEmergencyModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ValidateInputs;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static com.condordoc.doctor.BaseActivity.hideKeyboard;
public class EmergencyContactActivity extends LocalizationActivity {

    private ApiManager apiManager;

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @Nullable
    @OnClick(R.id.addcontacts)
    void onaddcontacts() {
        showaddcontactdialog();
    }

    private void showaddcontactdialog() {
        final Dialog dialog = new Dialog( this );
        // Include dialog.xml file
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        Window window2 = dialog.getWindow();
        dialog.setCancelable( true );
        window2.setGravity( Gravity.CENTER );
        window2.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.setContentView( R.layout.dialogforaddcontact );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        dialog.show();
        TextInputLayout inputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_refralcode );
        TextInputLayout phoneInputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_cpass );
        TextInputLayout emailInputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_email );
        EditText input_name = (EditText) dialog.findViewById( R.id.input_name );
        EditText input_email = (EditText) dialog.findViewById( R.id.input_email );
        EditText input_phone = (EditText) dialog.findViewById( R.id.input_contactno );
        TextView cancel = (TextView) dialog.findViewById( R.id.cancel );
        cancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard( EmergencyContactActivity.this );
                dialog.dismiss();
            }
        } );
        TextView save = (TextView) dialog.findViewById( R.id.viewongng );
        save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard( EmergencyContactActivity.this );
                boolean isValid = validateForm( input_name, inputLayout, input_phone, phoneInputLayout, input_email, emailInputLayout );
                if (isValid) {
                    int user_id = AppPreferences.newInstance().getUserId( EmergencyContactActivity.this );
                    String name = input_name.getText().toString().trim();
                    String phone = input_phone.getText().toString().trim();
                    CommonUtils.showProgressDialog( getBaseContext() );
                    apiManager.processEmergency( user_id, name, "" + input_email.getText().toString(), phone )
                            .subscribeOn( Schedulers.io() )
                            .observeOn( AndroidSchedulers.mainThread() )
                            .subscribe( new SingleObserver<CreateEmergencyModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onSuccess(CreateEmergencyModel model) {
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                    try {
                                        if (model != null && model.getStatus().equalsIgnoreCase( "true" )) {
                                            DataItem dataItem = new DataItem();
                                            dataItem.setName( "" + input_name.getText().toString() );
                                            dataItem.setContact( "" + input_phone.getText().toString() );
                                            topDownAdapter.addItem( dataItem );
                                            Toast toast =Toast.makeText( EmergencyContactActivity.this, "" + model.getMessage(), Toast.LENGTH_SHORT );
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                            toast.show();
                                            dialog.dismiss();

                                        } else {
                                            if (model != null && model.getError() != null) {
                                                Toast toast = Toast.makeText(EmergencyContactActivity.this, "" + model.getError(), Toast.LENGTH_SHORT);
                                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                                toast.show();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                    dialog.dismiss();
                                    Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                    toast.show();

                                }
                            } );

                }

            }

        } );
    }

    private EmergencyAdapter topDownAdapter;
    @BindView(R.id.main_recycler_view)
    RecyclerView EmergencyRecyclerview;
    @BindView(R.id.llfordetails)
    LinearLayout llfordetails;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_emergency_contact );
        ButterKnife.bind( this );

        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this ) );
        apiManager = retrofit.create( ApiManager.class );
        llfordetails.setVisibility( View.VISIBLE );
        EmergencyRecyclerview.setVisibility( View.GONE );
        swipe_refresh_layout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContactList();
            }
        } );
        EmergencyRecyclerview.setLayoutManager( new LinearLayoutManager( this ) );
        topDownAdapter = new EmergencyAdapter( new ArrayList<>(), new OnEmergencyTakeActionListener() {
            @Override
            public void show(DataItem dataItem) {
                showDetailsDilaog( dataItem );

            }

            @Override
            public void update(View view, DataItem dataItem, int pos) {
                showUpdateDialog( view, dataItem, pos );
//                apiManager.updateEmergency(dataItem.getId(),d)
            }

            @Override
            public void delete(View view, int detial_id, int pos) {
                deletedDialog( view, detial_id, pos );
            }

            @Override
            public void updateUI() {
                llfordetails.setVisibility( View.VISIBLE );
                EmergencyRecyclerview.setVisibility( View.GONE );
            }
        } );
        EmergencyRecyclerview.setAdapter( topDownAdapter );
        getContactList();
    }

    void getContactList() {
        CommonUtils.showProgressDialog( getBaseContext() );
        apiManager.getEmergency()
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<GetEmergencyModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(GetEmergencyModel getEmergencyModel) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        swipe_refresh_layout.setRefreshing( false );
                        llfordetails.setVisibility( View.GONE );
                        EmergencyRecyclerview.setVisibility( View.VISIBLE );
                        topDownAdapter.updateList( getEmergencyModel.getData() );
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );

                    }
                } );
    }
    // TODO

    private void showDetailsDilaog(DataItem dataItem) {
        final Dialog dialog = new Dialog( this );
        // Include dialog.xml file
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        Window window2 = dialog.getWindow();
        dialog.setCancelable( true );
        window2.setGravity( Gravity.CENTER );
        window2.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.setContentView( R.layout.dialogforaddcontact );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        dialog.show();
        TextInputLayout inputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_refralcode );
        TextInputLayout phoneInputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_cpass );
        EditText input_name = (EditText) dialog.findViewById( R.id.input_name );
        EditText input_phone = (EditText) dialog.findViewById( R.id.input_contactno );
        input_name.setEnabled( false );
        input_phone.setEnabled( false );
        input_name.setText( "" + dataItem.getName() );
        input_phone.setText( "" + dataItem.getContact() );
        TextView cancel = (TextView) dialog.findViewById( R.id.cancel );
        cancel.setVisibility( View.VISIBLE );
        TextView save = (TextView) dialog.findViewById( R.id.viewongng );
        save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                notifyAll();
            }
        } );
    }

    private void deletedDialog(View view, int detial_id, int pos) {
        final Dialog dialog = new Dialog( this );
        // Include dialog.xml file
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        Window window2 = dialog.getWindow();
        dialog.setCancelable( true );
        window2.setGravity( Gravity.CENTER );
        window2.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.setContentView( R.layout.dialogforaddcontact );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        dialog.show();
        LinearLayout show_input = (LinearLayout) dialog.findViewById( R.id.show_input );
        show_input.setVisibility( View.GONE );
        TextView title = (TextView) dialog.findViewById( R.id.timer );
        title.setText( dialog.getContext().getString( R.string.confirm_delete ) );
        TextView cancel = (TextView) dialog.findViewById( R.id.cancel );
        cancel.setText( "No" );
        cancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        } );
        TextView save = (TextView) dialog.findViewById( R.id.viewongng );
        save.setText( "Yes" );
        save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                CommonUtils.showProgressDialog( getBaseContext() );
                apiManager.deleteEmergency( detial_id )
                        .subscribeOn( Schedulers.io() )
                        .observeOn( AndroidSchedulers.mainThread() )
                        .subscribe( new SingleObserver<DeleteEmergencyModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(DeleteEmergencyModel updateEmergencyModel) {
                                CommonUtils.disMissProgressDialog( getBaseContext() );
                                if (updateEmergencyModel.getStatus().equalsIgnoreCase( "true" )) {
                                    topDownAdapter.deleted( pos );
                                    dialog.dismiss();
                                    Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + updateEmergencyModel.getMessage(), Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                    toast.show();

                                } else {
                                   Toast toast= Toast.makeText( EmergencyContactActivity.this, "" + updateEmergencyModel.getError(), Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }

                            }

                            @Override
                            public void onError(Throwable e) {
                                CommonUtils.disMissProgressDialog( getBaseContext() );
                                dialog.dismiss();
                                Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                toast.show();

                            }
                        } );

            }

        } );
    }

    private void showUpdateDialog(View view, DataItem dataItem, int pos) {
        final Dialog dialog = new Dialog( this );
        // Include dialog.xml file
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        Window window2 = dialog.getWindow();
        dialog.setCancelable( true );
        window2.setGravity( Gravity.CENTER );
        window2.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.setContentView( R.layout.dialogforaddcontact );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        dialog.show();
        TextInputLayout inputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_refralcode );
        TextInputLayout phoneInputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_cpass );
        TextInputLayout emailInputLayout = (TextInputLayout) dialog.findViewById( R.id.input_layout_email );
        EditText input_name = (EditText) dialog.findViewById( R.id.input_name );
        EditText input_phone = (EditText) dialog.findViewById( R.id.input_contactno );
        EditText email_phone = (EditText) dialog.findViewById( R.id.input_email );
        input_name.setText( "" + dataItem.getName() );
        input_phone.setText( "" + dataItem.getContact() );
        email_phone.setText( "" + dataItem.getEmail() );
        TextView cancel = (TextView) dialog.findViewById( R.id.cancel );
        cancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        } );
        TextView save = (TextView) dialog.findViewById( R.id.viewongng );
        save.setText( "Update" );
        save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValid = validateForm( input_name, inputLayout, input_phone, phoneInputLayout, email_phone, emailInputLayout );
                if (isValid) {
                    int user_id = AppPreferences.newInstance().getUserId( EmergencyContactActivity.this );
                    String name = input_name.getText().toString().trim();
                    String phone = input_phone.getText().toString().trim();
                    CommonUtils.showProgressDialog( getBaseContext() );
                    apiManager.updateEmergency( dataItem.getId(), user_id, name, "" + email_phone.getText().toString(), phone )
                            .subscribeOn( Schedulers.io() )
                            .observeOn( AndroidSchedulers.mainThread() )
                            .subscribe( new SingleObserver<UpdateEmergencyModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onSuccess(UpdateEmergencyModel updateEmergencyModel) {
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                    try {
                                        if (updateEmergencyModel.getStatus().equalsIgnoreCase( "true" )) {
                                            topDownAdapter.updateListItem( dataItem, pos );
                                            dialog.dismiss();
                                           Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + updateEmergencyModel.getMessage(), Toast.LENGTH_SHORT );
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                            toast.show();

                                        } else {
                                            Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + updateEmergencyModel.getError(), Toast.LENGTH_SHORT );
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                            toast.show();
                                        }

                                    } catch (Exception e) {
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                    dialog.dismiss();
                                    try {
                                        Toast toast=Toast.makeText( EmergencyContactActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(EmergencyContactActivity.this, android.R.color.holo_red_dark));
                                        toast.show();

                                    } catch (Exception x) {
                                    }

                                }
                            } );
                }
            }

        } );

    }

    private boolean validateForm(EditText input_name, TextInputLayout inputLayout, EditText phone, TextInputLayout phoneInputLayout,
                                 EditText email, TextInputLayout emilInputLayout) {
        if (!ValidateInputs.isValidName( input_name.getText().toString().trim() )) {
            inputLayout.setError( getString( R.string.invalid_nick_name ) );
            return false;
        } else if (!ValidateInputs.isValidPhoneNo( phone.getText().toString().trim() )) {
            phoneInputLayout.setError( getString( R.string.invalid_phone ) );
            return false;
        }  else if (!ValidateInputs.isValidEmail( email.getText().toString().trim() )) {
            emilInputLayout.setError( getString( R.string.invalid_emaild_id ) );
            return false;
        } else {
            return true;
        }
    }

}
