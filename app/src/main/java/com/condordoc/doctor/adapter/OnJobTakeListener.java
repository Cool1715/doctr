package com.condordoc.doctor.adapter;

import android.view.View;

import com.condordoc.doctor.model.job.DataItem;

public interface OnJobTakeListener {

    void startJob(View view, DataItem dataItem, int pos);
    void acceptJob(View view, DataItem dataItem, int pos);
    void cancelJob(View view, DataItem dataItem, int pos);
}
