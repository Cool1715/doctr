package com.condordoc.doctor.adapter;

public interface OnBottomReachedListener {

    void onBottomReached(int position);
}
