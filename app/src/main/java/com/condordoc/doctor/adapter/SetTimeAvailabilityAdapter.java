package com.condordoc.doctor.adapter;

import android.content.Context;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.model.schedule.TimeScheduleModel;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
public class SetTimeAvailabilityAdapter extends BaseAdapter {

    

    public interface OnAddPriceClick{
        void OnAddPriceClick(int pos, String from, String to, String price);
    }
    
    
    Context pContext;
    List<TimeScheduleModel> timeScheduleModels;
    OnAddPriceClick onAddPriceClick;
    private Map<String, Boolean> mapSchedule;
    private OnChangeTimeAvailabilitiesListener listener;

    public SetTimeAvailabilityAdapter(OnAddPriceClick listener, Context context, List<TimeScheduleModel> timeScheduleModels, Map<String, Boolean> mapSchedule) {
        this.timeScheduleModels = timeScheduleModels;
        this.mapSchedule = mapSchedule;
        this.pContext = context;
        this.onAddPriceClick = listener;
    }

    @Override
    public int getCount() {
        return timeScheduleModels.size();
    }

    @Override
    public Object getItem(int pos) {
        return pos;
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final MyAvailabilityGridAdapter.MyHolder myHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlayoutforavailable, parent, false);
            myHolder = new MyAvailabilityGridAdapter.MyHolder(convertView);
            convertView.setTag(myHolder);
        } else {
            myHolder = (MyAvailabilityGridAdapter.MyHolder) convertView.getTag();
        }

        myHolder.icon.setText("" + timeScheduleModels.get(position).toString());
        myHolder.icon.setTag(position);

        myHolder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                if (mapSchedule.containsKey(timeScheduleModels.get(position).toString())) {
                    if (timeScheduleModels.get(position).isActive()) {
                        timeScheduleModels.get(position).setActive(false);
                        myHolder.icon.setTextColor(Color.parseColor("#ffffff"));
                        myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgforapply));
                        String from = timeScheduleModels.get(position).getFromTime();
                        String to = timeScheduleModels.get(position).getToTime();
                        if (onAddPriceClick != null){
                            onAddPriceClick.OnAddPriceClick(position, from,to, "" );
                            notifyDataSetChanged();
                        }
                        listener.onSetTimeAvailability(position, "" + from, "" + to);
                    } else {
                        timeScheduleModels.get(position).setActive(true);
                        myHolder.icon.setTextColor(Color.parseColor("#0272A4"));
                        myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgfornonapply));
                        String from = timeScheduleModels.get(position).getFromTime();
                        String to = timeScheduleModels.get(position).getToTime();
                        listener.onRemoveTimeAvailability(position, "" + from, "" + to);

                    }
                } else {
                    if(!timeScheduleModels.get(position).isActive()) {
                        timeScheduleModels.get(position).setActive(true);
                        myHolder.icon.setTextColor(Color.parseColor("#ffffff"));
                        myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgforapply));
                        String from = timeScheduleModels.get(position).getFromTime();
                        String to = timeScheduleModels.get(position).getToTime();
                        if (onAddPriceClick != null){
                            onAddPriceClick.OnAddPriceClick(position, from,to, "");
                            notifyDataSetChanged();
                        }
                        listener.onSetTimeAvailability(position, "" + from, "" + to);
                    }else{
                        timeScheduleModels.get(position).setActive(false);
                        myHolder.icon.setTextColor(Color.parseColor("#0272A4"));
                        myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgfornonapply));
                        String from = timeScheduleModels.get(position).getFromTime();
                        String to = timeScheduleModels.get(position).getToTime();
                        listener.onRemoveTimeAvailability(position, "" + from, "" + to);
                    }
                }
            }
        });

        try {
            if (mapSchedule.containsKey(timeScheduleModels.get(position).toString())) {
                timeScheduleModels.get(position).setActive(false);
                myHolder.icon.setTextColor(Color.parseColor("#ffffff"));
                myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgforapply));
            } else {
                timeScheduleModels.get(position).setActive(false);
                myHolder.icon.setTextColor(Color.parseColor("#0272A4"));
                myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgfornonapply));
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (timeScheduleModels.get(position).isActive()) {
                timeScheduleModels.get(position).setActive(true);
                myHolder.icon.setTextColor(Color.parseColor("#ffffff"));
                myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgforapply));
            } else {
                timeScheduleModels.get(position).setActive(false);
                myHolder.icon.setTextColor(Color.parseColor("#0272A4"));
                myHolder.icon.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bgfornonapply));
            }

        }
        return convertView;
    }


    public void setOnChangeTimeAvailabilitiesListener(OnChangeTimeAvailabilitiesListener listener) {
        this.listener = listener;
    }

    public void clearCheckedList() {
        this.mapSchedule.clear();
        notifyDataSetChanged();
    }

    public void removeItemCheckedList(List<String> fromToList) {
        for (String fromToItem : fromToList) {
            mapSchedule.remove(fromToItem);
        }
        notifyDataSetChanged();
    }

    static class MyHolder {
        @BindView(R.id.icon)
        TextView icon;

        public MyHolder(View view) {
            ButterKnife.bind(this, view);
            AppCompatActivity activity = (AppCompatActivity) view.getContext().getApplicationContext();

        }
    }

    public interface OnChangeTimeAvailabilitiesListener {
        void onSetTimeAvailability(int post, String from, String to);

        void onRemoveTimeAvailability(int pos, String from, String to);
    }
}
