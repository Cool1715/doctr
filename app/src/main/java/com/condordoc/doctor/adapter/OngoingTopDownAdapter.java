package com.condordoc.doctor.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.SpecificJobActivity;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.GetAddressUitls;
import java.util.Collections;
import java.util.List;

public class OngoingTopDownAdapter extends RecyclerView.Adapter<OngoingTopDownAdapter.BaseViewHolder> {

    private static final int EMPTY_VIEW = 1;
    private static final int ITEM_VIEW = 2;
    private String TAG = OngoingTopDownAdapter.class.getSimpleName();
    private List<DataItem> dataItems;

    public OngoingTopDownAdapter(List<DataItem> dataItems) {
        this.dataItems = dataItems;
        Collections.reverse(this.dataItems);
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case EMPTY_VIEW:
                View v = inflater.inflate(R.layout.empty_item, viewGroup, false);
                return new EmptyRowViewHolder(v);
            case ITEM_VIEW:
                v = inflater.inflate(R.layout.itemlayouforpastjobs, viewGroup, false);
                return new MainRowViewHolder(v);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, final int position) {

        if (holder instanceof MainRowViewHolder) {
            MainRowViewHolder holder1 = (MainRowViewHolder) holder;
            holder1.bind(dataItems.get(position));
            Log.i(TAG, "onBindViewHolder: ");
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), SpecificJobActivity.class);
                    intent.putExtra(AppConstant.START_JOB_ID, dataItems.get(position).getId() + "");
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (dataItems != null && dataItems.size() == 0) {
            return EMPTY_VIEW;
        } else {
            return ITEM_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        assert dataItems != null;
        if (dataItems.size() == 0) {
            return 1;
        }
        return dataItems.size();
    }

    public void updateList(List<DataItem> dataItems) {
        this.dataItems = dataItems;
        Collections.reverse(this.dataItems);
        notifyDataSetChanged();
    }
    public class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView time, header, patientAddress;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class EmptyRowViewHolder extends BaseViewHolder {

        TextView time, header, patientAddress;

        public EmptyRowViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class MainRowViewHolder extends BaseViewHolder{

        TextView headerprovider,time, header, patientAddress, Finished;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
            time = (TextView) itemView.findViewById(R.id.time);
            header = (TextView) itemView.findViewById(R.id.header);
            patientAddress = (TextView) itemView.findViewById(R.id.patientAddress);
            Finished = (TextView) itemView.findViewById(R.id.pastFragStatus);
        }

        public void bind(DataItem dataItem) {
            try {
                headerprovider.setText(String.valueOf(dataItem.getId()));
                time.setText(dataItem.getCreatedAt());
                header.setText(String.format("%s-%s",dataItem.getPatientName(),dataItem.getDiseaseName()));
                patientAddress.setText(GetAddressUitls.getCompleteAddressString(itemView.getContext(), dataItem.getPatientLat(),
                        dataItem.getPatientLong()));

                if(dataItem.getStatus() == 1){
                    Finished.setText("ACCEPT");
                }

                if (dataItem.getStatus() == 4){
                    Finished.setText("On Going");
                }

                if (dataItem.getStatus() == 5){
                    Finished.setText("Finished");
                }

            } catch (Exception e) {
                patientAddress.setText("No Available Address");
                e.printStackTrace();
            }
        }
    }
}

