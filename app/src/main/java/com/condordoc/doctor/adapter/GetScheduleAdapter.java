package com.condordoc.doctor.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.model.schedule.DataItem;

import java.util.List;

public class GetScheduleAdapter extends RecyclerView.Adapter<GetScheduleAdapter.MyScheduleViewHolder> {

    private List<DataItem> scheduleItemList;

    public GetScheduleAdapter(List<DataItem> scheduleItemList){
        this.scheduleItemList = scheduleItemList;
    }

    @NonNull
    @Override
    public MyScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforavailable, viewGroup,false);
        return new MyScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyScheduleViewHolder myScheduleViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return scheduleItemList.size();
    }

    public void clear() {

    }

    public class MyScheduleViewHolder extends RecyclerView.ViewHolder {
        public MyScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public void updateList(List<DataItem> scheduleItemList){
        this.scheduleItemList = scheduleItemList;
        notifyDataSetChanged();
    }
}
