package com.condordoc.doctor.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.SpecificJobActivity;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.StartJobModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.GetAddressUitls;

import java.util.Collections;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class UpcomingTopDownAdapter extends RecyclerView.Adapter<UpcomingTopDownAdapter.BaseViewHolder> {

    private static final int EMPTY_VIEW = 1;
    private static final int ITEM_VIEW = 2;

    private List<DataItem> dataItemList;
    private final OnJobTakeListener listener;


    public UpcomingTopDownAdapter(List<DataItem> dataItemList, OnJobTakeListener listener) {
        this.dataItemList = dataItemList;
        Collections.reverse(this.dataItemList);
        this.listener = listener;
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = null;

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case EMPTY_VIEW:
                v = inflater.inflate(R.layout.empty_item, viewGroup, false);
                return new EmptyRowViewHolder(v);
            case ITEM_VIEW:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforjobs, viewGroup, false);
                return new MainRowViewHolder(v);

        }

        return null;

//        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforjobs, viewGroup, false);
//        return new MainRowViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder1,  int position) {
        if (holder1 instanceof  MainRowViewHolder) {

            MainRowViewHolder holder = (MainRowViewHolder) holder1;

            holder.bind(dataItemList.get(position));

        /*if (dataItemList.get(position).getStatus() == 1) {
            holder.startJob.setTag(position);
            holder.startJob.setText("Accept Job");
            holder.startJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.acceptJob(v, dataItemList.get(position), position);
                }
            });
        } else if (dataItemList.get(position).getStatus() == 4) {

        }*/
            holder.startJob.setTag(position);
            holder.startJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.startJob(v, dataItemList.get(position), holder.getAdapterPosition());
                }
            });
            holder.cancelJob.setTag(position);
            holder.cancelJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.cancelJob(v, dataItemList.get(position), holder.getAdapterPosition());
                }
            });

        }
    }

    public void startJob(Context context, DataItem dataItem, int pos, ApiManager apiManager) {

      apiManager.setJobStarted(String.valueOf(dataItem.getId())).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<StartJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }
            @Override
            public void onSuccess(StartJobModel cancelJobModel) {

                Intent intent = new Intent(context, SpecificJobActivity.class);
                intent.putExtra(AppConstant.START_JOB_ID, dataItem.getId() + "");
                context.startActivity(intent);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public void acceptJob(Context context, DataItem dataItem, int pos) {
        Intent intent = new Intent(context, SpecificJobActivity.class);
        intent.putExtra(AppConstant.START_JOB_ID, dataItem.getId() +"");
        context.startActivity(intent);
        dataItemList.remove(pos);
        notifyDataSetChanged();
    }

    public void cancelJob(DataItem dataItem, int pos) {
        dataItemList.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (dataItemList != null && dataItemList.size() == 0) {
            return EMPTY_VIEW;
        } else {
            return ITEM_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        assert dataItemList != null;
        if (dataItemList.size() == 0) {
            return 1;
        }
        return dataItemList.size();
    }


    public class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView time, header, patientAddress;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class EmptyRowViewHolder extends BaseViewHolder {

        TextView time, header, patientAddress;

        public EmptyRowViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void updateList(List<DataItem> dataItems) {
        this.dataItemList = dataItems;
        Collections.reverse(this.dataItemList);
        notifyDataSetChanged();
    }

    public class MainRowViewHolder extends BaseViewHolder {

        TextView headerprovider, startJob, cancelJob, patientAddress, header, time;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
            startJob = (TextView) itemView.findViewById(R.id.startJob);
            cancelJob = (TextView) itemView.findViewById(R.id.cancelJob);
            patientAddress = (TextView) itemView.findViewById(R.id.patientAddress);
            header = (TextView) itemView.findViewById(R.id.header);
            time = (TextView) itemView.findViewById(R.id.time);
        }

        public void bind(DataItem dataItem) {
            headerprovider.setText(String.valueOf(dataItem.getId()));
            header.setText(String.format("%s-%s", dataItem.getPatientName(), dataItem.getDiseaseName()));
            time.setText(dataItem.getCreatedAt());
            try {
                if (!TextUtils.isEmpty(dataItem.getPatientLat() + "") && !TextUtils.isEmpty(dataItem.getPatientLong() + "")) {
                    patientAddress.setText(GetAddressUitls.getCompleteAddressString(itemView.getContext(), dataItem.getPatientLat(),
                            dataItem.getPatientLong()));
                } else
                    patientAddress.setText("No Address");
            } catch (Exception e) {
                patientAddress.setText("No Address");
            }
        }
    }
}
