package com.condordoc.doctor.adapter;

import android.view.View;

import com.condordoc.doctor.model.emergency.DataItem;

public interface OnEmergencyTakeActionListener {

    void show(DataItem dataItem);

    void update(View view, DataItem dataItem, int pos);

    void delete(View view, int detial_id,int pos);

    void updateUI();

}
