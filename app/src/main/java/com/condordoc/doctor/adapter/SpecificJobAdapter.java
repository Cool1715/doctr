package com.condordoc.doctor.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.JobMapUserActivity;
import com.condordoc.doctor.R;
import com.condordoc.doctor.model.job.jobbyId.JobByIdStatus;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.DateTimeUtils;

import java.util.ArrayList;
public class SpecificJobAdapter extends RecyclerView.Adapter<SpecificJobAdapter.MainRowViewHolder> {

    private final ArrayList<JobByIdStatus.DataBean.JobUpdatesBean> jobList;
    private final Context context;
  //  private List<DataItem> dataItems;


 /*   public SpecificJobAdapter(List<DataItem> dataItemList) {
        dataItems = dataItemList;
    }*/

    public SpecificJobAdapter(Context applicationContext, ArrayList<JobByIdStatus.DataBean.JobUpdatesBean> jobList) {
        this.context=applicationContext;
        this.jobList=jobList;
    }


    @NonNull
    @Override
    public MainRowViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.itemlayoutforspecificjob, viewGroup, false);
        return new MainRowViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MainRowViewHolder holder, final int position) {
        holder.bind(jobList.get(position), position);
        holder.seal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), JobMapUserActivity.class);
                intent.putExtra(AppConstant.START_JOB_ID,jobList.get(position).getId() +"");
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobList!=null?jobList.size():0;
    }
/*
    public void updateList(List<DataItem> dataItems) {
        this.dataItems = dataItems;
        Collections.reverse(this.dataItems);
        notifyDataSetChanged();
    }*/

    public class MainRowViewHolder extends RecyclerView.ViewHolder {

        LinearLayout seal;
        TextView count, tvServiceType, timeAgo, time,tvDoctStatus;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            count = (TextView) itemView.findViewById(R.id.count);
            seal = (LinearLayout) itemView.findViewById(R.id.seeall);
            timeAgo = (TextView) itemView.findViewById(R.id.timeAgo);
            time = (TextView) itemView.findViewById(R.id.time);
            tvDoctStatus = (TextView) itemView.findViewById(R.id.tvDoctStatus);
        }

        public void bind(JobByIdStatus.DataBean.JobUpdatesBean dataItem, int pos) {

            try {
                count.setText(String.format("%s", String.valueOf(pos)));
                tvDoctStatus.setText(dataItem.getJob_update());
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Date date = sdf.parse(dataItem.getUpdatedAt());
                String timeagp = DateTimeUtils.getAgoTime(dataItem.getCreated_at());
                timeAgo.setText(timeagp);
                timeAgo.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
