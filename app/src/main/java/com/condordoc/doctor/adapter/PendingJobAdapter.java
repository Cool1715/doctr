package com.condordoc.doctor.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.SpecificJobActivity;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.GetAddressUitls;

import java.util.Collections;
import java.util.List;

import static com.condordoc.doctor.utils.AppConstant.START_JOB_ID;

public class PendingJobAdapter extends RecyclerView.Adapter<PendingJobAdapter.BaseViewHolder> {

    private static final int EMPTY_VIEW = 1;
    private static final int ITEM_VIEW = 2;

    private List<DataItem> dataItemList;
    private final OnJobActionListener listener;

    public PendingJobAdapter(List<DataItem> dataItemList, OnJobActionListener listener) {
        this.dataItemList = dataItemList;
        Collections.reverse(this.dataItemList);
        this.listener = listener;
    }


    public void updateList(List<DataItem> dataItems) {
        this.dataItemList = dataItems;
        Collections.reverse(this.dataItemList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public PendingJobAdapter.BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case EMPTY_VIEW:
                v = inflater.inflate(R.layout.empty_item, viewGroup, false);
                return new EmptyRowViewHolder(v);
            case ITEM_VIEW:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforpendingjobs, viewGroup, false);
                return new MainRowViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(PendingJobAdapter.BaseViewHolder holder1, final int position){

        if (holder1 instanceof MainRowViewHolder) {
            MainRowViewHolder holder = (MainRowViewHolder) holder1;

            holder.bind(dataItemList.get(position));
            holder.accept_job.setTag(position);
            holder.accept_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.jobAcceptByDoctor(v, position, dataItemList.get(position));
                }
            });

            holder.decline_job.setTag(position);
            holder.decline_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.jobDeclineByDoctor(v, position, dataItemList.get(position));
                    Toast.makeText(v.getContext(), "Decline Job", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    public void acceptJob(Context context, DataItem dataItem, int pos) {
        dataItemList.remove(pos);
        notifyDataSetChanged();
    }


    public void startJob(Context context, DataItem dataItem, int pos) {
        /*dataItemList.remove(pos);
        notifyItemChanged(pos);*/
        Intent intent = new Intent(context, SpecificJobActivity.class);
        intent.putExtra(START_JOB_ID, dataItem.getId());
        intent.putExtra(AppConstant.PATIENT_NAME, dataItem.getPatientName());
        intent.putExtra(AppConstant.LAT, dataItem.getPatientLat());
        intent.putExtra(AppConstant.LNG, dataItem.getDoctorLong());
        context.startActivity(intent);
    }

    public void declineJob(DataItem dataItem, int pos) {
        dataItemList.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (dataItemList != null && dataItemList.size() == 0) {
            return EMPTY_VIEW;
        } else {
            return ITEM_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        assert dataItemList != null;
        if (dataItemList.size() == 0) {
            return 1;
        }
        return dataItemList.size();
    }


    public class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView time, header, patientAddress;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class EmptyRowViewHolder extends BaseViewHolder {

        TextView time, header, patientAddress;

        public EmptyRowViewHolder(View itemView){
            super(itemView);
        }
    }


    public class MainRowViewHolder extends BaseViewHolder {

        TextView headerprovider, accept_job, decline_job, patientAddress, header, time;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
            accept_job = (TextView) itemView.findViewById(R.id.accept_job);
            decline_job = (TextView) itemView.findViewById(R.id.decline_job);
            patientAddress = (TextView) itemView.findViewById(R.id.patientAddress);
            header = (TextView) itemView.findViewById(R.id.header);
            time = (TextView) itemView.findViewById(R.id.time);
        }

        public void bind(DataItem dataItem) {
            try{
                headerprovider.setText(String.valueOf(dataItem.getId()));
                time.setText(dataItem.getDateOfBooking());
                header.setText(String.format("%s-%s", dataItem.getPatientName(), dataItem.getDiseaseName()));

            } catch (Exception e) {

            }
            try {
                if (!TextUtils.isEmpty(dataItem.getPatientLat() + "") && !TextUtils.isEmpty(dataItem.getPatientLong() + "")) {
                    patientAddress.setText(GetAddressUitls.getCompleteAddressString(itemView.getContext(), dataItem.getPatientLat(), dataItem.getPatientLong()));
                } else patientAddress.setText("No Address");
            } catch (Exception e) {
                patientAddress.setText("No Address");

            }
        }

    }
}
