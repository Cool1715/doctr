package com.condordoc.doctor.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.condordoc.doctor.R;
import com.condordoc.doctor.SubServiceActivity;
import com.condordoc.doctor.model.disease.DataItem;
import com.condordoc.doctor.model.disease_doctor.Disease;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.SelectDiease;

public class LocalServiceAdapter extends RecyclerView.Adapter<LocalServiceAdapter.ItemViewHolder>   {



    private List<DataItem> dataItems;
    private final SubServiceActivity activity;
    private int lastCheckedPosition = -1;
    private int id;
    private List<Disease> die;

    public LocalServiceAdapter(List<DataItem> dataItems, SubServiceActivity activity) {
        this.dataItems = dataItems;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LocalServiceAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view;

        switch (type) {
            case 1:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_local_service, viewGroup, false);
                return new ItemViewHolder(view, 1);

            default:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_local_service, viewGroup, false);
                return new ItemViewHolder(view, 0);

        }

    }

    @Override
    public void onBindViewHolder(@NonNull LocalServiceAdapter.ItemViewHolder itemViewHolder, int pos) {

        try {
            itemViewHolder.bind(dataItems.get(pos), pos);
        }catch (Exception e){
            itemViewHolder.empty();
        }
    }


    @Override
    public int getItemViewType(int position) {
        return dataItems.size() > 0 ? 1 : super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return dataItems.size() > 0 ? dataItems.size() : 1;
    }

    public void addLocalItem(List<DataItem> data) {
        dataItems = data;
        notifyDataSetChanged();
    }






    public class ItemViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView textView;

        public ItemViewHolder(@NonNull View itemView, int type) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.radio_button);
            textView = itemView.findViewById(R.id.empty);
        }

        public void bind(DataItem dataItem, int position) {
            Log.e("datttttt",""+dataItem.toString());
            checkBox.setText(dataItem.getName());
            checkBox.setId(dataItem.getId());
            checkBox.setChecked(dataItem.getCheck());

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    activity.setDate(checkBox.getId());
//                    AppPreferences.newInstance().setDiseaseId(itemView.getContext(), String.valueOf(dataItem.getId()));
//                    AppPreferences.newInstance().setServiceLocalId(itemView.getContext(), dataItem.getId());
                    //because of this blinking problem occurs so
                    //i have a suggestion to add notifyDataSetChanged();
                    //   notifyItemRangeChanged(0, list.length);//blink list problem



                }
            });


        }

        public void empty() {
            checkBox.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }
    }
}
