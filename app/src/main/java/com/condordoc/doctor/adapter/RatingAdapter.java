package com.condordoc.doctor.adapter;

import android.media.Rating;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.condordoc.doctor.R;

import com.condordoc.doctor.model.feedback.FeedbackGetModel;

public class RatingAdapter extends  RecyclerView.Adapter<RatingAdapter.BaseViewHolder> {
    private static final int EMPTY_VIEW = 1;
    private static final int ITEM_VIEW = 2;
    private String TAG = OngoingTopDownAdapter.class.getSimpleName();
    private List<FeedbackGetModel> dataItems;

    public RatingAdapter(List<FeedbackGetModel> dataItems) {

        this.dataItems = dataItems;
    }


    @NonNull
    @Override
    public RatingAdapter.BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case EMPTY_VIEW:
                View v = inflater.inflate(R.layout.empty_item, viewGroup, false);
                return new EmptyRowViewHolder(v);
            case ITEM_VIEW:
                v = inflater.inflate(R.layout.item_transaction, viewGroup, false);
                return new MainRowViewHolder(v);
            default:
                return null;
        }

    }



    @Override
    public void onBindViewHolder(RatingAdapter.BaseViewHolder holder, final int position) {

        if (holder instanceof RatingAdapter.MainRowViewHolder) {
            RatingAdapter.MainRowViewHolder holder1 = (RatingAdapter.MainRowViewHolder) holder;
            holder1.bind(dataItems.get(position));
            Log.i(TAG, "onBindViewHolder: ");
            holder.itemView.setTag(position);
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(v.getContext(), SpecificJobActivity.class);
//                    intent.putExtra(AppConstant.START_JOB_ID, dataItems.get(position).getId()+"");//pass only string
//                    v.getContext().startActivity(intent);
//                }
//            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (dataItems != null && dataItems.size() == 0) {
            return EMPTY_VIEW;
        } else {
            return ITEM_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        assert dataItems != null;
        if (dataItems.size() == 0){
            return 1;
        }
        return dataItems.size();
    }

    public void updateList(List<FeedbackGetModel> dataItems) {
        this.dataItems = dataItems;
        notifyDataSetChanged();
    }
    public class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView headerprovider, feeback;
        RatingBar rating;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class EmptyRowViewHolder extends RatingAdapter.BaseViewHolder {

        TextView headerprovider, feeback;
        RatingBar rating;
        public EmptyRowViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class MainRowViewHolder extends RatingAdapter.BaseViewHolder {

        TextView headerprovider, feeback;
        RatingBar rating;
        public MainRowViewHolder(View itemView) {
            super(itemView);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
            rating=(RatingBar) itemView.findViewById(R.id.rating);
            feeback = (TextView) itemView.findViewById(R.id.feedback);

        }

        public void bind(FeedbackGetModel dataItem) {
            try {
                headerprovider.setText(String.valueOf(dataItem.getCreatedAt()));
                rating.setRating(Float.parseFloat(dataItem.getRating()));
                feeback.setText(dataItem.getFeedback());
/*
                if(dataItem.getStatus() == 1){
                    Finished.setText("ACCEPTED");
                }

                if (dataItem.getStatus() == 4){
                    Finished.setText("On Going");
                }

                if (dataItem.getStatus() == 5){
                    Finished.setText("Finished");
                }*/


            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }
}
