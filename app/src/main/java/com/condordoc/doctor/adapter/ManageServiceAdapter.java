package com.condordoc.doctor.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.BankDetailActivity;
import com.condordoc.doctor.R;
import com.condordoc.doctor.SubServiceActivity;
import com.condordoc.doctor.model.manage.DataItem;
import com.condordoc.doctor.utils.AppConstant;

import java.util.List;

public class ManageServiceAdapter extends RecyclerView.Adapter<ManageServiceAdapter.MainRowViewHolder> {

    private List<DataItem> dataItems;
    String TAG = EmergencyAdapter.class.getSimpleName();

    public ManageServiceAdapter(List<DataItem> dataItems) {
        this.dataItems = dataItems;
    }

    @NonNull
    @Override
    public MainRowViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforservices, viewGroup, false);
        return new MainRowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainRowViewHolder holder, final int position) {
        Log.e(TAG, "asdasd22");
        holder.bind(dataItems.get(position));
        holder.seeall.setTag(position);
        holder.seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SubServiceActivity.class);
//                 intent.putExtra("location", MainActivity.locationtext.getText().toString().trim());
                intent.putExtra(AppConstant.MANAGE_SERVICE_ID, dataItems.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public void addItem(List<DataItem> manageServiceData) {
        this.dataItems = manageServiceData;
        notifyDataSetChanged();
    }

    public class MainRowViewHolder extends RecyclerView.ViewHolder {

        LinearLayout seeall;
        TextView headerprovider;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            seeall = (LinearLayout) itemView.findViewById(R.id.seeall);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
        }

        public void bind(DataItem item){
            headerprovider.setText(item.getServiceName());
        }
    }
}
