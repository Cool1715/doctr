package com.condordoc.doctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.condordoc.doctor.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAvailabilityGridAdapter extends BaseAdapter {

    Context context;
    String logos[];
    String counts[];
    LayoutInflater inflter;

    public MyAvailabilityGridAdapter(Context applicationContext, String[] logos, String[] abc2) {
        this.context = applicationContext;
        this.logos = logos;
        this.counts = abc2;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return logos.length;
    }
    @Override
    public Object getItem(int i) {
        return logos[i];
    }
    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MyHolder myHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_activity_gridview, parent, false);
            myHolder = new MyHolder(convertView);
            convertView.setTag(myHolder);
        } else {
            myHolder = (MyHolder) convertView.getTag();
        }

        myHolder.icon.setText(""+logos[position]);
//
//        myHolder.icon.setTag(position);
//
//        myHolder.icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int pos = (int) v.getTag();
//                if(counts[pos].equalsIgnoreCase("0")) {
//                    counts[pos]="1";
//                    myHolder.icon.setTextColor(Color.parseColor("#ffffff"));
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        myHolder.icon.setBackground(context.getDrawable(R.drawable.bgforapply));
//                    }
//                }
//                else
//                {
//                    counts[pos]="0";
//                    myHolder.icon.setTextColor(Color.parseColor("#0272A4"));
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        myHolder.icon.setBackground(context.getDrawable(R.drawable.bgfornonapply));
//                    }
//
//                }
//            }
//        });



        return convertView;
    }

    static class MyHolder {
        @BindView(R.id.icon)
        TextView icon;

        public MyHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
