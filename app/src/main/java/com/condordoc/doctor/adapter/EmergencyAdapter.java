package com.condordoc.doctor.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.model.emergency.DataItem;

import java.util.List;


public class EmergencyAdapter extends RecyclerView.Adapter<EmergencyAdapter.MainRowViewHolder> {


    private List<DataItem> dataItemList;
    private final OnEmergencyTakeActionListener listener;

    public EmergencyAdapter(List<DataItem> dataItemList, OnEmergencyTakeActionListener listener) {
        this.dataItemList = dataItemList;
        this.listener = listener;
    }


    @NonNull
    @Override
    public EmergencyAdapter.MainRowViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayoutforemergencycontact, viewGroup, false);
        return new EmergencyAdapter.MainRowViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EmergencyAdapter.MainRowViewHolder holder, final int position) {
        holder.bind(dataItemList.get(position));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.delete(v,dataItemList.get(position).getId(),position);
            }
        });

        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.update(v,dataItemList.get(position), position);
            }
            
        });

        holder.show_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                listener.show(dataItemList.get(position));
            }
        });
    }


    public void deleted(int pos){
        dataItemList.remove(pos);
        notifyItemRemoved(pos);

        if (dataItemList.size() == 0){
            listener.updateUI();
        }

    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    public void updateList(List<DataItem> dataItems) {
        this.dataItemList = dataItems;
        notifyDataSetChanged();
    }

    public void updateListItem(DataItem dataItem, int pos) {
        dataItemList.remove(pos);
        dataItemList.add(pos,dataItem);
        notifyItemChanged(pos);
    }

    public void addItem(DataItem dataItem) {
        dataItemList.add(dataItem);
        notifyDataSetChanged();
    }

    public class MainRowViewHolder extends RecyclerView.ViewHolder {

        LinearLayout show_details;
        ImageView delete, update;
        TextView name;
        TextView contacName;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            show_details = (LinearLayout) itemView.findViewById(R.id.show_details);
            name = (TextView) itemView.findViewById(R.id.name);
            contacName = (TextView) itemView.findViewById(R.id.conttact_number);
            update = (ImageView) itemView.findViewById(R.id.update);
            delete = (ImageView) itemView.findViewById(R.id.delete);
        }

        public void bind(DataItem dataItem) {
            name.setText(dataItem.getName());
            contacName.setText(dataItem.getContact());
        }
    }

}
