package com.condordoc.doctor.adapter;

import android.view.View;

import com.condordoc.doctor.model.job.DataItem;

public interface OnJobActionListener {

    void jobAcceptByDoctor(View view, int pos, DataItem dataItem);

    void jobDeclineByDoctor(View view, int pos, DataItem dataItem);
}
