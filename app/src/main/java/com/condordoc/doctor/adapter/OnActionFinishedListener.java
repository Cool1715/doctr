package com.condordoc.doctor.adapter;

import android.view.View;

import com.condordoc.doctor.model.job.DataItem;

public interface OnActionFinishedListener {

    void onCompletedJob(View view, DataItem dataItem, int pos);
}
