package com.condordoc.doctor.adapter;

import android.view.View;

public interface OnPickImageListener {

    void onPickImage(View view);
}
