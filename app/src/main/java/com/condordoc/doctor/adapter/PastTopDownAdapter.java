package com.condordoc.doctor.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.ReceiptActivity;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.GetAddressUitls;

import java.util.Collections;
import java.util.List;


public class PastTopDownAdapter extends RecyclerView.Adapter<PastTopDownAdapter.BaseViewHolder> {

    private static final int EMPTY_VIEW = 1;
    private static final int ITEM_VIEW = 2;
    private Context fcontext;

    private List<DataItem> dataItemList;
    private final OnActionFinishedListener listener;

    public PastTopDownAdapter(Context fcontext, List<DataItem> dataItemList, OnActionFinishedListener listener) {
        this.dataItemList = dataItemList;
        this.fcontext = fcontext;
        Collections.reverse(this.dataItemList);
        this.listener = listener;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = null;

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case EMPTY_VIEW:
                v = inflater.inflate(R.layout.empty_item, viewGroup, false);
                return new EmptyRowViewHolder(v);
            case ITEM_VIEW:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemlayouforpastjobs, viewGroup, false);
                return new MainRowViewHolder(v);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder1, final int position) {

        if (holder1 instanceof MainRowViewHolder) {
            MainRowViewHolder holder = (MainRowViewHolder) holder1;

            holder.bind(dataItemList.get(position));

            try {
                if (dataItemList.get(position) != null)
                    if (dataItemList.get(position).getStatus() == 0) {
                        holder.status.setText("Pending");

                    } else if (dataItemList.get(position).getStatus() == 1) {
                        holder.status.setText("Accept");

                    } else if (dataItemList.get(position).getStatus() == 2) {
                        holder.status.setText("Denied");

                    } else if (dataItemList.get(position).getStatus() == 3) {
                        holder.status.setText("Completed");

                    } else if (dataItemList.get(position).getStatus() == 4) {
                        holder.status.setText("On Going");

                    } else if (dataItemList.get(position).getStatus() == 5) {
                        holder.status.setText("Cancelled");

                    } else {
                        holder.status.setVisibility(View.GONE);
                        holder.status.setText("");
                    }

                holder.status.setTag(position);
                holder.itemPastJobLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                listener.onCompletedJob(v, dataItemList.get(position), position);
                        Intent intent = new Intent( fcontext, ReceiptActivity.class );
                        int getId = dataItemList.get( position ).getId();
                        intent.putExtra( AppConstant.JOB_ID, getId);
                        fcontext.startActivity(intent);

                    }
                });
                
                
            } catch (Exception e) {
                e.printStackTrace();
                holder.status.setVisibility(View.VISIBLE);
            }
        }

    }

    public void onJobFinished(Context context, DataItem dataItem) {
        Intent intent = new Intent(context, ReceiptActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getItemViewType(int position) {
        if (dataItemList != null && dataItemList.size() == 0) {
            return EMPTY_VIEW;
        } else {
            return ITEM_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        assert dataItemList != null;
        if (dataItemList.size() == 0) {
            return 1;
        }
        return dataItemList.size();
    }

    public void updateList(List<DataItem> dataItems) {
        dataItemList = dataItems;
        Collections.reverse(this.dataItemList);
        notifyDataSetChanged();
    }

    public void clear() {
        dataItemList.clear();
        notifyDataSetChanged();
    }


    public class BaseViewHolder extends RecyclerView.ViewHolder {

        TextView time, header, patientAddress;

        public BaseViewHolder(View itemView) {
            super(itemView);
        }


    }


    public class EmptyRowViewHolder extends BaseViewHolder {

        TextView time, header, patientAddress;

        public EmptyRowViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class MainRowViewHolder extends BaseViewHolder {

        TextView headerprovider, status, patientAddress, header, time;
        LinearLayout itemPastJobLayout;

        public MainRowViewHolder(View itemView) {
            super(itemView);
            headerprovider = (TextView) itemView.findViewById(R.id.headerprovider);
            patientAddress = (TextView) itemView.findViewById(R.id.patientAddress);
            header = (TextView) itemView.findViewById(R.id.header);
            time = (TextView) itemView.findViewById(R.id.time);
            status = (TextView) itemView.findViewById(R.id.pastFragStatus);
            itemPastJobLayout = itemView.findViewById( R.id.itemPastJobLayout );
        }

        public void bind(DataItem dataItem) {
            headerprovider.setText(String.valueOf(dataItem.getId()));
            headerprovider.setText(String.valueOf(dataItem.getId()));
            time.setText(dataItem.getCreatedAt());
            header.setText(String.format("%s-%s", dataItem.getPatientName(), dataItem.getDiseaseName()));
            try {
                if (!TextUtils.isEmpty(dataItem.getPatientLat() + "") && !TextUtils.isEmpty(dataItem.getPatientLong() + "")) {
                    patientAddress.setText(GetAddressUitls.getCompleteAddressString(itemView.getContext(), dataItem.getPatientLat(), dataItem.getPatientLong()));
                } else patientAddress.setText("No Address");
            } catch (Exception e) {
                patientAddress.setText("No Address");
            }

        }
    }
}
