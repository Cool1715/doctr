package com.condordoc.doctor.adapter;

import android.view.View;

public interface OnPickChildImageListener {

    void onPickImageChild(View view, int pos);
}
