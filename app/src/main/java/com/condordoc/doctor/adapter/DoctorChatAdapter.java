package com.condordoc.doctor.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.condordoc.doctor.DoctorChatActivity;
import com.condordoc.doctor.R;
import com.condordoc.doctor.model.doctor_chat.Datum;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorChatAdapter extends RecyclerView.Adapter<DoctorChatAdapter.BaseViewholder> {

    public final static int PATIENT_CHAT = 0;
    public final static int DOCTOR_CHAT = 1;
    private List<Datum> doctorChatModels;
    OnBottomReachedListener onBottomReachedListener;

    public DoctorChatAdapter(List<Datum> patientChatModels){
        this.doctorChatModels = patientChatModels;
    }

    @NonNull
    @Override
    public BaseViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = null;

        switch (viewType){
            case PATIENT_CHAT:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_patient_chat_message, viewGroup,false);
                return new PatientChatViewholder(view);
            case DOCTOR_CHAT:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_doctor_chat_message, viewGroup,false);
                return new DoctorChatViewholder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewholder viewholder, int position) {

        if (position == doctorChatModels.size() - 1){

            onBottomReachedListener.onBottomReached(position);

        }
        switch (viewholder.getItemViewType()){
            case PATIENT_CHAT:
                PatientChatViewholder patientChatViewholder1 = (PatientChatViewholder) viewholder;
                patientChatViewholder1.bind(this.doctorChatModels.get(position));
                break;
            case DOCTOR_CHAT:
                DoctorChatViewholder doctorChatViewholder = (DoctorChatViewholder) viewholder;
                doctorChatViewholder.bind(this.doctorChatModels.get(position));
                break;
        }
    }



    @Override
    public int getItemCount() {
        return doctorChatModels != null ? doctorChatModels.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.doctorChatModels.get(position).getMessageD() != null){
            return DOCTOR_CHAT;
        } else if(this.doctorChatModels.get(position).getMessageP() != null){
            return PATIENT_CHAT;
        }
        return super.getItemViewType(position);
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){

        this.onBottomReachedListener = onBottomReachedListener;
    }

    public void addItems(List<Datum> data) {
        this.doctorChatModels = data;
        Collections.reverse(this.doctorChatModels);
        notifyDataSetChanged();
    }

    public void addItem(Datum item) {
        this.doctorChatModels.add(item);
        notifyDataSetChanged();
    }

    public class PatientChatViewholder extends BaseViewholder {
        CircleImageView patientImageView;
        TextView patientMessageTextView, patientName;

        public PatientChatViewholder(@NonNull View itemView) {
            super(itemView);
            patientMessageTextView = itemView.findViewById(R.id.patientMessageTextView);
            patientName = itemView.findViewById(R.id.patientName);
            patientImageView = itemView.findViewById(R.id.patientImageView);
        }

        @Override
        public void bind(Datum patientChatModel) {
            if (patientChatModel.getMessageP() != null)
                patientMessageTextView.setText(patientChatModel.getMessageP().toString());
            patientName.setText(patientChatModel.getPatientName() +"");
            // Glide Image downloading

            if (!TextUtils.isEmpty(DoctorChatActivity.PATIENT_IMAGE_URL_FULL)){
                Glide.with(itemView.getContext())
                        .load(DoctorChatActivity.PATIENT_IMAGE_URL_FULL)
                        .error(R.drawable.user)
                        .into(patientImageView);
            }
        }


    }

    public class DoctorChatViewholder extends BaseViewholder {
        CircleImageView doctorImageView;
        TextView messageTextView, docName;

        public DoctorChatViewholder(@NonNull View itemView) {
            super(itemView);
            messageTextView = itemView.findViewById(R.id.doctorMessageTextView);
            docName = itemView.findViewById(R.id.docName);
            doctorImageView = itemView.findViewById(R.id.docImageView);
        }

        @Override
        public void bind(Datum patientChatModel) {
            if (patientChatModel.getMessageD() != null)
                messageTextView.setText(patientChatModel.getMessageD());

            docName.setText(patientChatModel.getDoctorName() +"");
            doctorImageView.setImageResource(R.drawable.user);

            if (!TextUtils.isEmpty(DoctorChatActivity.DOCTOR_IMAGE_URL_FULL)){
                Glide.with(itemView.getContext())
                        .load(DoctorChatActivity.DOCTOR_IMAGE_URL_FULL)
                        .error(R.drawable.user)
                        .into(doctorImageView);
            }
            // Image Glide Libaray
        }


    }


    public abstract class BaseViewholder extends RecyclerView.ViewHolder{

        public BaseViewholder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void bind(Datum patientChatModel);
    }
}
