package com.condordoc.doctor.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.condordoc.doctor.R;

import java.io.File;
import java.util.List;

public class UploadImageAdapter extends RecyclerView.Adapter<UploadImageAdapter.MyViewHolder> {

    private final List<String> documentList;
    private final OnPickChildImageListener listener;

    private List<ChosenImage> chosenImages;
    private int pos;


    public UploadImageAdapter(List<String> documentList, OnPickChildImageListener listener) {
        this.documentList = documentList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.itemlayoutforuploaddocs, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, int position) {
        viewHolder.title.setText(documentList.get(position));
        if (chosenImages != null)
            viewHolder.bind();

        viewHolder.setListener(pos);
    }

    @Override
    public int getItemCount() {
        return documentList.size();
    }


    public void updateChildDocument(List<ChosenImage> chosenImages, int pos) {
        this.chosenImages = chosenImages;
        this.pos = pos;
        notifyItemChanged(pos);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView addDocument, addDocument2;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.text33);
            addDocument = (ImageView) itemView.findViewById(R.id.addDocument);
            addDocument2 = (ImageView) itemView.findViewById(R.id.addDocument2);


        }

        public void setListener(final int pos){
            addDocument2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPickImageChild(v, pos);
                }
            });
            addDocument.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPickImageChild(v, pos);
                }
            });
        }

        public void bind() {
            if (chosenImages != null && chosenImages.get(0) != null) {
                Glide.with(itemView.getContext())
                        .load(new File(chosenImages.get(0).getThumbnailPath()))
                        .into(addDocument);

            }
            if (chosenImages != null && chosenImages.get(1) != null)
                Glide.with(itemView.getContext()).load(new File(chosenImages.get(1).getThumbnailPath()))
                        .into(addDocument2);
        }


    }
/*
    class HorizatalRecyclerAdapter extends RecyclerView.Adapter<HorizatalRecyclerAdapter.ImageDocumentViewHolder> {
        List<ChosenImage> chosenImageList;
        private final OnPickImageListener listener;

        public HorizatalRecyclerAdapter(List<ChosenImage> chosenImages, OnPickImageListener listener) {
            chosenImageList = chosenImages;
            this.listener = listener;
        }

        @NonNull
        @Override
        public ImageDocumentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_documentaion, viewGroup, false);
            return new ImageDocumentViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ImageDocumentViewHolder viewHolder, int i) {
            switch (getItemViewType(i)){
                case 1:
//            viewHolder.bind(chosenImageList.get(i));
                    break;
                case 2:
            viewHolder.bind(chosenImageList.get(i), i);
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return chosenImageList.size() == 0 ? 2 : chosenImageList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return chosenImageList.size() == 0 ? 1 : 2;
        }

        public void addImages(List<ChosenImage> chosenImageList) {
            this.chosenImageList = chosenImageList;
            this.notifyDataSetChanged();
        }

        private class ImageDocumentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            ImageView addDocument;

            public ImageDocumentViewHolder(@NonNull View itemView) {
                super(itemView);
                addDocument = itemView.findViewById(R.id.addDocument);
                addDocument.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "Add Image", Toast.LENGTH_SHORT).show();
                listener.onPickImage(v);
            }

            public void bind(ChosenImage chosenImage, int pos){
//                profileImageChanged = ;
                Glide.with(itemView.getContext()).load(new File(chosenImage.getThumbnailPath())).into(addDocument);
//                addDocument.setImageDrawable(chosenImage.getThumbnailPath());
            }
        }

    }

*/
}

