package com.condordoc.doctor;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.model.bank.BankModel;
import com.condordoc.doctor.model.bank.Data;
import com.condordoc.doctor.model.bank.GetBankModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ValidateInputs;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
public class BankDetailActivity extends LocalizationActivity {

    private static final String TAG = BankDetailActivity.class.getSimpleName();
    private ApiManager apiManager;
    @BindView(R.id.input_layout_refralcode)
    TextInputLayout bankName;
    @BindView(R.id.account_number)
    TextInputLayout account_number;
    @BindView(R.id.bank_name)
    TextInputLayout bank_name;
    @BindView(R.id.bank_address)
    TextInputLayout bank_address;
    @BindView(R.id.ifsc)
    TextInputLayout ifsc;
    @BindView(R.id.input_referal)
    EditText accName;
    @BindView(R.id.input_bankno)
    EditText input_bankno;
    @BindView(R.id.input_bankname)
    EditText input_bankname;
    @BindView(R.id.input_bankaddress)
    EditText input_bankaddress;
    @BindView(R.id.input_bankifsc)
    EditText input_bankifsc;

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @Nullable
    @OnClick(R.id.continuess)
    void oncontinue() {
//        if (!validationField()) {
        if (!ValidateInputs.isValidName( accName.getText().toString().trim() )) {
            if (accName.getText().toString().isEmpty()) {
                accName.setError( "Please provide name" );
            }
        } else if (!ValidateInputs.isValidNumber( input_bankno.getText().toString().trim() )) {
            if (input_bankno.getText().toString().isEmpty()) {
                input_bankno.setError( "Please provide account number" );
            }
        } else if (!ValidateInputs.isValidInput( input_bankname.getText().toString().trim() )) {
            if (input_bankname.getText().toString().isEmpty()) {
                input_bankname.setError( "Please provide bank name" );
            }
        }else if (!ValidateInputs.isValidInput( input_bankaddress.getText().toString().trim() )) {
            if (input_bankaddress.getText().toString().isEmpty()) {
                input_bankaddress.setError( "Please provide bank address" );
            }
        } else if (!ValidateInputs.isValidInput( input_bankifsc.getText().toString().trim() )) {
            if (input_bankifsc.getText().toString().isEmpty()) {
                input_bankifsc.setError( "Please provide ifsc code" );
            }
        }  else {
            ProgressDialog pd = new ProgressDialog( BankDetailActivity.this );
            pd.show();
            pd.setMessage( "Loading..." );
            pd.setCancelable( false );
            String user_id = String.valueOf( AppPreferences.newInstance().getUserId( this ) );
            String accNumber = input_bankno.getText().toString().trim();
            String ifsc_code = input_bankifsc.getText().toString();
            String bank = input_bankname.getText().toString();
            String acc_holder_name = accName.getText().toString();
            String address = input_bankaddress.getText().toString();
            CommonUtils.showProgressDialog( getBaseContext() );
            apiManager.postBankDetails( user_id, accNumber, ifsc_code, bank, acc_holder_name, address )
                    .subscribeOn( Schedulers.io() )
                    .observeOn( AndroidSchedulers.mainThread() )
                    .subscribe( new SingleObserver<BankModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            pd.dismiss();

                        }

                        @Override
                        public void onSuccess(BankModel bankModel) {
                            pd.dismiss();
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            try {
                                if (bankModel != null && bankModel.getStatus().equalsIgnoreCase( "true" )) {
                                    Intent in = new Intent( BankDetailActivity.this, UploadDocumentActivity.class );
                                    startActivity( in );
                                    finish();
                                } else {
                                    Toast toast=Toast.makeText( BankDetailActivity.this, "Error", Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(BankDetailActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            } catch (Exception e) {
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            pd.dismiss();
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            try {
                               Toast toast= Toast.makeText( BankDetailActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(BankDetailActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            } catch (Exception ex) {
                            }
                        }
                    } );
        }
//        }
     /*   Intent in = new Intent(BankDetailActivity.this, UploadDocumentActivity.class);
        startActivity(in);
        finish();*/
    }
//    private boolean validationField() {
//        
//    }
    public static BankDetailActivity bankDetailActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_bank_detail );
        ButterKnife.bind( this );
        bankDetailActivity = BankDetailActivity.this;
        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this ) );
        apiManager = retrofit.create( ApiManager.class );
        CommonUtils.showProgressDialog( getBaseContext() );
        apiManager.getBackDetails()
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<GetBankModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(GetBankModel getBankModel) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        if (getBankModel != null && getBankModel.getStatus().equalsIgnoreCase( "true" )) {
                            Data item = getBankModel.getData();
                            if (item != null) {
                                input_bankno.setText( item.getAccNo() );
                                Log.d( TAG, "" + item.getAccNo() );
                                input_bankname.setText( item.getBank() );
                                Log.d( TAG, "" + item.getBank() );
                                input_bankaddress.setText( item.getBranch() );
                                Log.d( TAG, "" + item.getBranch() );
                                accName.setText( item.getAccHolderName() );
                                Log.d( TAG, "" + item.getAccHolderName() );
                                input_bankifsc.setText( item.getIfsc() );
                                Log.d( TAG, "" + item.getIfsc() );
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        e.printStackTrace();
                    }
                } );

    }
}
