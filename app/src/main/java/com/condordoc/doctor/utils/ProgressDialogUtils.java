package com.condordoc.doctor.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.TextView;

import com.condordoc.doctor.R;


public class ProgressDialogUtils {

    private static ProgressDialogUtils progressDialogUtils;
    private static Dialog dialog;

    private ProgressDialogUtils() {

    }

    public static ProgressDialogUtils newInstance(Context context) {
        if (progressDialogUtils == null) {
            dialog = new Dialog(context);
            progressDialogUtils= new ProgressDialogUtils();
            return progressDialogUtils;
        }
        return progressDialogUtils;
    }
    public void showDialog(String message) {
        try {
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.show_progress_loading);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            dialog.show();

            TextView loadingText = dialog.findViewById(R.id.loadingText);
            if (!TextUtils.isEmpty(message))
                loadingText.setText(message);
        }catch (Exception e){

        }
    }

    public void hideDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                } else {
                    dialog = null;
                }
            }
        } catch (Exception e) {
        }
    }
}
