package com.condordoc.doctor.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.TextView;

import com.condordoc.doctor.R;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;

import pl.kitek.timertextview.TimerTextView;

public class PushDialogViewUtils {

    Context context;
    private final JobByIdModel jobByIdModel;
    Dialog dialog;


    public PushDialogViewUtils(Context context, JobByIdModel jobByIdModel) {
        this.context = context;
        this.jobByIdModel = jobByIdModel;
    }

    public void createDialogBox(UserPushNotificationAction notificationAction) {

        try {

            dialog = new Dialog(context);
            // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        dialog.setCancelable(true);
        window.setGravity(Gravity.CENTER);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogforjobrequest);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        long futureTimestamp = System.currentTimeMillis() + (37000);
        TimerTextView timerText = (TimerTextView) dialog.findViewById(R.id.timerText);
        timerText.setEndTime(futureTimestamp);

        new CountDownTimer(37000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("CountDownTimer", "onTick: ");
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
                notificationAction.finishedActivity();

            }
        }.start();


        TextView doctorNameDiesaseName = dialog.findViewById(R.id.doctorNameDiesaseName);
        TextView patientName = dialog.findViewById(R.id.patientName);
        RatingBar patientRating = dialog.findViewById(R.id.patientRating);
        TextView pushAddress = dialog.findViewById(R.id.pushAddress);
        TextView diescription = dialog.findViewById(R.id.diescription);
        if (jobByIdModel.getData().getDoctorName() != null && jobByIdModel.getData().getDiseaseName() != null)
            doctorNameDiesaseName.setText(String.format("%s-%s", jobByIdModel.getData().getDoctorName(), jobByIdModel.getData().getDiseaseName()));

        try {
            pushAddress.setText(GetAddressUitls.getCompleteAddressString(context,Double.valueOf(jobByIdModel.getData().getPatientLat()),
                    Double.valueOf(jobByIdModel.getData().getPatientLong())));

        }catch (Exception e){
            pushAddress.setText("");
        }

        if (jobByIdModel.getData() != null && jobByIdModel.getData().getDescription() != null){
            diescription.setText(jobByIdModel.getData().getDescription());
        }

        if (jobByIdModel.getData() != null && jobByIdModel.getData().getPatientName() != null){
            patientName.setText(jobByIdModel.getData().getPatientName());
        }

        try {
            if (jobByIdModel.getData() != null && jobByIdModel.getData().getRating() != null){
                patientRating.setRating(Float.valueOf(jobByIdModel.getData().getRating() + ""));
            }
        }catch (Exception e){

        }


        TextView cancel = (TextView) dialog.findViewById(R.id.decline);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                notificationAction.acceptPushNotificationDialog(dialog, "Declined");

//                Toast.makeText(context, "Appointment cancelled.", Toast.LENGTH_SHORT).show();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.accept);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                notificationAction.acceptPushNotificationDialog(dialog, "Accepted");
//                Toast.makeText(context, "Appointment accepted", Toast.LENGTH_SHORT).show();

            }

        });
        }catch (Exception e){

        }


    }


    public boolean isShowing(){
        try {
            return  dialog != null && !dialog.isShowing();
        }catch (Exception e){
            return false;
        }
    }

    public interface UserPushNotificationAction {

        void acceptPushNotificationDialog(Dialog dialog, String message);

        void deniedPushNotificationDialog(Dialog dialog, String message);

        void finishedActivity();

    }
}
