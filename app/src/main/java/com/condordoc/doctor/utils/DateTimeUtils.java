package com.condordoc.doctor.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtils {
    private static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("dd/MM/yy HH:mm:ss");
    public static String getDateTime(String date){
        return DATE_FORMAT.format(date);
    }

    public static String getAgoTime(String createdDateString) {

//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 24-10-19 04:54:25
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date createdConvertedDate = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar start = Calendar.getInstance();
        start.setTime(createdConvertedDate);


        return TimeAgo.getTimeAgo(start.getTime().getTime());
    }
}
