package com.condordoc.doctor.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class AppPreferences {
    private static final String MY_PREFS_NAME = "urbe_shared_prefrences";
    private static final String TOKEN = "token";
    private static final String CURRENCY = "currency";
    private static final String LANGUAGE = "language";
    private static final String USER_ID = "user_id";
    private static final String DOCTOR_RADUIS = "doctor";
    private static final String LAT = "lat";
    private static final String LONG = "long";
    private static final String USER_TYPE = "user_type";
    private static final String USER_PROFILE = "profile";
    private static final String USER_PASS = "pass";
    private static final String JOB_NOTIFICATION_POP = "notification_id";
    private static final String GET_STATUS = "status";
    private static final String FTOKEN = "ftoken";
    private static final String REFERRAL_CODE = "referral_code";
    private static final String FILE1 = "file1";
    private static final String FILE2 = "file2";
    private static final String FILE3 = "file3";
    private static final String FILE4 = "file4";
    private static final String PATIENT_ID = "patient_id";
    private static final String LOGIN_IN = "login";
    private static final String DOCTOR_ONLINE = "online";

    private AppPreferences() {

    }

    public static AppPreferences newInstance() {
        AppPreferences appPreferences = new AppPreferences();
        return appPreferences;
    }

    public void setToken(Context context, String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.TOKEN, token);
        editor.apply();
    }

    public String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.TOKEN, "");
    }

    // Currency
    public void setCurrency(Context context, String currency) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.CURRENCY, currency);
        editor.apply();
    }

    public String getCurrency(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.CURRENCY, "");
    }

    // Language

    public void setLanguage(Context context, String language) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.LANGUAGE, language);
        editor.apply();
    }

    public String getLanguage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.LANGUAGE, "");
    }

    public void setUserId(Context context, int user_id) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(AppPreferences.USER_ID, user_id);
        editor.apply();
    }

    public int getUserId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getInt(AppPreferences.USER_ID, 0);
    }


    public void setDoctorRadius(Context context, Float valueOf) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putFloat(AppPreferences.DOCTOR_RADUIS, valueOf);
        editor.apply();
    }

    public Float getDoctorRadius(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getFloat(AppPreferences.USER_ID, 5);
    }

    public void setCurrentLocationLat(Context context, String latitude) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.LAT, latitude);
        editor.apply();
    }

    public String getCurrentLocationLat(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.LAT, "");
    }

    public void setCurrentLocationLong(Context context, String lng) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.LONG, lng);
        editor.apply();
    }

    public String getCurrentLocationLong(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.LONG, "");
    }

    public void setUserType(Context context, String user_type) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.USER_TYPE, user_type);
        editor.apply();
    }

    public String getUserType(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.USER_TYPE, "");
    }

    public void setProfilePath(Context context, String path) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.USER_PROFILE, path);
        editor.apply();
    }

    public String getProfilePath(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.USER_PROFILE, "");

    }

    public void setPassword(Context context, String path) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.USER_PASS, path);
        editor.apply();
    }

    public String getPassword(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.USER_PASS, "");

    }

    public void setJobNotificationPop(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.JOB_NOTIFICATION_POP, value);
        editor.apply();
    }

    public String getJobNotificationPop(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.JOB_NOTIFICATION_POP, "");
    }

    public String getStatus(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.GET_STATUS, "false");
    }

    public void setStatus(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.GET_STATUS, value);
        editor.apply();
    }

    public void setFirebaseToken(Context context, String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.FTOKEN, token);
        editor.apply();

    }

    public String getFirebaseToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.FTOKEN, "");
    }

    public String getReferralToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.REFERRAL_CODE, "");
    }

    public void setReferralToken(Context context, String value){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.REFERRAL_CODE, value);
        editor.apply();
    }

    public String getFile1(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.FILE1, "");
    }

    public void setFile1(Context context, String file) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.FILE1, file);
        editor.apply();
    }


    public String getFile2(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.FILE2, "");
    }

    public void setFile2(Context context, String file) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.FILE2, file);
        editor.apply();
    }

    public String getFile3(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.FILE3, "");
    }

    public void setFile3(Context context, String file) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.FILE3, file);
        editor.apply();
    }

    public String getFile4(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getString(AppPreferences.FILE4, "");
    }

    public void setFile4(Context context, String file) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(AppPreferences.FILE4, file);
        editor.apply();
    }

    public void setPatientId(Context context, int patientId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(AppPreferences.PATIENT_ID, patientId);
        editor.apply();
    }

    public int getPatientId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getInt(AppPreferences.PATIENT_ID, 0);
    }

    public void clear(Context context){
        try {
            SharedPreferences preferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.apply();
        }catch (Exception e){
            Log.d("AppSharedPreferences", "not clear data");
        }
    }

    public boolean isLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getBoolean(AppPreferences.LOGIN_IN, false);
    }

    public void setLoggedIn(Context context, boolean loginIn) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(AppPreferences.LOGIN_IN, loginIn);
        editor.apply();
    }

    public boolean isDoctorOnline(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        return sharedPreferences.getBoolean(AppPreferences.DOCTOR_ONLINE, false);
    }

    public void setDoctorOnline(Context context, boolean doctor_online) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(AppPreferences.DOCTOR_ONLINE, doctor_online);
        editor.apply();
    }
}
