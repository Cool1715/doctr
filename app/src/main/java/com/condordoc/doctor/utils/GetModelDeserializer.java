package com.condordoc.doctor.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.condordoc.doctor.model.schedule.DataItem;
import com.condordoc.doctor.model.schedule.GetScheduleModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetModelDeserializer implements JsonDeserializer<GetScheduleModel> {
    @Override
    public GetScheduleModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<DataItem> items = new ArrayList<>();

        final JsonObject jsonObject = json.getAsJsonObject();
        JsonArray itemsJsonArray = new JsonArray();
        try {
            itemsJsonArray = jsonObject.get("data").getAsJsonArray();
        }catch (Exception e){
            itemsJsonArray = new JsonArray();
            e.printStackTrace();
        }

        assert itemsJsonArray != null;
        for (JsonElement itemsJsonElement : itemsJsonArray) {


            final JsonObject itemJsonObject = itemsJsonElement.getAsJsonObject();
            final String address = itemJsonObject.get("address").getAsString();
            final String updated_at = itemJsonObject.get("updated_at").getAsString();
            final int user_id = itemJsonObject.get("user_id").getAsInt();
            final String price = itemJsonObject.get("price").getAsString();
//            final String verify = itemJsonObject.get("verify").getAsString() == null ? "" : "";
            final String created_at = itemJsonObject.get("created_at").getAsString();
            final String from = itemJsonObject.get("from").getAsString();
            final int id = itemJsonObject.get("id").getAsInt();
            final String to = itemJsonObject.get("to").getAsString();
            final String status = itemJsonObject.get("status").getAsString();
//            DataItem(String address, String updatedAt, int userId, String price, Object verify, String createdAt,
//            String from, int id, String to, String status) {
            items.add(new DataItem(address, updated_at, user_id,price,"",created_at,from, id, to,status));
        }

        return new GetScheduleModel(jsonObject.get("message").getAsString(),
                jsonObject.get("status").getAsString(),
                jsonObject.get("error").getAsString(), items);
    }
}