package com.condordoc.doctor.utils;

import com.condordoc.doctor.BuildConfig;

public class AppConstant {

    public static final String PERSON_NAME = "person_name";
    public static final String CURRENCY_SYMBOL = "\u0024";
    public static final String PERSON_GIVEN_NAME = "person_given_name";
    public static final String PERSON_FAMILY = "person_family_name";
    public static final String PERSON_EMAIL = "person_email";
    public static final String PERSON_ID = "person_id";
    public static final String PERSON_URL = "person_url";
    public static final String DAY = "day";
    public static final String JOB_TYPE = "job";

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID +".locationaddress";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    public static final String MANAGE_SERVICE_ID = "manage_service_id";
    public static final int PENDING_JOB = 0;
    public static final int ACCEPT_DOCTOR = 1;
    public static final int DENIED_DOCTOR = 2;
    public static final int CANCEL_DOCTOR = 5;
    public static final int COMPLETED_DOCTOR = 3;
    public static final int START_DOCTOR = 4;
    public static final int DOCTOR_ARRIVED = 7;
    public static final int DOCTOR_JOB_COMPLETED = 8;

    public static final String START_JOB_ID = "starting";
    public static final String ENGLISH = "en";
    public static final String ARBIC = "ar";
    public static final String SPANISH = "es";
    public static final String USD = "usd";
    public static final String AED = "aed";
    public static final String JOB_ID = "job_id";
    public static final String PATIENT_NAME = "patient_name";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String PERSON_PHONE = "phone";
    public static final String PRICE = "price";
    public static final String DISCOUNT = "discount";
    public static final String MISC_FEE = "misc_fee";
    public static final String FINAL_PRICE = "final_price";
}
