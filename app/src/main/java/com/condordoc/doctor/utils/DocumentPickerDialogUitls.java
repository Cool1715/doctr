package com.condordoc.doctor.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.condordoc.doctor.R;


public class DocumentPickerDialogUitls {

    static DocumentPickerDialogUitls dialogUitls;
    static Context mContext;

    private DocumentPickerDialogUitls() {

    }

    public static DocumentPickerDialogUitls newInstance(Context context) {
        mContext = context;
        if (dialogUitls == null) {
            dialogUitls = new DocumentPickerDialogUitls();
            return dialogUitls;
        }
        return dialogUitls;
    }

    public void showImagePicker(DocumentPickerOrGalleryImageListener imagePickerOrGalleryImageListener) {
        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.document_dialogue);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imagePickerOrGalleryImageListener.onCancel();
            }
        });
        TextView camera = (TextView) dialog.findViewById(R.id.camera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imagePickerOrGalleryImageListener.onOpenCamera(dialog, "Open Camera");
            }

        });

        TextView imageGallery = (TextView) dialog.findViewById(R.id.imageGallery);
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imagePickerOrGalleryImageListener.onImageGallery(dialog, "Open Gallery");
            }

        });
        TextView document = (TextView) dialog.findViewById(R.id.document);
        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imagePickerOrGalleryImageListener.onDocument(dialog, "Open Document");
            }

        });

    }

    public interface DocumentPickerOrGalleryImageListener {

        void onOpenCamera(Dialog dialog, String message);

        void onImageGallery(Dialog dialog, String message);

        void onDocument(Dialog dialog, String message);

        void onCancel();

    }
}

