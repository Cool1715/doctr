package com.condordoc.doctor;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.astuetz.PagerSlidingTabStrip;
import com.condordoc.doctor.fragment.PastFragment;
import com.condordoc.doctor.fragment.PendingFragment;
import com.condordoc.doctor.fragment.UpcomingFragment;
import com.condordoc.doctor.utils.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YourJobsActivity extends LocalizationActivity {

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @BindView(R.id.tabs)
    PagerSlidingTabStrip tabStrip;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_jobs);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        tabStrip.setShouldExpand(true);
        tabStrip.setAllCaps(true);
        tabStrip.setIndicatorHeight(2);

        tabStrip.setViewPager(viewPager);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tabStrip.setTextColor(getColor(R.color.lightblack));
            tabStrip.setIndicatorColor(getColor(R.color.lightblack));
            tabStrip.setBackgroundColor(getColor(R.color.white));
            tabStrip.setDividerColor(getColor(R.color.white));
        }
        if (intent.getExtras() != null && intent.getExtras().getInt(AppConstant.JOB_TYPE) == 1){
            viewPager.setCurrentItem(0);
        } else if(intent.getExtras() != null && intent.getExtras().getInt(AppConstant.JOB_TYPE) == 2) {
            viewPager.setCurrentItem(1,true);
        } else {
            viewPager.setCurrentItem(0);
        }
    }


    class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final String[] TITLES = {"PENDING","UPCOMING","PAST"};


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:

                    return PendingFragment.newInstance("FirstFragment, Instance 1");

                case 1:

                    return UpcomingFragment.newInstance("SecondFragment, Instance 1");

                case 2:

                    return PastFragment.newInstance("FirstFragment, Instance 1");

                default:
                    return null;

            }
        }
    }
}
