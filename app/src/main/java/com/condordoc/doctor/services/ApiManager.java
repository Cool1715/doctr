package com.condordoc.doctor.services;

import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.model.bank.BankModel;
import com.condordoc.doctor.model.bank.GetBankModel;
import com.condordoc.doctor.model.currency.CurrencyModel;
import com.condordoc.doctor.model.disease.DiseaseModel;
import com.condordoc.doctor.model.disease.DisesaePostModel;
import com.condordoc.doctor.model.disease_doctor.Disease;
import com.condordoc.doctor.model.disease_doctor.DiseaseModelGet;
import com.condordoc.doctor.model.disease_doctor.DisesaeDoctorPostModel;
import com.condordoc.doctor.model.disease_doctor.DoctorGetModel;
import com.condordoc.doctor.model.doctor_chat.DoctorChatModel;
import com.condordoc.doctor.model.doctor_chat.StoreChatModel;
import com.condordoc.doctor.model.document.DocoumentGetModel;
import com.condordoc.doctor.model.document.DocumentModel;
import com.condordoc.doctor.model.emergency.CreateEmergencyModel;
import com.condordoc.doctor.model.emergency.DeleteEmergencyModel;
import com.condordoc.doctor.model.emergency.GetEmergencyModel;
import com.condordoc.doctor.model.emergency.UpdateEmergencyModel;
import com.condordoc.doctor.model.fake.FakeModel;
import com.condordoc.doctor.model.fcm.FCMTokenModel;
import com.condordoc.doctor.model.feedback.FeedbackGetModel;
import com.condordoc.doctor.model.feedback.FeedbackModel;
import com.condordoc.doctor.model.feedback.RatingGet;
import com.condordoc.doctor.model.job.AcceptJobModel;
import com.condordoc.doctor.model.job.CancelJobModel;
import com.condordoc.doctor.model.job.CompleteJobModel;
import com.condordoc.doctor.model.job.DeniedJobModel;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.model.job.JobGetModel;
import com.condordoc.doctor.model.job.MiscPaymentModel;
import com.condordoc.doctor.model.job.StartJobModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdStatus;
import com.condordoc.doctor.model.language.LanguageModel;
import com.condordoc.doctor.model.location.SetLocationModel;
import com.condordoc.doctor.model.manage.ManageService;
import com.condordoc.doctor.model.menu.MenusModel;
import com.condordoc.doctor.model.schedule.GetScheduleModel;
import com.condordoc.doctor.model.schedule.MultipleScheduleModel;
import com.condordoc.doctor.model.user.ApiResponse;
import com.condordoc.doctor.model.user.description.DescriptionModel;
import com.condordoc.doctor.model.user.description.DescriptionModelPost;
import com.condordoc.doctor.model.user.logout.LogoutModel;
import com.condordoc.doctor.model.user.password.ForgetPasswordModel;
import com.condordoc.doctor.model.user.password.ResetPasswordModel;
import com.condordoc.doctor.model.user.profile.MyProfileModel;
import com.condordoc.doctor.model.user.profile.UpdateProfileModel;
import com.condordoc.doctor.model.user.refresh.RefreshTokenModel;
import com.condordoc.doctor.model.user.sigin.sigin.SignInModel;
import com.condordoc.doctor.model.user.siginup.SignUpModel;
import com.condordoc.doctor.model.tnc.TNCModel;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Url;


public interface ApiManager {


    // register
    @FormUrlEncoded
    @POST(APIURL.REGISTER)
    Single<SignUpModel> register(@Field("first_name") String firstName, @Field("last_name")
            String lastName, @Field("email") String emaildId, @Field("user_type")
                                         String user_type, @Field("password") String password, @Field("mobile")
                                         String phoneNumber);

    // API_AUTH_LOGIN
    @FormUrlEncoded
    @POST(APIURL.API_AUTH_LOGIN)
    Single<SignInModel> login(@Field("email") String email, @Field("password") String password,@Field("user_type") String user_type);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(APIURL.API_AUTH_REFRESH)
    Single<RefreshTokenModel> refreshToken();

    // Logout
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(APIURL.API_AUTH_LOGOUT)
    Single<LogoutModel> logout();

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(APIURL.API_AUTH_ME)
    Single<MyProfileModel> myProfile();


    @GET
    Call<JobByIdStatus> calledJobByIdStatus(@Url String url);

    @POST("misc-payment")
    Call<MiscPaymentModel> calledMsgDetailApi(@Body JsonObject jsonObject);


        /*first_name:Doctor
    last_name:Demo
    email:skbhati199@gmail.com
    password:123456
    mobile:123456
    user_type:Doctor
    image:*/

    @Multipart
    @POST(APIURL.UPDATE_PROFILE)
    Single<UpdateProfileModel> updateProfile(@PartMap() Map<String, RequestBody> partMap,
                                             @Part MultipartBody.Part image);

    @GET(APIURL.MENU)
    Single<MenusModel> getMenuItems();

    // GET Disease
    @GET(APIURL.DISEASE_GET)
    Single<DiseaseModel> getDisease();

    // Post Disease
    @POST(APIURL.DISEASE_POST)
    Single<DisesaePostModel> postDisease();

    // GET Doctor Disease
    @GET(APIURL.DOCTOR_DISEASE_GET)
    Single<DoctorGetModel> getDoctorDisease();

    // Post Doctor Disease
    @POST(APIURL.DOCTOR_DISEASE_POST)
    Single<DisesaeDoctorPostModel> postDoctorDisease();

    // Schedule Get
    @GET(APIURL.SCHEDULE_GET)
    Single<GetScheduleModel> getSchedule();

    // Schedule Get
    @GET(APIURL.SCHEDULE_DATE_GET)
    Single<GetScheduleModel> getScheduleByDate(@Path("date") String date);

//    user_id:2
//    from:11:00
//    to:13:00
//    day:Monday
//    date:12 August
//    address:delhi
//    price:2000
//    status:new
// Schedule Post

    @FormUrlEncoded
    @POST(APIURL.SCHEDULE_POST)
    Single<MultipleScheduleModel> postSchedule(@Field("user_id") String userId,
                                               @Field("from") String from,
                                               @Field("to") String to,
                                               @Field("day") String day,
                                               @Field("date") String date,
                                               @Field("address") String address,
                                               @Field("price") String price,
                                               @Field("status") String status);

    @FormUrlEncoded
    @POST(APIURL.SCHEDULE_UPDATE)
    Single<MultipleScheduleModel> updateSchedule(@Path("id") String id,
                                                 @Field("user_id") String userId,
                                                 @Field("from") String from,
                                                 @Field("to") String to,
                                                 @Field("day") String day,
                                                 @Field("date") String date,
                                                 @Field("address") String address,
                                                 @Field("price") String price,
                                                 @Field("status") String status
    );
    @FormUrlEncoded
    @POST(APIURL.Location_UPDATE)
    Single<SetLocationModel> updateLocation(@Field("doctor_lat") String lat,
                                            @Field("doctor_long") String longi

    );
    @FormUrlEncoded
    @POST(APIURL.SCHEDULE_MULTIPLE_POST)
    Single<MultipleScheduleModel> postMultipleSchedule(@Field("data") String dataJson);


    @FormUrlEncoded
    @POST(APIURL.SCHEDULE_DEL_MULTIPLE_POST)
    Single<MultipleScheduleModel> postDelMultipleSchedule(@Field("data") String dataJson);


    @GET(APIURL.POST_DISEASE_DOCTOR)
    Single<DiseaseModelGet> getDiseaseDoctor();


    @FormUrlEncoded
    @POST(APIURL.POST_DISEASE_DOCTOR)
    Single<DisesaeDoctorPostModel> setService(@Field("disease_id") String diease_id);

    // Feedback Post
//    @FormUrlEncoded
    @GET(APIURL.FEEDBACK_GET)
    Single<List<FeedbackGetModel>> getFeedbackModel(@Path("id") int patient_id);

    @GET(APIURL.FEEDBACK_GET)
    Single<RatingGet> getRatingModel(@Path("id") int patient_id);
    // Feedback Get
    @FormUrlEncoded
    @POST(APIURL.FEEDBACK_POST)
    Single<FeedbackModel> postFeedbackModel(@Field("doctor_id") int doctor_id,
                                            @Field("patient_id") int patient_id,
                                            @Field("feedback") String feedback,
                                            @Field("rating") float rating);

//     Jobs
//    @GET(APIURL.JOBS_GET)
//    Single<> // ERROR


    @GET(APIURL.GET_CURRENCY)
    Single<CurrencyModel> getCurrency();


    @GET(APIURL.DOCTOR_DESCRIPTION)
    Single<DescriptionModel> getDescription();


    @FormUrlEncoded
    @POST(APIURL.DOCTOR_DESCRIPTION)
    Single<DescriptionModelPost> setDescription(@Field("description") String descri
    );




    @GET(APIURL.GET_LANGUAGE)
    Single<LanguageModel> getLanguage();


    @GET(APIURL.tnc) // Error
    Single<TNCModel> getTNC();

    @GET(APIURL.about) // Error
    Single<TNCModel> getAbout();
    @GET(APIURL.privacy) // Error
    Single<TNCModel> getPrivacy();
    @GET(APIURL.contact) // Error
    Single<TNCModel> getContact();
    @GET(APIURL.faq) // Error
    Single<TNCModel> getFaq();


    //    user_id:3
//    name:Deepak
//    email:gusaindeepak@gmail.com
//    contact:123456789
    @FormUrlEncoded
    @POST(APIURL.POST_EMERGENCY)
    Single<CreateEmergencyModel> processEmergency(@Field("user_id") int user_id,
                                                  @Field("name") String name,
                                                  @Field("email") String email,
                                                  @Field("contact") String contact);

    @GET(APIURL.GET_EMERGENCY)
    Single<GetEmergencyModel> getEmergency();


    @FormUrlEncoded
    @POST(APIURL.UPDATE_EMERGENCY)
    Single<UpdateEmergencyModel> updateEmergency(@Path("id") int path,
                                                 @Field("user_id") int user_id,
                                                 @Field("name") String name,
                                                 @Field("email") String email,
                                                 @Field("contact") String contact);

    @DELETE(APIURL.DELETE_EMERGENCY)
    Single<DeleteEmergencyModel> deleteEmergency(@Path("id") int id);


    //    user_id:2
//    acc_no:123456789
//    ifsc:1234567
//    bank:syndicate
//    mobile:123456789
//    branch:delhi


    /* user_id:2
     acc_no:123456789
     ifsc:1234567
     bank:syndicate
 //mobile:123456789
     branch:delhi
     acc_holder_name:Sonu Kumar*/
    @FormUrlEncoded
    @POST(APIURL.POST_BANK)
    Single<BankModel> postBankDetails(@Field("user_id") String user_id,
                                      @Field("acc_no") String acc_no,
                                      @Field("ifsc") String ifsc,
                                      @Field("bank") String bank,
                                      @Field("acc_holder_name") String mobile,
                                      @Field("branch") String address);

    //
    @GET(APIURL.GET_BANK)
    Single<GetBankModel> getBackDetails();

    // done
    @Multipart
    @POST(APIURL.POST_DOCUMENT)
    Observable<DocumentModel> postDocument(
            @Part MultipartBody.Part filePart1,
            @Part MultipartBody.Part filePart2,
            @Part("doc_type") RequestBody doc_type);


    // Pending
    @GET(APIURL.GET_DOCUMENT)
    Single<DocoumentGetModel> getDocument();


   /* @GET(APIURL.JOB_ACCEPT_DOCTOR)
    Single<AcceptJobModel> jobAcceptedByDoctor(@Path("id") String id);


    @GET(APIURL.JOB_DENIED_DOCTOR)
    Single<DeniedJobModel> jobDeniedByDoctor(@Path("id") String id);


    @GET(APIURL.JOB_COMPLETED_DOCTOR)
    Single<CompleteJobModel> jobCompletedByDoctor(@Path("id") String id);

    @GET(APIURL.JOB_ACCEPTED_BUT_CANCEL_DOCTOR)
    Single<CancelJobModel> jobCancelByDoctor(@Path("id") String id);

    @GET(APIURL.JOB_START_DOCTOR)
    Single<StartJobModel> jobStartByDoctor(@Path("id") String id);*/


    @GET(APIURL.JOBS_GET)
    Observable<GetJobModel> getJobs();

    @GET(APIURL.JOBS_GET_BY_ID)
    Observable<JobByIdModel> getJobById(@Path("id") String id);


    @GET(APIURL.getAcceptJobs)
    Observable<GetJobModel> getjobAcceptedDoctor();

    @GET(APIURL.getPendingJobs)
    Observable<JobGetModel> getJobPendingDoctor();


    @GET(APIURL.getJobDeniedDoctor)
    Observable<JobGetModel> getJobDeniedDoctor();


    @GET(APIURL.getJobStarted)
    Observable<JobGetModel> getJobStarted();


    @GET(APIURL.getJobCompletedDoctor)
    Observable<JobGetModel> getJobCompletedDoctor();


    /*Route::get('setjobAcceptedDoctor/{id}','doctor\DoctorJobController@setJobAccepted');
Route::get('setJobDeniedDoctor/{id}','doctor\DoctorJobController@setJobDenied');
Route::get('setJobCompletedDoctor/{id}','doctor\DoctorJobController@setJobCompleted');
Route::get('setJobStarted/{id}','doctor\DoctorJobController@setJobStated');
Route::get('setJobAcceptedButCancel/{id}','doctor\DoctorJobController@setJobAcceptedButCancel');
*/

    @GET(APIURL.SET_JOB_ACCEPTED_DOCTOR)
    Single<AcceptJobModel> setjobAcceptedDoctor(@Path("id") String id);

    @GET(APIURL.SET_DOC_ARRIVED)
    Single<AcceptJobModel> setDocArrived(@Path("id") String id);

    @GET(APIURL.SET_DOC_BEGIN_JOB)
    Single<AcceptJobModel> setDocBeginJob(@Path("id") String id);

    @GET(APIURL.SET_JOB_DENIED_DOCTOR)
    Single<DeniedJobModel> setJobDeniedDoctor(@Path("id") String id);

    @GET(APIURL.SET_JOB_COMPLETED_DOCTOR)
    Single<CompleteJobModel> setJobCompletedDoctor(@Path("id") String id);


    @GET(APIURL.SET_JOB_STARTED)
    Single<StartJobModel> setJobStarted(@Path("id") String id);

    @GET(APIURL.SET_JOB_ACCEPTED_BUT_CANCEL)
    Single<CancelJobModel> setJobAcceptedButCancel(@Path("id") String id);

    @FormUrlEncoded
    @POST(APIURL.PASSWORD_CHAMGE)
    Single<ResetPasswordModel> passwordChange(@Field("o_password") String oldPassword, @Field("n_password") String newPassword);

    @GET(APIURL.FORGOT_PASSWORD)
    Single<ForgetPasswordModel> forgotPassword(@Path("email") String emaild);

    @FormUrlEncoded
    @POST(APIURL.store_chat)
    Single<StoreChatModel> sendMessage(@Field("user_id")String patient_id,
                                       @Field("message") String message_d);


    @FormUrlEncoded
    @POST(APIURL.chat_get)
    Single<DoctorChatModel> getDoctorMessage(@Field("user_id") String patient_id);

    @GET(APIURL.GET_DISEASE)
    Single<DiseaseModel> getDiseaseService(@Path("manage_service_id") int id);

    // Not created
    @FormUrlEncoded
    @POST(APIURL.doctor_chat)
    Single<String> sendOtp(@Field("") int mobileNumber);

    @FormUrlEncoded
    @POST(APIURL.doctor_chat)
    Single<String> verfiyingOtpCode(int code);

    @FormUrlEncoded
    @POST(APIURL.GET_RECEIPT)
    Single<String> downloadReceipt(int userId, int job_id);

//    @GET(APIURL.JOBS_ID)
//    Single<GetJobModel> getJobById(int jobId);

    @FormUrlEncoded
    @POST(APIURL.SET_LOCATION)
    Single<SetLocationModel> setLocation(@Field("user_lat") String lnt, @Field("user_long") String lng);

    @GET(APIURL.MANAGE_SERIVCE)
    Single<ManageService> getManageService();

    @FormUrlEncoded
    @POST(APIURL.FIREBASE_TOKEN)
    Single<FCMTokenModel> sendFCMToekn(@Field("token") String token);

    //    @FormUrlEncoded
    @GET(APIURL.FAKE_EMAIL_ID)
    Single<FakeModel> fakeVerify(@Path("emailId") String emailId);  //TODO


    @FormUrlEncoded
    @POST(APIURL.google)
    Single<SignInModel> googleLogin(@Field("token") String token, @Field("user_type") String userType);

    @FormUrlEncoded
    @POST(APIURL.facebook)
    Single<SignInModel> facebookLogin(@Field("token") String token, @Field("user_type") String userType);



    @FormUrlEncoded
    @POST(APIURL.postWithdrawRequest)
    Single<ApiResponse> withdrawRequest(@Field("amount") String amount);


}
