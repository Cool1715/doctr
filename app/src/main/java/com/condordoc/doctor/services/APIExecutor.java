package com.condordoc.doctor.services;

import android.content.Context;

import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.utils.AppPreferences;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("ALL")
public class APIExecutor {

    private ApiManager apiService;
    public static ApiManager getApiService(Context context) {
      //  String base_url="http://api.urbesalud.com/api/auth/jobs/";
        String base_url= APIURL.BASE_URL+ "/api/auth/";
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Authorization", "Bearer "+ AppPreferences.newInstance().getToken(context))
                                .method(original.method(), original.body());
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .connectTimeout(10000, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(base_url).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        return retrofit.create(ApiManager.class);

    }
}
