package com.condordoc.doctor;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.condordoc.doctor.model.job.AcceptJobModel;
import com.condordoc.doctor.model.job.CancelJobModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.PushDialogViewUtils;

import androidx.core.content.ContextCompat;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DialogJobsActivity extends AppCompatActivity {

    private static final String TAG = DialogJobsActivity.class.getSimpleName();
    ApiManager manager;
    PushDialogViewUtils pushDialogViewUtils;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        manager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);


        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.START_JOB_ID)){
            String jobId = getIntent().getStringExtra(AppConstant.START_JOB_ID);

            getJobId(jobId);

        } else {
            Toast toast=Toast.makeText(this, "Notification", Toast.LENGTH_SHORT);
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
            toast.show();
        }



    }


    @Override
    protected void onResume() {
        super.onResume();
        if (pushDialogViewUtils != null && !pushDialogViewUtils.isShowing()){
            finish();
        }
    }

    private void getJobId(String jobId) {

        manager.getJobById(jobId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<JobByIdModel>() {
            @Override
            public void accept(JobByIdModel jobByIdModel) throws Exception {

                try {
                    if (jobByIdModel != null && jobByIdModel.getStatus().equalsIgnoreCase("true")) {
                        showDialog(jobByIdModel);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (jobByIdModel != null && jobByIdModel.getError() != null) {
                                    Toast toast = Toast.makeText(DialogJobsActivity.this, "" + jobByIdModel.getError(), Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                                else if (jobByIdModel != null && jobByIdModel.getMessage() != null){
                                   Toast toast= Toast.makeText(DialogJobsActivity.this, "" + jobByIdModel.getMessage(), Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }
                        }, 2000);
                    }
                }catch (Exception e){
                    Toast toast=Toast.makeText(DialogJobsActivity.this, "Somethig went worng", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                    toast.show();
                    finish();
                }


            }


        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Toast toast=Toast.makeText(DialogJobsActivity.this, "Something went worng", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                toast.show();
                finish();
            }
        });
    }


    private void showDialog(JobByIdModel jobByIdModel) {
        pushDialogViewUtils = new PushDialogViewUtils(this, jobByIdModel);
        pushDialogViewUtils.createDialogBox(new PushDialogViewUtils.UserPushNotificationAction() {
            @Override
            public void acceptPushNotificationDialog(Dialog dialog, String message) {
                CommonUtils.showProgressDialog(getBaseContext());
                manager.setjobAcceptedDoctor(AppPreferences.newInstance().getJobNotificationPop(DialogJobsActivity.this)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<AcceptJobModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(AcceptJobModel cancelJobModel) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                            Intent in = new Intent(DialogJobsActivity.this, JobMapUserActivity.class);
                            in.putExtra(AppConstant.START_JOB_ID, AppPreferences.newInstance().getJobNotificationPop(DialogJobsActivity.this));
                            startActivity(in);
                        } catch (Exception e) {
                            Toast toast=Toast.makeText(DialogJobsActivity.this, "Something went worng.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        finish();
                    }
                });

            }

            @Override
            public void deniedPushNotificationDialog(Dialog dialog, String message) {
                CommonUtils.showProgressDialog(getBaseContext());
                manager.setJobAcceptedButCancel(AppPreferences.newInstance().getJobNotificationPop(DialogJobsActivity.this)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<CancelJobModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(CancelJobModel cancelJobModel) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        if (cancelJobModel != null && cancelJobModel.getStatus().equalsIgnoreCase("true")) {
                            Toast toast=Toast.makeText(DialogJobsActivity.this, "Appointment Cancelled.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(DialogJobsActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        finish();
                    }
                });

            }

            @Override
            public void finishedActivity() {
                finish();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.getExtras() != null && intent.getExtras().containsKey(AppConstant.START_JOB_ID)){
            String jobId = getIntent().getStringExtra(AppConstant.START_JOB_ID);
            getJobId(jobId);
            Log.d(TAG, "Intent ");
        }
    }
}
