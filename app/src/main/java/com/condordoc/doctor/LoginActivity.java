package com.condordoc.doctor;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.condordoc.doctor.others.CommonUtils;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.condordoc.doctor.facebook.FacebookHelper;
import com.condordoc.doctor.facebook.FacebookListener;
import com.condordoc.doctor.fcm.FCMHandler;
import com.condordoc.doctor.model.user.password.ForgetPasswordModel;
import com.condordoc.doctor.model.user.sigin.sigin.SignInModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.google.android.material.textfield.TextInputLayout;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.core.content.ContextCompat;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static com.condordoc.doctor.utils.ValidateInputs.isValidPassword;

public class LoginActivity extends BaseActivity implements FacebookListener {
    private static final int RC_SIGN_IN = 100;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText inputEmail, inputPassword;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword;
    private LinearLayout btnSignIn;
    FacebookHelper facebookHelper;
    private GoogleSignInClient mGoogleSignInClient;
    GoogleSignInOptions gso;
    private ApiManager apiManager;
    private AccessToken accessToken;


    @OnClick(R.id.forgot_password)
    void forgotPassword() {

        showForgotPasswordDialog();

    }

    @OnClick(R.id.register_show)
    void showRegister() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
//        finish();
    }


    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    @Nullable
    @OnClick(R.id.googleLogIn)
    void onGoogleLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Nullable
    @OnClick(R.id.facebookLogin)
    void onFacebook() {
        facebookHelper.performSignIn(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        accessToken = AccessToken.getCurrentAccessToken();
        ButterKnife.bind(this);
        Retrofit retrofit = DoctorApp.retrofit(null);
        apiManager = retrofit.create(ApiManager.class);
        facebookHelper = new FacebookHelper(this);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.client_deep))
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        btnSignIn = (LinearLayout) findViewById(R.id.btn_signin);

        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new MyTextWatcher(inputPassword));

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        printHashKey(this);

        if (accessToken != null)
            Log.d("Facebook", accessToken.getToken());


    }


    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);

    }

    private void updateUI(GoogleSignInAccount account) {

        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(AppConstant.PERSON_NAME, personName);
            intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
            intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
            intent.putExtra(AppConstant.PERSON_EMAIL, personEmail);
            intent.putExtra(AppConstant.PERSON_ID, personId);
            intent.putExtra(AppConstant.PERSON_URL, personPhoto.toString());
            startActivity(intent);
            finish();
            
            
            apiManager.googleLogin(account.getIdToken(),"doctor")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<SignInModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(SignInModel signInModel) {
                            if (signInModel != null) {

                                if (signInModel.getStatus().equalsIgnoreCase("true")) {
                                    String personName = account.getDisplayName();
                                    String personGivenName = account.getGivenName();
                                    String personFamilyName = account.getFamilyName();
                                    String personEmail = account.getEmail();
                                    String personId = account.getId();
                                    Uri personPhoto = account.getPhotoUrl();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.putExtra(AppConstant.PERSON_NAME, personName);
                                    intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
                                    intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
                                    intent.putExtra(AppConstant.PERSON_EMAIL, personEmail);
                                    intent.putExtra(AppConstant.PERSON_ID, personId);
                                    intent.putExtra(AppConstant.PERSON_URL, personPhoto.toString());
                                    Log.d("Google Token", "" + account.getIdToken());

                                    FCMHandler fcmHandler = new FCMHandler();
                                    fcmHandler.enableFCM();
                                    Log.d(TAG, signInModel.getData().getOriginal().getAccessToken());
                                    AppPreferences.newInstance().setLoggedIn(LoginActivity.this, true);
                                    AppPreferences.newInstance().setToken(LoginActivity.this, signInModel.getData().getOriginal().getAccessToken());
                                    Toast toast=Toast.makeText(getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                    startActivity(intent);
                                    try {
                                        IntroActivity.introActivity.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                } else {
                                    Toast toast=Toast.makeText(LoginActivity.this, "" + signInModel.getError(), Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }

                        }
                        @Override
                        public void onError(Throwable e) {
                            Toast toast=Toast.makeText(LoginActivity.this, "Server Not responding", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                            toast.show();

                        }
                    });
        }
    }

    // TODO
    private void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);

        TextInputLayout input_layout_refralcode = (TextInputLayout) dialog.findViewById(R.id.input_layout_refralcode);
//        TextInputLayout input_layout_cpass = (TextInputLayout) dialog.findViewById(R.id.input_layout_cpass);
//        TextInputLayout input_layout_confirm = (TextInputLayout) dialog.findViewById(R.id.input_layout_confirm);

        EditText input_current_pass = (EditText) dialog.findViewById(R.id.input_referal);
//        EditText input_new_pass = (EditText) dialog.findViewById(R.id.input_newpass);
//        EditText input_confirm_pass = (EditText) dialog.findViewById(R.id.input_confirm);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(LoginActivity.this);
                dialog.dismiss();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.viewongng);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = input_current_pass.getText().toString().trim();

                if (email.isEmpty() || !isValidEmail(email)) {
                    input_current_pass.setError(getString(R.string.err_msg_email));
                    requestFocus(input_current_pass);
                }else {
                    hideKeyboard( LoginActivity.this );
                    dialog.dismiss();
                    boolean isValid = validateForm( input_current_pass, input_layout_refralcode );
                    if (isValid)
                        apiManager.forgotPassword( email )
                                .subscribeOn( Schedulers.io() )
                                .observeOn( AndroidSchedulers.mainThread() )
                                .subscribe( new SingleObserver<ForgetPasswordModel>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {
                                    }

                                    @Override
                                    public void onSuccess(ForgetPasswordModel s) {
                                        try {
                                            if (s != null) {
                                                if (s.getStatus().equalsIgnoreCase( "true" )) {
                                                    Toast toast=Toast.makeText( LoginActivity.this, "Password Changed.", Toast.LENGTH_SHORT );
                                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                                    toast.show();
                                                } else {
                                                    String messsage = s.getMessage();
                                                    Toast toast=Toast.makeText( LoginActivity.this, "" + messsage, Toast.LENGTH_LONG );
                                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                                    toast.show();
                                                }
                                            }
                                        } catch (Exception e) {
                                            Toast toast=Toast.makeText( LoginActivity.this, "Something went wrong.", Toast.LENGTH_SHORT );
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                            toast.show();
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Toast toast=Toast.makeText( LoginActivity.this, "Something went wrong.", Toast.LENGTH_SHORT );
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                        toast.show();
                                    }
                                } );
                }
            }

        });
    }

    private boolean validateForm(EditText input_current, TextInputLayout inputcurrentLayout) {
        if (!isValidEmail(input_current.getText().toString().trim())) {
            inputcurrentLayout.setError(getString(R.string.invalid_nick_name));
            return false;
        } else {
            return true;
        }
    }

    private void submitForm() {


        if (!isNetworkConnected()) {
           Toast toast= Toast.makeText(this, getString(R.string.no_internet_connected) + "", Toast.LENGTH_SHORT);
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
            toast.show();
            return;
        }
        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }
        ProgressDialog pd=new ProgressDialog(LoginActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        CommonUtils.showProgressDialog(getApplicationContext());
        findViewById(R.id.register_show).setEnabled(false);
        apiManager.login(email, password,"Doctor")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<SignInModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        pd.dismiss();

                    }

                    @Override
                    public void onSuccess(SignInModel signInModel){
                       CommonUtils.disMissProgressDialog(getBaseContext());
                        findViewById(R.id.register_show).setEnabled(true);
                        pd.dismiss();
                        try {
                            if (signInModel != null ){
                                if (signInModel.getStatus().equalsIgnoreCase("true")) {
                                    FCMHandler fcmHandler = new FCMHandler();
                                    fcmHandler.enableFCM();
                                    Log.d(TAG, signInModel.getData().getOriginal().getAccessToken());
                                    AppPreferences.newInstance().setLoggedIn(LoginActivity.this, true);
                                    AppPreferences.newInstance().setToken(LoginActivity.this, signInModel.getData().getOriginal().getAccessToken());
                                    Intent in = new Intent(LoginActivity.this, MainActivity.class);
                                    Toast toast=Toast.makeText(LoginActivity.this, "Successfully logged in", Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                    startActivity(in);
                                    finish();
                                }else {
                                    String message = signInModel.getMessage();
                                    String error =signInModel.getError();
                                    Toast toast=Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }else  {
                                Toast toast=Toast.makeText(LoginActivity.this, "Something went wrong.", Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast toast=Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        findViewById(R.id.register_show).setEnabled(true);
                        pd.dismiss();

                        try {
                            Toast toast=Toast.makeText(LoginActivity.this, "Email/Password Not Recognized", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                            toast.show();

                        }catch (Exception ex){

                        }

                    }
                });
    }

    private boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(inputEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(inputPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return true;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onFbSignInFail(String errorMessage) {
        Log.d(TAG, "onFbSignInFail: " + errorMessage);
    }

    @Override
    public void onFbSignInSuccess(String id, String name, String email, String phone, String imgUrl, String token) {
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//        Toast.makeText(getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT).show();

        Log.d("facebook token ", token);


        apiManager.facebookLogin(token,"doctor")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<SignInModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(SignInModel signInModel) {
                        try {
                            if (signInModel != null) {

                                if(signInModel.getStatus().equalsIgnoreCase("true")) {

                                    Log.d(TAG, "onFbSignInSuccess: " + name + " " );

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.putExtra(AppConstant.PERSON_NAME, name);
                                    //        intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
                                    //        intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
                                    intent.putExtra(AppConstant.PERSON_EMAIL, email);
                                    intent.putExtra(AppConstant.PERSON_ID, id);
                                    intent.putExtra(AppConstant.PERSON_URL, imgUrl);
                                    startActivity(intent);
                                    Log.d("Facebook Token Doctor ", "" + token);
                                    FCMHandler fcmHandler = new FCMHandler();
                                    fcmHandler.enableFCM();
                                    AppPreferences.newInstance().setLoggedIn(LoginActivity.this, true);

                                    Log.d(TAG, signInModel.getData().getOriginal().getAccessToken());
                                    AppPreferences.newInstance().setToken(LoginActivity.this, signInModel.getData().getOriginal().getAccessToken());

                                   Toast toast= Toast.makeText(getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                    startActivity(intent);
                                    try {
                                        IntroActivity.introActivity.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    finish();
                                } else {
                                    Toast toast=Toast.makeText(LoginActivity.this, "" + signInModel.getError(), Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }
                        }catch (Exception e){
                            Toast toast=Toast.makeText(LoginActivity.this, "Something went wrong.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(LoginActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void onFBSignOut() {
        Log.d(TAG, "onFBSignOut: ");
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }


    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        facebookHelper.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }


    private void refreshIdToken() {
        // Attempt to silently refresh the GoogleSignInAccount. If the GoogleSignInAccount
        // already has a valid token this method may complete immediately.
        //
        // If the user has not previously signed in on this device or the sign-in has expired,
        // this asynchronous branch will attempt to sign in the user silently and get a valid
        // ID token. Cross-device single sign on will occur in this branch.
        mGoogleSignInClient.silentSignIn()
                .addOnCompleteListener(this, new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        handleSignInResult(task);
                    }
                });
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
//            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            assert account != null;
            String idToken = account.getIdToken();
            Log.d(TAG, "" + idToken);
            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            e.printStackTrace();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }
}

