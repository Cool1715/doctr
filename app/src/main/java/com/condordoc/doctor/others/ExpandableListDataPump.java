package com.condordoc.doctor.others;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> cricket = new ArrayList<String>();
        cricket.add("Test 1 Answer");


        List<String> football = new ArrayList<String>();
        football.add("Test 2 Answer");


        List<String> basketball = new ArrayList<String>();
        basketball.add("Test 3 Answer");


        expandableListDetail.put("Test 1", cricket);
        expandableListDetail.put("Test 2", football);
        expandableListDetail.put("Test 3", basketball);
        return expandableListDetail;
    }
}

