package com.condordoc.doctor.model.user.profile;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("image")
	private String image;

	@SerializedName("balance")
	private String balance;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("refer_code")
	private String referCode;

	@SerializedName("refer_token")
	private String referToken;

	@SerializedName("verified")
	private Boolean verified;



	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}


	public void setVerify(Boolean veri){
		this.verified = veri;
	}

	public Boolean getVerified(){
		return verified;
	}

	public void setReferCode(String referCode){
		this.referCode = referCode;
	}

	public String getReferCode(){
		return referCode;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"image = '" + image + '\'' + 
			",user_type = '" + userType + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",balance = '" + balance + '\'' +
			",last_name = '" + lastName + '\'' +
			",id = '" + id + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",email = '" + email + '\'' + 
			",refer_code = '" + referCode + '\'' +
					",verified = '" + verified + '\'' +
					"}";
		}

	public String getReferToken() {
		return referToken;
	}

	public void setReferToken(String referToken) {
		this.referToken = referToken;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
}