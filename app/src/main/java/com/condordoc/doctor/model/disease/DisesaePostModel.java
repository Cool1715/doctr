package com.condordoc.doctor.model.disease;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisesaePostModel {



    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
