package com.condordoc.doctor.model.disease_doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorGetModel {


    @SerializedName("doctor_id")
    @Expose
    private Integer doctorId;
    @SerializedName("doctor_name")
    @Expose
    private String doctorName;
    @SerializedName("diseases")
    @Expose
    private List<Disease> diseases = null;

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public List<Disease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Disease> diseases) {
        this.diseases = diseases;
    }

}
