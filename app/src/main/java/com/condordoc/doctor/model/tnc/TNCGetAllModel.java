package com.condordoc.doctor.model.tnc;

import com.google.gson.annotations.SerializedName;

public class TNCGetAllModel {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("tnc")
	private String tnc;

	@SerializedName("type")
	private String type;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setTnc(String tnc){
		this.tnc = tnc;
	}

	public String getTnc(){
		return tnc;
	}

	public void setType(String tnc){
		this.type = tnc;
	}

	public String getType(){
		return type;
	}
	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"TNCGetAllModel{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",tnc = '" + tnc + '\'' +
					",type = '" + type + '\'' +
					",created_at = '" + createdAt + '\'' +
			",id = '" + id + '\'' + 
			"}";
		}
}