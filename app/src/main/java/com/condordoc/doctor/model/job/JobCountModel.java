package com.condordoc.doctor.model.job;

public class JobCountModel {

    int pendingJob;
    int upcomingJob;

    public JobCountModel(int pendingJob, int upcomingJob) {
        this.pendingJob = pendingJob;
        this.upcomingJob = upcomingJob;
    }

    public int getPendingJob() {
        return pendingJob;
    }

    public void setPendingJob(int pendingJob) {
        this.pendingJob = pendingJob;
    }

    public int getUpcomingJob() {
        return upcomingJob;
    }

    public void setUpcomingJob(int upcomingJob) {
        this.upcomingJob = upcomingJob;
    }
}
