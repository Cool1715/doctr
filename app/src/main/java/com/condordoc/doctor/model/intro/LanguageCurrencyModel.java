package com.condordoc.doctor.model.intro;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LanguageCurrencyModel{

	@SerializedName("datacurrency")
	private List<DatacurrencyItem> datacurrency;

	@SerializedName("datalanguage")
	private List<DatalanguageItem> datalanguage;

	@SerializedName("message")
	private String message;

	@SerializedName("error")
	private String error;

	@SerializedName("status")
	private String status;

	public LanguageCurrencyModel(List<DatacurrencyItem> datacurrency, List<DatalanguageItem> datalanguage) {
		this.datacurrency = datacurrency;
		this.datalanguage = datalanguage;
	}

	public void setDatacurrency(List<DatacurrencyItem> datacurrency){
		this.datacurrency = datacurrency;
	}

	public List<DatacurrencyItem> getDatacurrency(){
		return datacurrency;
	}

	public void setDatalanguage(List<DatalanguageItem> datalanguage){
		this.datalanguage = datalanguage;
	}

	public List<DatalanguageItem> getDatalanguage(){
		return datalanguage;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setError(String error){
		this.error = error;
	}

	public String getError(){
		return error;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LanguageCurrencyModel{" + 
			"datacurrency = '" + datacurrency + '\'' + 
			",datalanguage = '" + datalanguage + '\'' + 
			",message = '" + message + '\'' + 
			",error = '" + error + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}