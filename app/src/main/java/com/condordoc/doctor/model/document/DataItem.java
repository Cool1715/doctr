package com.condordoc.doctor.model.document;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("file2")
	private String file2;

	@SerializedName("file1")
	private String file1;

	@SerializedName("doc_type")
	private String docType;

	@SerializedName("status")
	private int status;

	public void setFile2(String file2){
		this.file2 = file2;
	}

	public String getFile2(){
		return file2;
	}

	public void setFile1(String file1){
		this.file1 = file1;
	}

	public String getFile1(){
		return file1;
	}

	public void setDocType(String docType){
		this.docType = docType;
	}

	public String getDocType(){
		return docType;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"file2 = '" + file2 + '\'' + 
			",file1 = '" + file1 + '\'' + 
			",doc_type = '" + docType + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}