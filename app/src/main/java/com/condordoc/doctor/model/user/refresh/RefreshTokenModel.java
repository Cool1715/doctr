package com.condordoc.doctor.model.user.refresh;

import com.google.gson.annotations.SerializedName;

public class RefreshTokenModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("token_type")
	private String tokenType;

	@SerializedName("expires_in")
	private long expiresIn;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setTokenType(String tokenType){
		this.tokenType = tokenType;
	}

	public String getTokenType(){
		return tokenType;
	}

	public void setExpiresIn(long expiresIn){
		this.expiresIn = expiresIn;
	}

	public long getExpiresIn(){
		return expiresIn;
	}

	@Override
 	public String toString(){
		return 
			"RefreshTokenModel{" + 
			"access_token = '" + accessToken + '\'' + 
			",token_type = '" + tokenType + '\'' + 
			",expires_in = '" + expiresIn + '\'' + 
			"}";
		}
}