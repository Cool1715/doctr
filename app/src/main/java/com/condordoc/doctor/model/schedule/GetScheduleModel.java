package com.condordoc.doctor.model.schedule;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetScheduleModel{

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("error")
	private String error;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public GetScheduleModel(String message, String status, String error, List<DataItem> dataItems){
		this.message = message;
		this.error = error;
		this.status = status;
		this.data = dataItems;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setError(String error){
		this.error = error;
	}

	public String getError(){
		return error;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetScheduleModel{" + 
			"data = '" + data + '\'' + 
			",error = '" + error + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}