package com.condordoc.doctor.model.schedule;

import com.google.gson.annotations.SerializedName;

public class ScheduleItem{



	@SerializedName("address")
	private String address;

	@SerializedName("price")
	private String price;

	@SerializedName("from")
	private String from;

	@SerializedName("to")
	private String to;

	@SerializedName("status")
	private String status;

	public ScheduleItem(String address, String price, String from, String to, String status) {
		this.address = address;
		this.price = price;
		this.from = from;
		this.to = to;
		this.status = status;
	}
	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setFrom(String from){
		this.from = from;
	}

	public String getFrom(){
		return from;
	}

	public void setTo(String to){
		this.to = to;
	}

	public String getTo(){
		return to;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ScheduleItem{" + 
			"address = '" + address + '\'' + 
			",price = '" + price + '\'' + 
			",from = '" + from + '\'' + 
			",to = '" + to + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}