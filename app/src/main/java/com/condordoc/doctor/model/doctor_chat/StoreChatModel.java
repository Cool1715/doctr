package com.condordoc.doctor.model.doctor_chat;

import com.google.gson.annotations.SerializedName;

public class StoreChatModel{

	@SerializedName("message")
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"StoreChatModel{" + 
			"message = '" + message + '\'' + 
			"}";
		}
}