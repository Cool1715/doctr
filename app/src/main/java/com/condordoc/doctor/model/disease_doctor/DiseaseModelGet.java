package com.condordoc.doctor.model.disease_doctor;


import com.condordoc.doctor.model.disease.DataItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DiseaseModelGet {

    @SerializedName("data")
    private DoctorGetModel data;

    @SerializedName("message")
    private String message;

    @SerializedName("error")
    private String error;

    @SerializedName("status")
    private String status;

    public void setData(DoctorGetModel data){
        this.data = data;
    }

    public DoctorGetModel getData(){
        return data;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setError(String error){
        this.error = error;
    }

    public String getError(){
        return error;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "DiseaseModel{" +
                        "data = '" + data + '\'' +
                        ",message = '" + message + '\'' +
                        ",error = '" + error + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}