package com.condordoc.doctor.model.disease;



import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DiseaseModel {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("error")
    private String error;

    @SerializedName("status")
    private String status;



    public void setData(List<DataItem> data){
        this.data = data;
    }

    public List<DataItem> getData(){
        return data;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setError(String error){
        this.error = error;
    }

    public String getError(){
        return error;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }



    @Override
    public String toString(){
        return
                "DiseaseModel{" +
                        "data = '" + data + '\'' +
                        ",message = '" + message + '\'' +
                        ",error = '" + error + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}