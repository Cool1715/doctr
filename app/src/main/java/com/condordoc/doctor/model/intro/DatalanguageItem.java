package com.condordoc.doctor.model.intro;

import com.google.gson.annotations.SerializedName;

public class DatalanguageItem{

	@SerializedName("string_value")
	private String stringValue;

	@SerializedName("lang_code")
	private String langCode;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("string_name")
	private String stringName;

	public void setStringValue(String stringValue){
		this.stringValue = stringValue;
	}

	public String getStringValue(){
		return stringValue;
	}

	public void setLangCode(String langCode){
		this.langCode = langCode;
	}

	public String getLangCode(){
		return langCode;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStringName(String stringName){
		this.stringName = stringName;
	}

	public String getStringName(){
		return stringName;
	}

	@Override
 	public String toString(){
		return 
			"DatalanguageItem{" + 
			"string_value = '" + stringValue + '\'' + 
			",lang_code = '" + langCode + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",string_name = '" + stringName + '\'' + 
			"}";
		}
}