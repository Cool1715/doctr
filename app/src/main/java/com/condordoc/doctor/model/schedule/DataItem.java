package com.condordoc.doctor.model.schedule;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("address")
	private String address;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;



	@SerializedName("price")
	private String price;

	@SerializedName("verify")
	private String verify;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("from")
	private String from;

	@SerializedName("id")
	private int id;

	@SerializedName("to")
	private String to;

	@SerializedName("status")
	private String status;

	public DataItem(String address, String updatedAt, int userId, String price, String verify, String createdAt, String from, int id, String to, String status) {
		this.address = address;
		this.updatedAt = updatedAt;
		this.userId = userId;
		this.price = price;
		this.verify = verify;
		this.createdAt = createdAt;
		this.from = from;
		this.id = id;
		this.to = to;
		this.status = status;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setVerify(String verify){
		this.verify = verify;
	}

	public String getVerify(){
		return verify;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setFrom(String from){
		this.from = from;
	}

	public String getFrom(){
		return from;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTo(String to){
		this.to = to;
	}

	public String getTo(){
		return to;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"address = '" + address + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",price = '" + price + '\'' + 
			",verify = '" + verify + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",from = '" + from + '\'' + 
			",id = '" + id + '\'' + 
			",to = '" + to + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}