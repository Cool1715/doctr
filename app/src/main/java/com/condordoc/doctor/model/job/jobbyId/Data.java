package com.condordoc.doctor.model.job.jobbyId;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("doctor_image")
	private String image;


	@SerializedName("image_patient")
	private String imagePatient;


	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("promo_code")
	private String promoCode;

	@SerializedName("patient_long")
	private String patientLong;

	@SerializedName("doctor_long")
	private String doctorLong;

	@SerializedName("from_time")
	private String fromTime;

	@SerializedName("doctor_id")
	private int doctorId;

	@SerializedName("date_of_time")
	private String dateOfTime;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("patient_id")
	private int patientId;

	@SerializedName("disease_id")
	private int diseaseId;

	@SerializedName("price")
	private String price;

	@SerializedName("patient_name")
	private String patientName;

	@SerializedName("id")
	private int id;

	@SerializedName("disease_name")
	private String diseaseName;

	@SerializedName("date_of_booking")
	private String dateOfBooking;

	@SerializedName("doctor_name")
	private String doctorName;

	@SerializedName("to_time")
	private String toTime;

	@SerializedName("patient_lat")
	private String patientLat;

	@SerializedName("payment_method")
	private String paymentMethod;

	@SerializedName("doctor_lat")
	private Object doctorLat;

	@SerializedName("status")
	private int status;

	@SerializedName("doctor_number")
	private String phone;

	@SerializedName("patient_number")
	private String patientNumber;

	@SerializedName("rating")
	private String rating;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setPromoCode(String promoCode){
		this.promoCode = promoCode;
	}

	public Object getPromoCode(){
		return promoCode;
	}

	public void setPatientLong(String patientLong){
		this.patientLong = patientLong;
	}

	public String getPatientLong(){
		return patientLong;
	}

	public void setDoctorLong(String doctorLong){
		this.doctorLong = doctorLong;
	}

	public Object getDoctorLong(){
		return doctorLong;
	}

	public void setFromTime(String fromTime){
		this.fromTime = fromTime;
	}

	public String getFromTime(){
		return fromTime;
	}

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setDateOfTime(String dateOfTime){
		this.dateOfTime = dateOfTime;
	}

	public String getDateOfTime(){
		return dateOfTime;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPatientId(int patientId){
		this.patientId = patientId;
	}

	public int getPatientId(){
		return patientId;
	}

	public void setDiseaseId(int diseaseId){
		this.diseaseId = diseaseId;
	}

	public int getDiseaseId(){
		return diseaseId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setPatientName(String patientName){
		this.patientName = patientName;
	}

	public String getPatientName(){
		return patientName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setDiseaseName(String diseaseName){
		this.diseaseName = diseaseName;
	}

	public String getDiseaseName(){
		return diseaseName;
	}

	public void setDateOfBooking(String dateOfBooking){
		this.dateOfBooking = dateOfBooking;
	}

	public String getDateOfBooking(){
		return dateOfBooking;
	}

	public void setDoctorName(String doctorName){
		this.doctorName = doctorName;
	}

	public String getDoctorName(){
		return doctorName;
	}

	public void setToTime(String toTime){
		this.toTime = toTime;
	}

	public String getToTime(){
		return toTime;
	}

	public void setPatientLat(String patientLat){
		this.patientLat = patientLat;
	}

	public String getPatientLat(){
		return patientLat;
	}

	public void setPaymentMethod(String paymentMethod){
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentMethod(){
		return paymentMethod;
	}

	public void setDoctorLat(Object doctorLat){
		this.doctorLat = doctorLat;
	}

	public Object getDoctorLat(){
		return doctorLat;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		 this.phone = phone;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"image = '" + image + '\'' + 
			",description = '" + description + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",promo_code = '" + promoCode + '\'' + 
			",patient_long = '" + patientLong + '\'' + 
			",doctor_long = '" + doctorLong + '\'' + 
			",from_time = '" + fromTime + '\'' + 
			",doctor_id = '" + doctorId + '\'' + 
			",date_of_time = '" + dateOfTime + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",patient_id = '" + patientId + '\'' + 
			",disease_id = '" + diseaseId + '\'' + 
			",price = '" + price + '\'' + 
			",patient_name = '" + patientName + '\'' + 
			",id = '" + id + '\'' + 
			",disease_name = '" + diseaseName + '\'' + 
			",date_of_booking = '" + dateOfBooking + '\'' + 
			",doctor_name = '" + doctorName + '\'' + 
			",to_time = '" + toTime + '\'' + 
			",patient_lat = '" + patientLat + '\'' + 
			",payment_method = '" + paymentMethod + '\'' + 
			",doctor_lat = '" + doctorLat + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

		public void setRating(String rating){
		this.rating = rating;
		}

    public String  getRating() {
        return rating;
    }

	public String getImagePatient() {
		return imagePatient;
	}

	public void setImagePatient(String imagePatient) {
		this.imagePatient = imagePatient;
	}

	public String getPatientNumber() {
		return patientNumber;
	}

	public void setPatientNumber(String patientNumber) {
		this.patientNumber = patientNumber;
	}
}