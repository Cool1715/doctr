package com.condordoc.doctor.model.schedule;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RequestScheduleModel{

	@SerializedName("schedule")
	private List<ScheduleItem> schedule;

	public void setSchedule(List<ScheduleItem> schedule){
		this.schedule = schedule;
	}

	public List<ScheduleItem> getSchedule(){
		return schedule;
	}

	@Override
 	public String toString(){
		return 
			"RequestScheduleModel{" + 
			"schedule = '" + schedule + '\'' + 
			"}";
		}
}