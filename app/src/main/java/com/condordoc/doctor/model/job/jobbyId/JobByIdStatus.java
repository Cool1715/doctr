package com.condordoc.doctor.model.job.jobbyId;

import java.io.Serializable;
import java.util.List;

public class JobByIdStatus implements Serializable {


    /**
     * status : true
     * message :
     * error :
     * data : {"id":155,"doctor_id":51,"patient_id":59,"disease_id":2,"doctor_name":"arvind doctor","disease_name":"Flu","patient_name":"arvind patients","date_of_booking":"2020-2-14","date_of_time":"17:00-17:30","from_time":"17:00","to_time":"17:30","promo_code":null,"description":"hdhehhehehshshhebeb","payment_method":"online","price":"200","patient_lat":"28.6081213","patient_long":"77.3719107","doctor_lat":null,"doctor_long":null,"status":4,"created_at":"2020-02-14 09:17:49","updated_at":"2020-02-14 11:05:30","patient_image":"591903726895_17baed51-2f5a-4b07-918c-eadd45f8fa43-scale-1.jpeg","doctor_image":"51940724745_8ff44f03-9f78-4b50-a038-522708407d07-scale-2.jpeg","patient_number":"9907559343","doctor_number":"+919907559","misc_fee":0,"material_fee":0,"doctor_discount":0,"extra_id":0,"job_updates":[{"id":64,"job_id":155,"job_update":"New Appointment","created_at":"2020-02-14 09:17:51","updated_at":"2020-02-14 09:17:51"},{"id":73,"job_id":155,"job_update":"Your doctor has accepted the appointment.","created_at":"2020-02-14 10:41:02","updated_at":"2020-02-14 10:41:02"},{"id":74,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 10:41:08","updated_at":"2020-02-14 10:41:08"},{"id":75,"job_id":155,"job_update":"Your doctor has reached the location.","created_at":"2020-02-14 10:41:49","updated_at":"2020-02-14 10:41:49"},{"id":76,"job_id":155,"job_update":"Your doctor has completed the job.","created_at":"2020-02-14 10:41:59","updated_at":"2020-02-14 10:41:59"},{"id":79,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 11:05:32","updated_at":"2020-02-14 11:05:32"},{"id":80,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 11:12:57","updated_at":"2020-02-14 11:12:57"}]}
     */

    private String status;
    private String message;
    private String error;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 155
         * doctor_id : 51
         * patient_id : 59
         * disease_id : 2
         * doctor_name : arvind doctor
         * disease_name : Flu
         * patient_name : arvind patients
         * date_of_booking : 2020-2-14
         * date_of_time : 17:00-17:30
         * from_time : 17:00
         * to_time : 17:30
         * promo_code : null
         * description : hdhehhehehshshhebeb
         * payment_method : online
         * price : 200
         * patient_lat : 28.6081213
         * patient_long : 77.3719107
         * doctor_lat : null
         * doctor_long : null
         * status : 4
         * created_at : 2020-02-14 09:17:49
         * updated_at : 2020-02-14 11:05:30
         * patient_image : 591903726895_17baed51-2f5a-4b07-918c-eadd45f8fa43-scale-1.jpeg
         * doctor_image : 51940724745_8ff44f03-9f78-4b50-a038-522708407d07-scale-2.jpeg
         * patient_number : 9907559343
         * doctor_number : +919907559
         * misc_fee : 0
         * material_fee : 0
         * doctor_discount : 0
         * extra_id : 0
         * job_updates : [{"id":64,"job_id":155,"job_update":"New Appointment","created_at":"2020-02-14 09:17:51","updated_at":"2020-02-14 09:17:51"},{"id":73,"job_id":155,"job_update":"Your doctor has accepted the appointment.","created_at":"2020-02-14 10:41:02","updated_at":"2020-02-14 10:41:02"},{"id":74,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 10:41:08","updated_at":"2020-02-14 10:41:08"},{"id":75,"job_id":155,"job_update":"Your doctor has reached the location.","created_at":"2020-02-14 10:41:49","updated_at":"2020-02-14 10:41:49"},{"id":76,"job_id":155,"job_update":"Your doctor has completed the job.","created_at":"2020-02-14 10:41:59","updated_at":"2020-02-14 10:41:59"},{"id":79,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 11:05:32","updated_at":"2020-02-14 11:05:32"},{"id":80,"job_id":155,"job_update":"Your appointment has started now.","created_at":"2020-02-14 11:12:57","updated_at":"2020-02-14 11:12:57"}]
         */

        private int id;
        private int doctor_id;
        private int patient_id;
        private int disease_id;
        private String doctor_name;
        private String disease_name;
        private String patient_name;
        private String date_of_booking;
        private String date_of_time;
        private String from_time;
        private String to_time;
        private Object promo_code;
        private String description;
        private String payment_method;
        private String price;
        private String patient_lat;
        private String patient_long;
        private Object doctor_lat;
        private Object doctor_long;
        private int status;
        private String created_at;
        private String updated_at;
        private String patient_image;
        private String doctor_image;
        private String patient_number;
        private String doctor_number;
        private int misc_fee;
        private int material_fee;
        private int doctor_discount;
        private int extra_id;
        private List<JobUpdatesBean> job_updates;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(int doctor_id) {
            this.doctor_id = doctor_id;
        }

        public int getPatient_id() {
            return patient_id;
        }

        public void setPatient_id(int patient_id) {
            this.patient_id = patient_id;
        }

        public int getDisease_id() {
            return disease_id;
        }

        public void setDisease_id(int disease_id) {
            this.disease_id = disease_id;
        }

        public String getDoctor_name() {
            return doctor_name;
        }

        public void setDoctor_name(String doctor_name) {
            this.doctor_name = doctor_name;
        }

        public String getDisease_name() {
            return disease_name;
        }

        public void setDisease_name(String disease_name) {
            this.disease_name = disease_name;
        }

        public String getPatient_name() {
            return patient_name;
        }

        public void setPatient_name(String patient_name) {
            this.patient_name = patient_name;
        }

        public String getDate_of_booking() {
            return date_of_booking;
        }

        public void setDate_of_booking(String date_of_booking) {
            this.date_of_booking = date_of_booking;
        }

        public String getDate_of_time() {
            return date_of_time;
        }

        public void setDate_of_time(String date_of_time) {
            this.date_of_time = date_of_time;
        }

        public String getFrom_time() {
            return from_time;
        }

        public void setFrom_time(String from_time) {
            this.from_time = from_time;
        }

        public String getTo_time() {
            return to_time;
        }

        public void setTo_time(String to_time) {
            this.to_time = to_time;
        }

        public Object getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(Object promo_code) {
            this.promo_code = promo_code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPatient_lat() {
            return patient_lat;
        }

        public void setPatient_lat(String patient_lat) {
            this.patient_lat = patient_lat;
        }

        public String getPatient_long() {
            return patient_long;
        }

        public void setPatient_long(String patient_long) {
            this.patient_long = patient_long;
        }

        public Object getDoctor_lat() {
            return doctor_lat;
        }

        public void setDoctor_lat(Object doctor_lat) {
            this.doctor_lat = doctor_lat;
        }

        public Object getDoctor_long() {
            return doctor_long;
        }

        public void setDoctor_long(Object doctor_long) {
            this.doctor_long = doctor_long;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getPatient_image() {
            return patient_image;
        }

        public void setPatient_image(String patient_image) {
            this.patient_image = patient_image;
        }

        public String getDoctor_image() {
            return doctor_image;
        }

        public void setDoctor_image(String doctor_image) {
            this.doctor_image = doctor_image;
        }

        public String getPatient_number() {
            return patient_number;
        }

        public void setPatient_number(String patient_number) {
            this.patient_number = patient_number;
        }

        public String getDoctor_number() {
            return doctor_number;
        }

        public void setDoctor_number(String doctor_number) {
            this.doctor_number = doctor_number;
        }

        public int getMisc_fee() {
            return misc_fee;
        }

        public void setMisc_fee(int misc_fee) {
            this.misc_fee = misc_fee;
        }

        public int getMaterial_fee() {
            return material_fee;
        }

        public void setMaterial_fee(int material_fee) {
            this.material_fee = material_fee;
        }

        public int getDoctor_discount() {
            return doctor_discount;
        }

        public void setDoctor_discount(int doctor_discount) {
            this.doctor_discount = doctor_discount;
        }

        public int getExtra_id() {
            return extra_id;
        }

        public void setExtra_id(int extra_id) {
            this.extra_id = extra_id;
        }

        public List<JobUpdatesBean> getJob_updates() {
            return job_updates;
        }

        public void setJob_updates(List<JobUpdatesBean> job_updates) {
            this.job_updates = job_updates;
        }

        public static class JobUpdatesBean {
            /**
             * id : 64
             * job_id : 155
             * job_update : New Appointment
             * created_at : 2020-02-14 09:17:51
             * updated_at : 2020-02-14 09:17:51
             */

            private int id;
            private int job_id;
            private String job_update;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getJob_id() {
                return job_id;
            }

            public void setJob_id(int job_id) {
                this.job_id = job_id;
            }

            public String getJob_update() {
                return job_update;
            }

            public void setJob_update(String job_update) {
                this.job_update = job_update;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
