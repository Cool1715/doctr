package com.condordoc.doctor.model.job;

import java.io.Serializable;

public class MiscPaymentModel implements Serializable {

    /**
     * status : 200
     * message : the Service is created Successfully
     * data :
     * error :
     */

    private String status;
    private String message;
    private String data;
    private String error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
