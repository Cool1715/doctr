package com.condordoc.doctor.model.feedback;

import com.condordoc.doctor.model.user.raring.RatingItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingGet {

	@SerializedName("data")
	private List<FeedbackGetModel> data;



	public void setData(List<FeedbackGetModel> data){
		this.data = data;
	}

	public List<FeedbackGetModel> getData(){
		return data;
	}



	@Override
 	public String toString(){
		return 
			"LogoutModel{" +
			"data = '" + data + '\'' + 

			"}";
		}
}