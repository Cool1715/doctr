package com.condordoc.doctor.model.user.sigin.sigin;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("exception")
	private Object exception;

	@SerializedName("headers")
	private Headers headers;

	@SerializedName("original")
	private Original original;

	public void setException(Object exception){
		this.exception = exception;
	}

	public Object getException(){
		return exception;
	}

	public void setHeaders(Headers headers){
		this.headers = headers;
	}

	public Headers getHeaders(){
		return headers;
	}

	public void setOriginal(Original original){
		this.original = original;
	}

	public Original getOriginal(){
		return original;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"exception = '" + exception + '\'' + 
			",headers = '" + headers + '\'' + 
			",original = '" + original + '\'' + 
			"}";
		}
}