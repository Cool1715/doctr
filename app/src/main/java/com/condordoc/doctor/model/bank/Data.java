package com.condordoc.doctor.model.bank;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("acc_holder_name")
	private String accHolderName;

	@SerializedName("bank")
	private String bank;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("acc_no")
	private String accNo;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("ifsc")
	private String ifsc;

	@SerializedName("branch")
	private String branch;

	public void setAccHolderName(String accHolderName){
		this.accHolderName = accHolderName;
	}

	public String getAccHolderName(){
		return accHolderName;
	}

	public void setBank(String bank){
		this.bank = bank;
	}

	public String getBank(){
		return bank;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setAccNo(String accNo){
		this.accNo = accNo;
	}

	public String getAccNo(){
		return accNo;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIfsc(String ifsc){
		this.ifsc = ifsc;
	}

	public String getIfsc(){
		return ifsc;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"acc_holder_name = '" + accHolderName + '\'' + 
			",bank = '" + bank + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",acc_no = '" + accNo + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",ifsc = '" + ifsc + '\'' + 
			",branch = '" + branch + '\'' + 
			"}";
		}
}