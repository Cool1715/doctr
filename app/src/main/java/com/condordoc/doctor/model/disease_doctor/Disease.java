package com.condordoc.doctor.model.disease_doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Disease {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("disease_id")
    @Expose
    private Integer disease_id;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getDisease() {
        return disease_id;
    }

    public void setDisease(Integer did) {
        this.disease_id = did;
    }
}
