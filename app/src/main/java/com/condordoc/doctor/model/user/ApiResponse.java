package com.condordoc.doctor.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Tamim on 24/10/2017.
 */


public class ApiResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @SerializedName("error")
    @Expose
    private String error;



    //@SerializedName("values")
//    @Expose
//    private List<ApiValue> values = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String code) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



//    public List<ApiValue> getValues() {
//        return values;
//    }
//
//    public void setValues(List<ApiValue> values) {
//        this.values = values;
//    }
}
