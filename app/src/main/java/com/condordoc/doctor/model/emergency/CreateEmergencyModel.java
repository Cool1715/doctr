package com.condordoc.doctor.model.emergency;

import com.google.gson.annotations.SerializedName;

public class CreateEmergencyModel {

	@SerializedName("data")
	private String data;

	@SerializedName("error")
	private String error;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	public void setError(String error){
		this.error = error;
	}

	public String getError(){
		return error;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CreateEmergencyModel{" + 
			"data = '" + data + '\'' + 
			",error = '" + error + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}