package com.condordoc.doctor.model.schedule;


import androidx.annotation.NonNull;

public class TimeScheduleModel {

    private int id;
    private String toTime;
    private String fromTime;
    private boolean isActive = false;

    public TimeScheduleModel(String fromTime , String toTime) {
        this.toTime = toTime;
        this.fromTime = fromTime;
        isActive = false;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("%s-%s",fromTime, toTime);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
