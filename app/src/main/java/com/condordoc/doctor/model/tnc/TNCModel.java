package com.condordoc.doctor.model.tnc;

import com.google.gson.annotations.SerializedName;

public class TNCModel {

	@SerializedName("data")
	private DataItem data;

	@SerializedName("status")
	private String status;

	public void setData(DataItem data){
		this.data = data;
	}

	public DataItem getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"TNCModel{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}