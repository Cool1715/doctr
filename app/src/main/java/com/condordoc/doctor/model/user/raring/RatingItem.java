package com.condordoc.doctor.model.user.raring;

import com.google.gson.annotations.SerializedName;

public class RatingItem {

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("doctor_id")
	private String doctor_id;

	@SerializedName("patient_id")
	private String patient_id;

	@SerializedName("feedback")
	private String feedback;

	@SerializedName("rating")
	private String rating;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;



	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setDoctorid(String docid){
		this.doctor_id = docid;
	}

	public String getDoctorid(){
		return doctor_id;
	}

	public void setPatientid(String patid){
		this.patient_id = patid;
	}

	public String getPatientid(){
		return patient_id;
	}

	public void setRating(String rate){
		this.rating = rate;
	}

	public String getRating(){
		return rating;
	}
	public void setFeedback(String des){
		this.feedback = des;
	}

	public String getFeedback(){
		return feedback;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",service_id = '" + doctor_id + '\'' +
			",feedback = '" + feedback + '\'' +
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' +
					"}";
		}
}