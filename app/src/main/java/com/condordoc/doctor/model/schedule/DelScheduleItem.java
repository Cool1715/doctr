package com.condordoc.doctor.model.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DelScheduleItem {

	@SerializedName("id")
	@Expose
	private String id;

	public DelScheduleItem(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}