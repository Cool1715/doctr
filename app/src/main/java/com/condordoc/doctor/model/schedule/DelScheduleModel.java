
package com.condordoc.doctor.model.schedule;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DelScheduleModel {

    @SerializedName("schedule")
    @Expose
    private List<DelScheduleItem> schedule = null;

    public List<DelScheduleItem> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<DelScheduleItem> schedule) {
        this.schedule = schedule;
    }

}
