package com.condordoc.doctor.model.user.description;

import com.google.gson.annotations.SerializedName;

public class DesItem {

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("doctor_id")
	private String doctor_id;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;



	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setDoctorid(String docid){
		this.doctor_id = docid;
	}

	public String getDoctorid(){
		return doctor_id;
	}

	public void setDescription(String des){
		this.description = des;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",service_id = '" + doctor_id + '\'' +
			",description = '" + description + '\'' +
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' +

					"}";
		}
}