package com.condordoc.doctor.model.user.raring;

import com.condordoc.doctor.model.user.description.DesItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingGet {

	@SerializedName("data")
	private List<RatingItem> data;



	public void setData(List<RatingItem> data){
		this.data = data;
	}

	public List <RatingItem> getData(){
		return data;
	}



	@Override
 	public String toString(){
		return 
			"LogoutModel{" +
			"data = '" + data + '\'' + 

			"}";
		}
}