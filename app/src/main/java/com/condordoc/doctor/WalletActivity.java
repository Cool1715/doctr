package com.condordoc.doctor;



import android.content.Intent;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.fcm.FCMHandler;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.model.user.ApiResponse;
import com.condordoc.doctor.model.user.sigin.sigin.SignInModel;
import com.condordoc.doctor.services.ApiManager;


import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class WalletActivity extends LocalizationActivity {

    ApiManager apiManager;
    @BindView(R.id.wallet_amount)
    TextView displayAmount;

    @BindView(R.id.cashearning)
    TextView cashearning;

    @BindView(R.id.onlinepending)
    TextView onlinepending;

    @BindView(R.id.input_amount)
    EditText input_amount;



    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    private int total,online,cash=0;


    @Nullable
    @OnClick(R.id.addMoney)
    void onAddMoney() {
         Log.e("xchhhh",input_amount.getText().toString());
        if(Integer.parseInt(input_amount.getText().toString())<=0 )
        {
            input_amount.setError(getString(R.string.amou));
            return;
        }
        if(Integer.parseInt(input_amount.getText().toString())>online)
        {
            input_amount.setError(getString(R.string.pend));
            return;
        }

        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance(this);
        progressDialogUtils.showDialog(getString(R.string.protrans));
        apiManager = App.retrofit(AppPreferences.newInstance().getToken(this))
                .create(ApiManager.class);
        apiManager.withdrawRequest( input_amount.getText().toString())
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<ApiResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(ApiResponse signInModel) {
                        try {
                            if (signInModel != null) {
                                if (signInModel.getStatus().equalsIgnoreCase( "true" ))
                                {
                                    progressDialogUtils.hideDialog();

                                } else {
                                    progressDialogUtils.hideDialog();
                                    Toast toast=Toast.makeText( WalletActivity.this, "" + signInModel.getError(), Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(WalletActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            }
                        } catch (Exception e) {
                            progressDialogUtils.hideDialog();
                            Toast toast=Toast.makeText( WalletActivity.this, "Something went wrong.", Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(WalletActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("errrrrr",e.getMessage());
                        progressDialogUtils.hideDialog();
                    }
                } );


    }


    @Nullable
    @OnClick(R.id.tnc)
    void ontnc() {

        Intent intent = new Intent(WalletActivity.this,AboutUsActivity.class);
        intent.putExtra(APIURL.Activity,"Tnc");
        startActivity(intent);
    }

    @Nullable
    @OnClick(R.id.pp)
    void onpp() {

        Intent intent = new Intent(WalletActivity.this,AboutUsActivity.class);
        intent.putExtra(APIURL.Activity,"pp");
        startActivity(intent);
    }



    @Nullable
    @OnClick(R.id.view)
    void onViewTranction()
    {
        Intent intent = new Intent(WalletActivity.this, TransactionHistory.class);
        WalletActivity.this.startActivity(intent);
    }


    @Nullable
    @OnClick(R.id.reqview)
    void onViewRequest()
    {
        Intent intent = new Intent(WalletActivity.this, TransactionHistory.class);
        WalletActivity.this.startActivity(intent);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

             Log.e("token",AppPreferences.newInstance().getToken(getApplicationContext()));
        ApiManager apiManager = App.retrofit(AppPreferences.newInstance().getToken(this))
                .create(ApiManager.class);

        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance(this);
        progressDialogUtils.showDialog(getString(R.string.fetchhistory));
        apiManager.getJobs()
                .subscribeOn(Schedulers.io())
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR)

                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        progressDialogUtils.hideDialog();
                        try {
                            for (DataItem item: dataItems)
                            {
                                total=total+Integer.parseInt(item.getPrice());
                                if(item.getPaymentMethod().equals("online"))
                                {
                                    online=online+Integer.parseInt(item.getPrice());
                                }
                                else
                                {
                                    cash=cash+Integer.parseInt(item.getPrice());
                                }
                            }

                            displayAmount.setText(AppConstant.CURRENCY_SYMBOL+" "+String.valueOf(total)+".00");
                            cashearning.setText(AppConstant.CURRENCY_SYMBOL+" "+cash+".00");
                            onlinepending.setText(AppConstant.CURRENCY_SYMBOL+" "+online+".00");
                        }catch (Exception e){

                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            progressDialogUtils.hideDialog();
//                            Toast.makeText(OngoingActivity.this, "", Toast.LENGTH_SHORT).show();
                        }catch (Exception ex){

                        }
                    }
                });

    }


    public class DecimalDigitsInputFilter implements InputFilter {

        private final int decimalDigits;

        /**
         * Constructor.
         *
         * @param decimalDigits maximum decimal digits
         */
        public DecimalDigitsInputFilter(int decimalDigits) {
            this.decimalDigits = decimalDigits;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {


            int dotPos = -1;
            int len = dest.length();
            for (int i = 0; i < len; i++) {
                char c = dest.charAt(i);
                if (c == '.' || c == ',') {
                    dotPos = i;
                    break;
                }
            }
            if (dotPos >= 0) {

                // protects against many dots
                if (source.equals(".") || source.equals(",")) {
                    return "";
                }
                // if the text is entered before the dot
                if (dend <= dotPos) {
                    return null;
                }
                if (len - dotPos > decimalDigits) {
                    return "";
                }
            }

            return null;
        }
    }
}
