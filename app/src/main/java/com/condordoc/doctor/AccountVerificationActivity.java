package com.condordoc.doctor;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.model.fake.FakeModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AccountVerificationActivity extends LocalizationActivity {


    private static final String TAG = AccountVerificationActivity.class.getSimpleName();
    @BindView(R.id.done_layout)
    LinearLayout done_layout;

    @BindView(R.id.phoneNumber)
    TextView phoneNumber;


    ProgressDialogUtils progressDialogUtils;
    private String phone;


    @Nullable
    @OnClick(R.id.fakeVerify)
    void onFakeVerify(){
        if (!TextUtils.isEmpty(emailId)){
            progressDialogUtils = ProgressDialogUtils.newInstance(this);
            apiManager.fakeVerify(emailId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<FakeModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(FakeModel fakeModel) {
                            progressDialogUtils.hideDialog();
                            try {
                                if (fakeModel != null) {
                                    if (fakeModel.getStatus().equalsIgnoreCase("true")) {
                                        Toast toast=Toast.makeText(AccountVerificationActivity.this, "Verified EmailId " + emailId, Toast.LENGTH_SHORT);
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(AccountVerificationActivity.this, android.R.color.holo_red_dark));
                                        toast.show();
                                        Intent intent = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        String error = fakeModel.getError();
                                        String message = fakeModel.getMessage();
                                        Log.d(TAG, "Error " + error +" message " + message);
                                        Toast toast=Toast.makeText(AccountVerificationActivity.this, "Check Mail Account : Index or Spam Folder for Verifying email Id", Toast.LENGTH_SHORT);
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(AccountVerificationActivity.this, android.R.color.holo_red_dark));
                                        toast.show();
                                    }
                                }
                            }catch (Exception e){
                                Toast toast=Toast.makeText(AccountVerificationActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(AccountVerificationActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            progressDialogUtils.hideDialog();
                            try{
                                Toast toast=Toast.makeText(AccountVerificationActivity.this, "Something went wrong!" + emailId, Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(AccountVerificationActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }
                    });

        }
    }

    @Nullable
    @OnClick(R.id.btn_code)
    void onbtncode() {
//        Intent in = new Intent(this,MainActivity.class);
//        startActivity(in);
//        finish();
/*
        if (!TextUtils.isEmpty(emailId)){
            progressDialogUtils.showDialog("Fake verified!");
            apiManager.fakeVerify(emailId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<FakeModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(FakeModel fakeModel) {
                            progressDialogUtils.hideDialog();
                            try {
                                Toast.makeText(AccountVerificationActivity.this, "Verified Email ID " + emailId, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            progressDialogUtils.hideDialog();
                            try{
                                HttpException httpException = (HttpException) e;
                                if (httpException.code() == 401){
                                    Toast.makeText(AccountVerificationActivity.this, "Fake Verified EmailId, Please login" + emailId, Toast.LENGTH_SHORT).show();
                                    Intent  intent = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }
                    });

        }*/
    }
    @Nullable
    @OnClick(R.id.btn_otp)
    void onBtnOtp() {
//        Intent in = new Intent(this,MainActivity.class);
//        startActivity(in);
//        finish();

        /*if (!TextUtils.isEmpty(emailId)){
            progressDialogUtils.showDialog("Fake verified!");
            apiManager.fakeVerify(emailId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<FakeModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(FakeModel fakeModel) {
                            progressDialogUtils.hideDialog();
                            try {
                                Toast.makeText(AccountVerificationActivity.this, "Verified Email ID " + emailId, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            progressDialogUtils.hideDialog();
                            try{
                                HttpException httpException = (HttpException) e;
                                if (httpException.code() == 401){
                                    Toast.makeText(AccountVerificationActivity.this, "Fake Verified EmailId, Please login" + emailId, Toast.LENGTH_SHORT).show();
                                    Intent  intent = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }
                    });*/

//        }
    }

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    ApiManager apiManager;

    String emailId  = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);
        ButterKnife.bind(this);
        apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);


        if (getIntent().getExtras().containsKey(AppConstant.PERSON_EMAIL)){
            emailId = getIntent().getStringExtra(AppConstant.PERSON_EMAIL);
        }

        if (getIntent().getExtras().containsKey(AppConstant.PERSON_EMAIL)){
            phone = getIntent().getStringExtra(AppConstant.PERSON_PHONE);
            phoneNumber.setText(phone.toString());
        }  else {
            phoneNumber.setVisibility(View.GONE);
        }
    }


    private void sendOtp(){
        Single<String> stringSingle = apiManager.sendOtp(9);
        stringSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(String s) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void verfiyingOtp(){
        Single<String> stringSingle = apiManager.verfiyingOtpCode(1234);
        stringSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(String s) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }







}

