package com.condordoc.doctor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.model.job.CompleteJobModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.services.LocationService;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CollectPaymentActivity extends LocalizationActivity {

    @BindView(R.id.headerPrice)
    TextView headerPrice;

    @BindView(R.id.doc_disc)
    TextView doc_disc;

    @BindView(R.id.discountPrice)
    TextView discountPrice;

    @BindView(R.id.misc_fee)
    TextView misc_fee;


    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.diesaseName)
    TextView diesaseName;

    @BindView(R.id.dateTime)
    TextView dateTime;

    ApiManager apiManager;



    @BindView(R.id.method)
    TextView method;

    private String job_id = "0";


    @Nullable
    @OnClick(R.id.login)
    void onlogin() {

        if (!job_id.equalsIgnoreCase("0")){
            setJobCompletedDoctor();


        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_payment);
        ButterKnife.bind(this);
        apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.FINAL_PRICE)){

            headerPrice.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.FINAL_PRICE)));
        }
        if (getIntent().getExtras() != null){
            //  headerPrice.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.PRICE)));
            dateTime.setText( getIntent().getStringExtra("datetime"));
            diesaseName.setText(getIntent().getStringExtra("dieases"));
            method.setText(getIntent().getStringExtra("paymethod"));

        }

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.PRICE)){
          //  headerPrice.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.PRICE)));
            price.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.PRICE)));
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.MISC_FEE)){
            misc_fee.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.MISC_FEE)));
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.DISCOUNT)){
            doc_disc.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.DISCOUNT)));
            discountPrice.setText(String.format("$%s", getIntent().getStringExtra(AppConstant.DISCOUNT)));
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.START_JOB_ID)) {
            job_id = getIntent().getStringExtra(AppConstant.START_JOB_ID);
        }

    }


    private void setJobCompletedDoctor() {
        ProgressDialog pd=new ProgressDialog(CollectPaymentActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.setJobCompletedDoctor(job_id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<CompleteJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                pd.dismiss();
            }

            @Override
            public void onSuccess(CompleteJobModel completeJobModel) {
                pd.dismiss();
                CommonUtils.disMissProgressDialog(getBaseContext());
                try {
                    if (completeJobModel != null && completeJobModel.getStatus().equalsIgnoreCase("true")) {
//                        textslide.setText(getResources().getString(R.string.taptoendjob));

                        Intent intent = new Intent(CollectPaymentActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);

//                        Toast.makeText(CollectPaymentActivity.this, "" + completeJobModel.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast=Toast.makeText(CollectPaymentActivity.this, "Job has been completed.", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(CollectPaymentActivity.this, android.R.color.holo_red_dark));
                        toast.show();

                        try {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(CollectPaymentActivity.this, LocationService.class);
                                    Log.d("Collected", "remove location service");
                                    stopService(intent);
                                }
                            }, 1000 * 2);

                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        assert completeJobModel != null;
                        Toast toast=Toast.makeText(CollectPaymentActivity.this, "" + completeJobModel.getError(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(CollectPaymentActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                } catch (Exception e) {
                }
            }
            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                CommonUtils.disMissProgressDialog(getBaseContext());
            }
        });
    }
}
