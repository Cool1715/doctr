package com.condordoc.doctor;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.condordoc.doctor.adapter.LocalServiceAdapter;
import com.condordoc.doctor.model.disease.DataItem;
import com.condordoc.doctor.model.disease.DiseaseModel;
import com.condordoc.doctor.model.disease_doctor.Disease;
import com.condordoc.doctor.model.disease_doctor.DiseaseModelGet;
import com.condordoc.doctor.model.disease_doctor.DisesaeDoctorPostModel;
import com.condordoc.doctor.model.disease_doctor.DoctorGetModel;
import com.condordoc.doctor.model.emergency.CreateEmergencyModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;

public class SubServiceActivity extends LocalizationActivity {

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }


    @BindView(R.id.local_service)
    RecyclerView local_service;

    @BindView(R.id.booknow)
    TextView booknow;

    @OnClick(R.id.booknow)
    void next() {
        finish();
//        Intent in;
        /*if (AppPreferences.newInstance().isSetManaullyAddress(SubServiceActivity.this)) {
            int id = AppPreferences.newInstance().getServiceLocalId(this);
            if (id == -1) {
                Toast.makeText(subServiceActivity, "Please selected local service", Toast.LENGTH_SHORT).show();
                return;
            }
            in = new Intent(SubServiceActivity.this, PaymentModeActivity.class);
            in.putExtra(AppConstant.MANAGE_SERVICE_ID, id);
            in.putExtra("location", getIntent().getStringExtra("location"));
            startActivity(in);
        } else {*/
//            in = new Intent(SubServiceActivity.this, AddLocationAddressActivity.class);
//            in.putExtra("location", getIntent().getStringExtra("location"));
//            startActivity(in);
//
       // }
    }

    public static SubServiceActivity subServiceActivity;
    LocalServiceAdapter serviceAdapter;
    public ApiManager apiManager;
    private List<DataItem> diseaseModel;
    ProgressDialogUtils progressDialogUtils;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_service);
        ButterKnife.bind(this);

        subServiceActivity = SubServiceActivity.this;

        apiManager = App.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);

        serviceAdapter = new LocalServiceAdapter(new ArrayList<>(), this);

        local_service.setLayoutManager(new LinearLayoutManager(this));
        local_service.setAdapter(serviceAdapter);
        Log.e("token",AppPreferences.newInstance().getToken( this ) );

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.MANAGE_SERVICE_ID)) {
            int manage_id = getIntent().getIntExtra(AppConstant.MANAGE_SERVICE_ID, 0);

           progressDialogUtils = ProgressDialogUtils.newInstance(this);
            progressDialogUtils.showDialog("Loading");
            apiManager.getDiseaseService(manage_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<DiseaseModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(DiseaseModel serviceLocation) {
                            diseaseModel=serviceLocation.getData();


                            try {
                                if (serviceLocation != null && serviceLocation.getStatus().equalsIgnoreCase("true")) {
                                    serviceAdapter.addLocalItem(serviceLocation.getData());
                                    getData();
                                } else {
                                    Toast toast=Toast.makeText(SubServiceActivity.this, "Something went wrong.", Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SubServiceActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onError(Throwable throwable) {

                            try {
                                progressDialogUtils.hideDialog();
                                HttpException httpException = (HttpException) throwable;
                                JSONObject jsonObject = new JSONObject(httpException.response().errorBody().toString());
                                String errorMessage = jsonObject.get("error").toString();
                               Toast toast= Toast.makeText(SubServiceActivity.this, "" + errorMessage, Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SubServiceActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

        }


    }

    public void setDate(int id)
    {
        Log.e("id",""+id);
        progressDialogUtils = ProgressDialogUtils.newInstance(this);
        progressDialogUtils.showDialog("Loading");
        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this ) );
        apiManager = retrofit.create( ApiManager.class );
        apiManager.setService(String.valueOf(id) )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<DisesaeDoctorPostModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(DisesaeDoctorPostModel model) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        progressDialogUtils.hideDialog();
                        try {
                            if (model != null && model.getStatus().equalsIgnoreCase( "true" )) {
                                Log.e("user",model.getMessage());
                               Toast toast= Toast.makeText( SubServiceActivity.this, "" + model.getMessage(), Toast.LENGTH_SHORT );
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SubServiceActivity.this, android.R.color.holo_red_dark));
                                toast.show();

                               if(model.getMessage().equals("Doctor Disease is Added Successfully")) {
//                                    getData();
                                   for(int i=0; i<diseaseModel.size();i++)
                                   {
                                       if(diseaseModel.get(i).getId()==id)
                                       {
                                           diseaseModel.get(i).setCheck(true);
                                           serviceAdapter.notifyDataSetChanged();
                                       }
                                   }
                                }
                               else
                               {
                                   for(int i=0; i<diseaseModel.size();i++)
                                   {
                                       if(diseaseModel.get(i).getId()==id)
                                       {
                                           diseaseModel.get(i).setCheck(false);
                                           serviceAdapter.notifyDataSetChanged();
                                       }
                                   }
                               }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        progressDialogUtils.hideDialog();
                        Toast toast=Toast.makeText( SubServiceActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SubServiceActivity.this, android.R.color.holo_red_dark));
                        toast.show();

                    }
                } );


    }


    public void getData()
    {
        Log.e("dffj","vfvfhhhg");

        apiManager.getDiseaseDoctor()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DiseaseModelGet>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(DiseaseModelGet serviceLocation) {
                        Log.e("service111",""+serviceLocation.getData().getDiseases());

                        try {
//

                                for(Disease d:serviceLocation.getData().getDiseases())
                                {

                                    for(int i=0;i<diseaseModel.size();i++)
                                        if(diseaseModel.get(i).getId()==d.getDisease())
                                        {
                                            Log.e("iddeta",""+d.getName());
                                            diseaseModel.get(i).setCheck(true);
                                            serviceAdapter.notifyDataSetChanged();
                                        }


                                    }
                            progressDialogUtils.hideDialog();


                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {

                        try {

                            HttpException httpException = (HttpException) throwable;
                            JSONObject jsonObject = new JSONObject(httpException.response().errorBody().toString());
                            String errorMessage = jsonObject.get("error").toString();
                           Toast toast= Toast.makeText(SubServiceActivity.this, "" + errorMessage, Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SubServiceActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

    }
}
