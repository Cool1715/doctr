package com.condordoc.doctor;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.bumptech.glide.Glide;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.adapter.SpecificJobAdapter;
import com.condordoc.doctor.model.job.CancelJobModel;
import com.condordoc.doctor.model.job.CompleteJobModel;
import com.condordoc.doctor.model.job.MiscPaymentModel;
import com.condordoc.doctor.model.job.StartJobModel;
import com.condordoc.doctor.model.job.jobbyId.Data;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdStatus;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.APIExecutor;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.GetAddressUitls;
import com.condordoc.doctor.utils.ProgressDialogUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SpecificJobActivity extends LocalizationActivity {

    private static final String TAG = SpecificJobActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSION = 100;
    private SpecificJobAdapter topDownAdapter;
    FloatingActionButton floatingActionButton;
    private ArrayList<JobByIdStatus.DataBean.JobUpdatesBean> jobList=new ArrayList<JobByIdStatus.DataBean.JobUpdatesBean>();

    @BindView(R.id.iv_car_type_image)
    CircleImageView iv_car_type_image;

    @BindView(R.id.rating)
    RatingBar rating;

    String currentCharge;

    @BindView(R.id.name)
    TextView name;


    @BindView(R.id.address)
    TextView address;
    
    @BindView(R.id.main_recycler_view)
    RecyclerView SpecificJobRecyclerview;
    private ApiManager apiManager;
    ProgressDialogUtils dialogUtils;
    private JobByIdModel getJobModel;
    private int misc_fee_resp;
    private int discount_resp;
    private String datatime,dieases,paymethod;


    @Nullable
    @OnClick(R.id.morebutton)
    void onmore() {
        PopupMenu popup = new PopupMenu(SpecificJobActivity.this, findViewById(R.id.morebutton));
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

        try {
            if (getJobModel.getData() != null && getJobModel.getData().getPatientNumber() != null) {
                popup.getMenu().findItem(R.id.one).setVisible(true);
            } else {
                popup.getMenu().findItem(R.id.one).setVisible(false);
            }
        } catch (Exception e) {
            Log.d(TAG, "No available phone");
        }

//        try {
//            if (getJobModel.getData() != null && getJobModel.getData().getPatientId() > 0) {
//                popup.getMenu().findItem(R.id.two).setVisible(true);
//            } else {
//                popup.getMenu().findItem(R.id.two).setVisible(false);
//            }
//        } catch (Exception e) {
//            Log.d(TAG, "No available patient");
//        }


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {


                if (item.getTitle().toString().equalsIgnoreCase("call")) {
                    try {
                        if (ContextCompat.checkSelfPermission(SpecificJobActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getJobModel.getData().getPatientNumber()));
                            startActivity(intent);
                            finish();
                        } else {
                            ActivityCompat.requestPermissions(SpecificJobActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    Toast.makeText(SpecificJobActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
//                } else if (item.getTitle().toString().equalsIgnoreCase("Message")) {
//                    try {
//                        Intent intent = new Intent(SpecificJobActivity.this, DoctorChatActivity.class);
//                        if (getJobModel != null) {
//                            AppPreferences.newInstance().setPatientId(SpecificJobActivity.this, getJobModel.getData().getPatientId());
//                            intent.putExtra(AppConstant.START_JOB_ID, getJobModel.getData().getId() + "");
//                            startActivity(intent);
//                            finish();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                } else if (item.getTitle().equals(getResources().getString(R.string.canceljob))) {
                    cancelJob();
//
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }
    
    private void cancelJob(){
        ProgressDialog pd=new ProgressDialog(SpecificJobActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        apiManager.setJobAcceptedButCancel(String.valueOf(job_id)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<CancelJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                pd.dismiss();

            }

            @Override
            public void onSuccess(CancelJobModel cancelJobModel) {
                pd.dismiss();
                try {
                    if (cancelJobModel != null && cancelJobModel.getStatus().equalsIgnoreCase("true")) {
                       Toast toast= Toast.makeText(SpecificJobActivity.this, "Job Cancel", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                        finish();

                    } else {
                        Toast toast=Toast.makeText(SpecificJobActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                try {
                    Toast toast=Toast.makeText(SpecificJobActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                    toast.show();
                } catch (Exception xe) {

                }
            }
        });
    }

    @BindView(R.id.slide)
    TextView textslide;

    String job_id;
    
    @Nullable
    @OnClick(R.id.floatchat)
    void setFloatingActionButton(){
        try {
            Intent intent = new Intent(SpecificJobActivity.this, DoctorChatActivity.class);
            if (getJobModel != null) {
                AppPreferences.newInstance().setPatientId(SpecificJobActivity.this, getJobModel.getData().getPatientId());
                intent.putExtra(AppConstant.START_JOB_ID, getJobModel.getData().getId() + "");
                startActivity(intent);
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @OnClick(R.id.slide)
    void onslide() {
        if (textslide.getText().toString().equalsIgnoreCase(getResources().getString(R.string.taptoendjob))) {
//            setJobCompletedDoctor();
            showTotalChargeDialog();
        } else {
            Intent intent = new Intent(getApplicationContext(), JobMapUserActivity.class);
            intent.putExtra(AppConstant.START_JOB_ID,job_id);
            startActivity(intent);
           // setJobStarted();
        }
    }

    private void setJobStarted() {
        apiManager.setJobStarted(String.valueOf(job_id)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<StartJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }
            @Override
            public void onSuccess(StartJobModel cancelJobModel) {
                dialogUtils.hideDialog();
                try {
                    if (cancelJobModel != null && cancelJobModel.getStatus().equalsIgnoreCase("true")) {

                        textslide.setText(getResources().getString(R.string.taptoendjob));
                       Toast toast= Toast.makeText(SpecificJobActivity.this, "" + cancelJobModel.getMessage(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();

                    } else {
                        assert cancelJobModel != null;
                        Toast toast=Toast.makeText(SpecificJobActivity.this, "" + cancelJobModel.getError(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                dialogUtils.hideDialog();
            }
        });
    }

    private void setJobCompletedDoctor() {
        dialogUtils.showDialog("Start Job");
        apiManager.setJobCompletedDoctor(String.valueOf(job_id)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<CompleteJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }
            @Override
            public void onSuccess(CompleteJobModel completeJobModel) {
                dialogUtils.hideDialog();
                try {
                    if (completeJobModel != null && completeJobModel.getStatus().equalsIgnoreCase("true")) {
                        textslide.setText(getResources().getString(R.string.taptoendjob));
                        showTotalChargeDialog();
                       Toast toast= Toast.makeText(SpecificJobActivity.this, "" + completeJobModel.getMessage(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    } else {
                        assert completeJobModel != null;
                        Toast toast=Toast.makeText(SpecificJobActivity.this, "" + completeJobModel.getError(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SpecificJobActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                }catch (Exception e) {

                }
            }
            @Override
            public void onError(Throwable e){
                dialogUtils.hideDialog();
            }
        });
    }

    private void showTotalChargeDialog() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogfortotalcharge);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        ImageView closeIm = (ImageView) dialog.findViewById(R.id.close);
        TextView current_charge = (TextView) dialog.findViewById(R.id.current_charge);
        EditText doctor_discout = (EditText) dialog.findViewById(R.id.doctor_discout);
        EditText misc_fee = (EditText) dialog.findViewById(R.id.misc_fee);

        TextView editFinalTotal = (TextView) dialog.findViewById(R.id.finalTotal);

        try {
            if(!TextUtils.isEmpty(currentCharge) && Float.valueOf(currentCharge) > 0) {
                current_charge.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(Double.valueOf(currentCharge)));
            }else{
                current_charge.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(Double.valueOf(currentCharge)));
            }
        } catch (Exception e) {

        }
        misc_fee.setText(String.valueOf(misc_fee_resp));
        doctor_discout.setText(String.valueOf(discount_resp));

        double currentPrice = Double.valueOf(currentCharge);
        double mis_feeValue = Double.valueOf(misc_fee.getText().toString());
        double discoutValue = Double.valueOf(doctor_discout.getText().toString());

        double finalPrice = currentPrice + Double.valueOf(misc_fee.getText().toString()) - Double.valueOf(doctor_discout.getText().toString());
        editFinalTotal.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(finalPrice));

        misc_fee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence editText1, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence editText1, int start, int before, int count) {
                if (!TextUtils.isEmpty(editText1.toString())) {
                    double result = Double.valueOf(editText1.toString());
                    double finalPrice = (currentPrice + result) - Double.valueOf(doctor_discout.getText().toString());
                    if (finalPrice > 0) {
                        editFinalTotal.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(finalPrice));
                    } else {
                        editFinalTotal.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(0));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        doctor_discout.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence editText1, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence editText1, int start, int before, int count) {
                if (!TextUtils.isEmpty(editText1.toString())) {
                    double result = Double.valueOf(editText1.toString());
                    double finalPrice = (currentPrice + Double.valueOf(misc_fee.getText().toString())) - result;
                    if (finalPrice > 0) {
                        editFinalTotal.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(finalPrice));
                    }else {
                        editFinalTotal.setText(AppConstant.CURRENCY_SYMBOL + new DecimalFormat("#0.00").format(0));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        closeIm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.viewongng);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try {

                   /* http://dumpin.in:8003/api/auth/misc-payment
                    misc_fee:10
                    material_fee:10
                    discount:10
                    job_id:122*/

                   callPaymentApi(Double.valueOf(misc_fee.getText().toString()),Double.valueOf(doctor_discout.getText().toString()),current_charge,finalPrice);

                }catch (Exception e) {

                }
            }
        });
    }

    private void callPaymentApi(double mis_feeValue, double discoutValue, TextView current_charge, double finalPrice) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("misc_fee",mis_feeValue);
        jsonObject.addProperty("material_fee","0");
        jsonObject.addProperty("discount",discoutValue);
        jsonObject.addProperty("job_id",job_id);

        CommonUtils.showProgressDialog(getApplicationContext());
        Call<MiscPaymentModel> call = APIExecutor.getApiService(getApplicationContext()).calledMsgDetailApi(jsonObject);
        Log.e("url",""+call.request().url());
        call.enqueue(new retrofit2.Callback<MiscPaymentModel>() {
            @Override
            public void onResponse(Call<MiscPaymentModel> call, Response<MiscPaymentModel> response){
                CommonUtils.disMissProgressDialog(getApplicationContext());
                Log.e("misc payment response","" + new Gson().toJson(response.body()));

                if(response.body()!=null && response.body().getStatus()!=null && response.body().getStatus().equalsIgnoreCase("200")){
                    String price = current_charge.getText().toString().replace("$", "");

                    Intent in = new Intent(SpecificJobActivity.this, CollectPaymentActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    in.putExtra(AppConstant.START_JOB_ID, job_id);
                    in.putExtra("datetime",datatime);
                    in.putExtra("dieases",dieases);
                    in.putExtra("paymethod",paymethod);
                    in.putExtra(AppConstant.PRICE, price);
                    in.putExtra(AppConstant.DISCOUNT, String.valueOf(discoutValue));
                    in.putExtra(AppConstant.FINAL_PRICE, String.valueOf(finalPrice));
                    in.putExtra(AppConstant.MISC_FEE, String.valueOf(mis_feeValue));
                    startActivity(in);
                    finish();
                }
            }
            @Override
            public void onFailure(Call<MiscPaymentModel> call, Throwable t) {
                CommonUtils.disMissProgressDialog(getApplicationContext());
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_job);
        ButterKnife.bind(this);
        
        findViewById(R.id.job_Inprocess_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dialogUtils = ProgressDialogUtils.newInstance(this);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);
        SpecificJobRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        topDownAdapter = new SpecificJobAdapter(getApplicationContext(),jobList);
        SpecificJobRecyclerview.setVisibility(View.VISIBLE);
        SpecificJobRecyclerview.setAdapter(topDownAdapter);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.START_JOB_ID)) {
            job_id = getIntent().getStringExtra(AppConstant.START_JOB_ID);
            //setJobStarted();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getJobs();


        getJobById();
    }



    private void getJobById() {
        dialogUtils.showDialog("");
        apiManager.getJobById(String.valueOf(job_id)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<JobByIdModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(JobByIdModel getJobModel) {
                try {
                    dialogUtils.hideDialog();
                    if (getJobModel != null && getJobModel.getStatus().equalsIgnoreCase("true")) {
                        if(getJobModel.getData().getStatus()==8){
                            textslide.setText(getResources().getString(R.string.taptoendjob));
                        }else {
                            textslide.setText(getResources().getString(R.string.track_on_map));
                        }
                        SpecificJobActivity.this.getJobModel = getJobModel;

                        Data item = getJobModel.getData();

                        name.setText(String.format("%s %s", item.getPatientName(), item.getDiseaseName()));
                        if (item.getRating() != null)
                            rating.setRating(Float.parseFloat(item.getRating()));

                        currentCharge = item.getPrice();
                        dieases=item.getDiseaseName();
                        datatime=item.getDateOfBooking();
                         paymethod= item.getPaymentMethod();

                        if (getJobModel.getData().getImagePatient() != null)
                            Glide.with(SpecificJobActivity.this).load(APIURL.IMAGE_USER_URL + getJobModel.getData().getImagePatient()).error(R.drawable.user).into(iv_car_type_image);
                        else
                            Glide.with(SpecificJobActivity.this).load(R.drawable.user).error(R.drawable.user).into(iv_car_type_image);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (getJobModel != null && getJobModel.getStatus().equalsIgnoreCase("true")) {
                        Data item = getJobModel.getData();
                        if (item != null)
                            address.setText(GetAddressUitls.getCompleteAddressString(SpecificJobActivity.this, Double.valueOf(item.getPatientLat()), Double.valueOf(item.getPatientLong())));
                    }

                } catch (Exception e) {
                    address.setText("No Address");
                }
            }

            @Override
            public void onError(Throwable e) {
                dialogUtils.hideDialog();
            }

            @Override
            public void onComplete() {

            }
        });
    }

   /* private void getJobss() {
        apiManager.getJobs().map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            try {
                if (jobGetModel.getStatus().equalsIgnoreCase("true")) return jobGetModel.getData();
                return new ArrayList<>();
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }).flatMapIterable(dataItems -> dataItems).filter(dataItem -> dataItem.getStatus() == AppConstant.START_DOCTOR || dataItem.getDoctorId() == AppPreferences.newInstance().getUserId(this)).toList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<List<DataItem>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<DataItem> dataItems) {
                try {
                    if (dataItems != null && dataItems.size() > 0)
                        topDownAdapter.updateList(dataItems);
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }*/

    private void getJobs() {
        CommonUtils.showProgressDialog(getApplicationContext());
        Call<JobByIdStatus> call = APIExecutor.getApiService(getApplicationContext()).calledJobByIdStatus("jobs/"+String.valueOf(job_id));
        Log.e("url",""+call.request().url());
        call.enqueue(new retrofit2.Callback<JobByIdStatus>() {
            @Override
            public void onResponse(Call<JobByIdStatus> call, Response<JobByIdStatus> response){
                CommonUtils.disMissProgressDialog(getApplicationContext());
                Log.e("getJobsById response","" + new Gson().toJson(response.body()));
                if(response.body()!=null && response.body().getData()!=null && response.body().getData().getJob_updates().size()>0){
                    // topDownAdapter.updateList(response.body().getData().getJob_updates());
                    misc_fee_resp=response.body().getData().getMisc_fee();
                    discount_resp=response.body().getData().getDoctor_discount();
                    jobList.addAll(response.body().getData().getJob_updates());
                   Collections.reverse(jobList);
                    topDownAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<JobByIdStatus> call, Throwable t) {
                CommonUtils.disMissProgressDialog(getApplicationContext());
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
