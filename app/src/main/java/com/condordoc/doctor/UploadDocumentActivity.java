package com.condordoc.doctor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.utils.DocumentPickerDialogUitls;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.FilePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.FilePickerCallback;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.model.document.DataItem;
import com.condordoc.doctor.model.document.DocoumentGetModel;
import com.condordoc.doctor.model.document.DocumentModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ImagePathUtils;
import com.condordoc.doctor.utils.ImagePickerDialogUitls;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class UploadDocumentActivity extends LocalizationActivity implements ImagePickerCallback,FilePickerCallback {


    private static final String TAG = UploadDocumentActivity.class.getSimpleName();
    //    @BindView(R.id.uploadDocumentRecyclerView)
//    RecyclerView uploadDocumentRecyclerView;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private FilePicker filepicker;

    private static int state = 0;

    @BindView(R.id.addDocument)
    ImageView addDocument;
    @BindView(R.id.addDocument2)
    ImageView addDocument2;
    @BindView(R.id.addDocument3)
    ImageView addDocument3;
    @BindView(R.id.addDocument4)
    ImageView addDocument4;

    @BindView(R.id.pdl1)
    LinearLayout pdl1;
    @BindView(R.id.pdl2)
    LinearLayout pdl2;
    @BindView(R.id.pdl3)
    LinearLayout pdl3;
    @BindView(R.id.pdl4)
    LinearLayout pdl4;


    @BindView(R.id.textpdf1)
    TextView textpdf1;
    @BindView(R.id.textpdf2)
    TextView textpdf2;
    @BindView(R.id.textpdf3)
    TextView textpdf3;
    @BindView(R.id.textpdf4)
    TextView textpdf4;

    @BindView(R.id.down1)
    ImageView down1;
    @BindView(R.id.down2)
    ImageView down2;
    @BindView(R.id.down3)
    ImageView down3;
    @BindView(R.id.down4)
    ImageView down4;

    List<Observable<DocumentModel>> requestList;
    List<MultipartBody.Part> mutiParts;


    private ApiManager apiManager;
    private String pickerPath;

    @OnClick(R.id.addDocument)
    public void pickImage1() {
        pos = 1;
//        pickImageSingle();
        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }
            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });

    }
    @OnClick(R.id.pdl1)
    public void pickImage11() {
        pos = 1;
    //        pickImageSingle();
        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }
            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });

    }

    @OnClick(R.id.addDocument2)

    public void pickImage2() {
        pos = 2;
//        pickImageSingle();

        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }

            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
}

    @OnClick(R.id.pdl2)
    public void pickImage12() {
        pos = 2;
//        pickImageSingle();

        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }

            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
    }

    @OnClick(R.id.addDocument3)
    public void pickImage3() {
        pos = 3;
//        pickImageSingle();

        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }
            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
    }
    @OnClick(R.id.pdl3)
    public void pickImage13() {
        pos = 2;
//        pickImageSingle();

        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }

            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
    }


    @OnClick(R.id.addDocument4)
    public void pickImage4() {
        pos = 4;
//        pickImageSingle();
        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }
            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
    }
    @OnClick(R.id.pdl4)
    public void pickImage14() {
        pos = 2;
//        pickImageSingle();

        DocumentPickerDialogUitls.newInstance(this).showImagePicker(new DocumentPickerDialogUitls.DocumentPickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }

            @Override
            public void onDocument(Dialog dialog, String message) {
                pickDocument();
            }

            @Override
            public void onCancel() {

            }
        });
    }


    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @BindView(R.id.continuess)
    TextView continuess;

    @Nullable
    @OnClick(R.id.continuess)
    void oncontinue() {


        if (mutiParts != null) {

            MultipartBody.Part part1 = mutiParts.get(0);
            if (part1 == null ){
               Toast toast= Toast.makeText(this, "Please set medical Certificate First image", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                toast.show();
                return;
            }
            MultipartBody.Part part2 = mutiParts.get(1);
            if (part2 == null ){
               Toast toast =Toast.makeText(this, "Please set medical Certificate Second image", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                toast.show();
                return;
            }

            MultipartBody.Part part3 = mutiParts.get(2);
            if (part3 == null ){
               Toast toast= Toast.makeText(this, "Please set Graduation Degree Certificate First image", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                toast.show();
                return;
            }
            MultipartBody.Part part4 = mutiParts.get(3);
            if (part4 == null ){
                Toast toast=Toast.makeText(this, "Please set Graduation Degree Certificate Second image", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                toast.show();
                return;
            }
            RequestBody certificate1 = RequestBody.create(MediaType.parse("text/plain"), "Medical Certificate");
            RequestBody certificate2 = RequestBody.create(MediaType.parse("text/plain"), "Graduation Degree Certificate");
            Observable<DocumentModel> document1 = apiManager.postDocument(part1, part2, certificate1);
            Observable<DocumentModel> document2 = apiManager.postDocument(part3, part4, certificate2);

            requestList = new ArrayList<>();
            if (document1 != null) requestList.add(document1);
            if (document2 != null) requestList.add(document2);

            if (requestList.size() == 2) {
                CommonUtils.showProgressDialog(getBaseContext());
                Observable.zip(requestList, new Function<Object[], Object>() {
                    @Override
                    public Object apply(Object[] objects) throws Exception {
                        return new Object();
                    }
                }).delay(2000, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        requestList.clear();
                        CommonUtils.disMissProgressDialog(getBaseContext());
//                        continuess.setVisibility(View.VISIBLE);
                        continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.grey));
                        Toast toast=Toast.makeText(UploadDocumentActivity.this, "Successfully upload.", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        Toast toast=Toast.makeText(UploadDocumentActivity.this, "Please update at least two documents", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                });
            }

            if (requestList.size() == 1) {
                if (document1 != null) {
                    CommonUtils.showProgressDialog(getBaseContext());
                    document1
                            .delay(1000, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<DocumentModel>() {
                        @Override
                        public void accept(DocumentModel documentModel) throws Exception {
                            CommonUtils.disMissProgressDialog(getBaseContext());
//                            continuess.setVisibility(View.GONE);
                            continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.grey));

                            Toast toast=Toast.makeText(UploadDocumentActivity.this, "Successfully upload.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            CommonUtils.disMissProgressDialog(getBaseContext());
                           Toast toast= Toast.makeText(UploadDocumentActivity.this, "Please update at least two documents.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    });
                }


                if (document2 != null) {
                    CommonUtils.showProgressDialog(getBaseContext());
                    document2
                            .delay(1000, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<DocumentModel>() {
                        @Override
                        public void accept(DocumentModel documentModel) throws Exception {
                            CommonUtils.disMissProgressDialog(getBaseContext());
//                            continuess.setVisibility(View.GONE);
                            continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.grey));

                            Toast toast=Toast.makeText(UploadDocumentActivity.this, "Successfully upload.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            CommonUtils.disMissProgressDialog(getBaseContext());
                            Toast toast=Toast.makeText(UploadDocumentActivity.this, "Please update at least two documents.", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    });
                }

            }
        }
    }

    String documents[] = {"Medical Certificate", "Graduation Degree Certificate"};


    private static int pos;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);
        ButterKnife.bind(this);
        requestList = new ArrayList<>(4);
        mutiParts = new ArrayList<>(4);
        try {

            File file1 =new File(AppPreferences.newInstance().getFile1(this));
            File file2 =new File(AppPreferences.newInstance().getFile2(this));
            File file3 =new File(AppPreferences.newInstance().getFile3(this));
            File file4 =new File(AppPreferences.newInstance().getFile4(this));

            MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", ""+file1.getName(),
                    RequestBody.create(MediaType.parse("image/*"), file1));
            MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", ""+file2.getName(),
                    RequestBody.create(MediaType.parse("image/*"), file2));
            MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", ""+file3.getName(),
                    RequestBody.create(MediaType.parse("image/*"), file3));
            MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file2", ""+file4.getName(),
                    RequestBody.create(MediaType.parse("image/*"), file4));
            mutiParts.add(0,filePart1);
            mutiParts.add(1,filePart2);
            mutiParts.add(2,filePart3);
            mutiParts.add(3,filePart4);
        }catch (Exception e){
            MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", "",
                    RequestBody.create(MediaType.parse("image/*"), ""));
            MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", "",
                    RequestBody.create(MediaType.parse("image/*"), ""));
            MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", "",
                    RequestBody.create(MediaType.parse("image/*"), ""));
            MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file2", "",
                    RequestBody.create(MediaType.parse("image/*"), ""));
            mutiParts.add(0,filePart1);
            mutiParts.add(1,filePart2);
            mutiParts.add(2,filePart3);
            mutiParts.add(3,filePart4);
        }


        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);

        List<DataItem> dataItemList;
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.getDocument().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<DocoumentGetModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(DocoumentGetModel docoumentGetModel) {
                CommonUtils.disMissProgressDialog(getBaseContext());
                if (docoumentGetModel != null) {
                    List<DataItem> items = docoumentGetModel.getData();
                    Log.e("itemssss",items.toString());
                    if (items.size() > 0) {
                        try {
                            if (items.size() == 1) {
                                Log.e("typeee",items.get(0).getDocType());
                                if (items.get(0).getDocType().contains("medical")) {

                                    if(items.get(0).getFile1().contains(".pdf"))
                                    {
                                        addDocument.setVisibility(View.GONE);
                                        pdl1.setVisibility(View.VISIBLE);
                                        textpdf1.setText(items.get(0).getFile1());
                                        down1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile1());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this).asBitmap().load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile1())
                                                .error(R.drawable.plus)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument.setImageBitmap(resource);
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile1());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(1, filePart1);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }

                                    if(items.get(0).getFile2().contains(".pdf"))
                                    {
                                        addDocument2.setVisibility(View.GONE);
                                        pdl2.setVisibility(View.VISIBLE);
                                        textpdf2.setText(items.get(0).getFile2());
                                        down2.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile2());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this).asBitmap().load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile2())
                                                .error(R.drawable.plus)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument2.setImageBitmap(resource);
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile2());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(2, filePart2);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }
                                }

                                if (items.get(0).getDocType().contains("Graduation")){

                                    if(items.get(0).getFile1().contains(".pdf"))
                                    {
                                        addDocument3.setVisibility(View.GONE);
                                        pdl3.setVisibility(View.VISIBLE);
                                        textpdf3.setText(items.get(0).getFile1());
                                        down3.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile1());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this).asBitmap().load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile1())
                                                .error(R.drawable.plus)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)

                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument3.setImageBitmap(resource);
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile1());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(3, filePart3);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }

                                    if(items.get(0).getFile2().contains(".pdf"))
                                    {
                                        addDocument4.setVisibility(View.GONE);
                                        pdl4.setVisibility(View.VISIBLE);
                                        textpdf4.setText(items.get(1).getFile2());
                                        down4.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile2());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this).asBitmap().load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile2())
                                                .error(R.drawable.plus)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument4.setImageBitmap(resource);
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile2());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file2", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(4, filePart4);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }
                                }
                            }
                            if (items.size() == 2) {
                                Log.e("typeee",items.get(0).getDocType());
                                try {

                                    if(items.get(0).getFile1().contains(".pdf"))
                                    {
                                     addDocument.setVisibility(View.GONE);
                                        pdl1.setVisibility(View.VISIBLE);
                                        textpdf1.setText(items.get(0).getFile1());
                                        down1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile1());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this)
                                                .asBitmap()
                                                .load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile1())
                                                .error(R.drawable.plus)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument.setImageBitmap(resource);
                                                        addDocument.buildDrawingCache();

                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile1());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(1, filePart1);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }

                                    if(items.get(0).getFile2().contains(".pdf"))
                                    {
                                        addDocument2.setVisibility(View.GONE);
                                        pdl2.setVisibility(View.VISIBLE);
                                        textpdf2.setText(items.get(0).getFile2());
                                        down2.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(0).getFile2());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this)
                                                .asBitmap()
                                                .load(APIURL.IMAGE_DOCUMENT_URL + items.get(0).getFile2())
                                                .error(R.drawable.plus)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument2.setImageBitmap(resource);
                                                        addDocument2.buildDrawingCache();

                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile2());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(2, filePart2);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }

                                    if(items.get(1).getFile1().contains(".pdf"))
                                    {
                                        addDocument3.setVisibility(View.GONE);
                                        pdl3.setVisibility(View.VISIBLE);
                                        textpdf3.setText(items.get(1).getFile1());
                                        down3.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(1).getFile1());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this)
                                                .asBitmap()
                                                .load(APIURL.IMAGE_DOCUMENT_URL + "" + items.get(1).getFile1())
                                                .error(R.drawable.plus)
                                                .into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument3.setImageBitmap(resource);
                                                        addDocument3.buildDrawingCache();
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile1());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(3, filePart3);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }

                                    if(items.get(1).getFile2().contains(".pdf"))
                                    {
                                        addDocument4.setVisibility(View.GONE);
                                        pdl4.setVisibility(View.VISIBLE);
                                        textpdf4.setText(items.get(1).getFile2());
                                        down4.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openPdf(items.get(1).getFile2());
                                            }
                                        });
                                    }
                                    else {
                                        Glide.with(UploadDocumentActivity.this)
                                                .asBitmap()
                                                .load(APIURL.IMAGE_DOCUMENT_URL + "" + items.get(1).getFile2())
                                                .error(R.drawable.plus).
                                                into(new CustomTarget<Bitmap>() {
                                                    @Override
                                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                        addDocument4.setImageBitmap(resource);
                                                        addDocument4.buildDrawingCache();
                                                        String filePath = ImagePathUtils.saveImage(UploadDocumentActivity.this, resource, items.get(0).getFile2());
                                                        File file = new File(filePath);
                                                        MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                                                        mutiParts.add(4, filePart4);
                                                    }

                                                    @Override
                                                    public void onLoadCleared(@Nullable Drawable placeholder) {

                                                    }
                                                });
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
//                            continuess.setVisibility(View.GONE);
                            continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.grey));

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("errrr",e.getMessage());
                        }
//                                continuess.setVisibility(View.GONE);

                    }


                }

            }

            @Override
            public void onError(Throwable e) {
                try {
                    CommonUtils.disMissProgressDialog(getBaseContext());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//                        ProgressDialogUtils.newInstance(UploadDocumentActivity.this).hideDialog();
            }
        });

    }


    public void takePicture() {
        cameraPicker = new CameraImagePicker(this);
        cameraPicker.setDebugglable(true);
        cameraPicker.setCacheLocation(CacheLocation.EXTERNAL_STORAGE_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
            else if(requestCode ==Picker.PICK_FILE )
            {
                if(filepicker == null) {
                    filepicker = new FilePicker(UploadDocumentActivity.this);
                    filepicker.setFilePickerCallback(this);
                }
                filepicker.submit(data);
            }
        }
    }


    public void pickDocument()
    {
        filepicker= new FilePicker(UploadDocumentActivity.this);
        // filePicker.allowMultiple();
        // filePicker.
        filepicker.setMimeType("application/pdf");

        filepicker.setFilePickerCallback(this);
        filepicker.pickFile();
    }


    public void pickImageSingle() {
        imagePicker = new ImagePicker(this);
        imagePicker.setDebugglable(true);
//        imagePicker.setFolderName("Random");
        imagePicker.setRequestId(1234);
        imagePicker.ensureMaxSize(500, 500);
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        Bundle bundle = new Bundle();
        bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
        imagePicker.setCacheLocation(CacheLocation.EXTERNAL_STORAGE_PUBLIC_DIR);
        imagePicker.pickImage();
    }

    public void pickImageMultiple() {
        imagePicker = new ImagePicker(this);
        imagePicker.setImagePickerCallback(this);
        imagePicker.allowMultiple();
        imagePicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> chosenImages) {
        File profileImageChanged = new File(chosenImages.get(0).getThumbnailPath());
//        continuess.setVisibility(View.VISIBLE);
        continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.appcolor));

        Log.e(TAG, profileImageChanged.getAbsolutePath());
        RequestBody certificate;
        switch (pos) {
            case 1:
                pdl1.setVisibility(View.GONE);
                addDocument.setVisibility(View.VISIBLE);
                MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", profileImageChanged.getName(),
                        RequestBody.create(MediaType.parse("image/*"), profileImageChanged));

                Glide.with(this).load(profileImageChanged).into(addDocument);
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Medical Certificate");
                mutiParts.add(0, filePart1);
                AppPreferences.newInstance().setFile1(UploadDocumentActivity.this, chosenImages.get(0).getThumbnailPath());

                break;
            case 2:
                pdl2.setVisibility(View.GONE);
                addDocument2.setVisibility(View.VISIBLE);
                MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", profileImageChanged.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
                Glide.with(this).load(profileImageChanged).into(addDocument2);
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Medical Certificate");
                AppPreferences.newInstance().setFile2(UploadDocumentActivity.this, chosenImages.get(0).getThumbnailPath());

                mutiParts.add(1, filePart2);
                break;
            case 3:
                pdl3.setVisibility(View.GONE);
                addDocument3.setVisibility(View.VISIBLE);
                MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", profileImageChanged.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
                Glide.with(this).load(profileImageChanged).into(addDocument3);

                certificate = RequestBody.create(MediaType.parse("text/plain"), "Graduation Degree Certificate");
                AppPreferences.newInstance().setFile3(UploadDocumentActivity.this, chosenImages.get(0).getThumbnailPath());


                mutiParts.add(2, filePart3);
                break;
            case 4:
                pdl4.setVisibility(View.GONE);
                addDocument4.setVisibility(View.VISIBLE);
                MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file2", profileImageChanged.getName(),
                        RequestBody.create(MediaType.parse("image/*"), profileImageChanged));

                Glide.with(this).load(profileImageChanged).into(addDocument4);
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Graduation Degree Certificate");
                AppPreferences.newInstance().setFile4(UploadDocumentActivity.this, chosenImages.get(0).getThumbnailPath());

                mutiParts.add(3, filePart4);
                break;
            default:
                mutiParts.clear();
                break;
        }
    }

    @Override
    public void onError(String s) {
        Glide.with(this).load(R.drawable.plus).into(addDocument);
        Glide.with(this).load(R.drawable.plus).into(addDocument2);
        Glide.with(this).load(R.drawable.plus).into(addDocument3);
        Glide.with(this).load(R.drawable.plus).into(addDocument4);

        mutiParts.clear();
        Toast toast=Toast.makeText(this, "Error " + s, Toast.LENGTH_SHORT);
        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(UploadDocumentActivity.this, android.R.color.holo_red_dark));
        toast.show();
    }

    @Override
    public void onFilesChosen(List<ChosenFile> chosenFile)
    {
        File profileImageChanged = new File(chosenFile.get(0).getOriginalPath());
        Log.e("path",profileImageChanged.getName()+","+chosenFile.get(0).getOriginalPath());
//        continuess.setVisibility(View.VISIBLE);
        continuess.setBackgroundColor(ContextCompat.getColor(UploadDocumentActivity.this,R.color.appcolor));

        Log.e(TAG, profileImageChanged.getAbsolutePath());
        RequestBody certificate;
        switch (pos) {
            case 1:
                MultipartBody.Part filePart1 = MultipartBody.Part.createFormData("file1", profileImageChanged.getName(),
                        RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
               addDocument.setVisibility(View.GONE);
               pdl1.setVisibility(View.VISIBLE);
                textpdf1.setText(profileImageChanged.getName());
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Medical Certificate");
                mutiParts.add(0, filePart1);
                AppPreferences.newInstance().setFile1(UploadDocumentActivity.this, chosenFile.get(0).getOriginalPath());

                break;
            case 2:
                MultipartBody.Part filePart2 = MultipartBody.Part.createFormData("file2", profileImageChanged.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageChanged));

                addDocument2.setVisibility(View.GONE);
                pdl2.setVisibility(View.VISIBLE);
                textpdf2.setText(profileImageChanged.getName());
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Medical Certificate");
                AppPreferences.newInstance().setFile2(UploadDocumentActivity.this, chosenFile.get(0).getOriginalPath());

                mutiParts.add(1, filePart2);
                break;
            case 3:
                MultipartBody.Part filePart3 = MultipartBody.Part.createFormData("file1", profileImageChanged.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageChanged));

                addDocument3.setVisibility(View.GONE);
                pdl3.setVisibility(View.VISIBLE);
                textpdf3.setText(profileImageChanged.getName());
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Graduation Degree Certificate");
                AppPreferences.newInstance().setFile3(UploadDocumentActivity.this, chosenFile.get(0).getOriginalPath());


                mutiParts.add(2, filePart3);
                break;
            case 4:
                MultipartBody.Part filePart4 = MultipartBody.Part.createFormData("file2", profileImageChanged.getName(),
                        RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
                addDocument4.setVisibility(View.GONE);
                pdl4.setVisibility(View.VISIBLE);
                textpdf4.setText(profileImageChanged.getName());
                certificate = RequestBody.create(MediaType.parse("text/plain"), "Graduation Degree Certificate");
                AppPreferences.newInstance().setFile4(UploadDocumentActivity.this, chosenFile.get(0).getOriginalPath());

                mutiParts.add(3, filePart4);
                break;
            default:
                mutiParts.clear();
                break;
        }
    }



    public void openPdf(String file)
    {
        String pdf_url=APIURL.IMAGE_DOCUMENT_URL+file;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
        startActivity(browserIntent);
    }
}
