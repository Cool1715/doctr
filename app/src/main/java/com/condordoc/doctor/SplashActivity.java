package com.condordoc.doctor;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.condordoc.doctor.model.user.profile.MyProfileModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;

import androidx.core.content.ContextCompat;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CALL_PHONE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private static final int MY_PERMISSIONS_REQUEST_Call_Contacts = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            if (!isNetworkConnected()) {
               Toast toast= Toast.makeText(this, getString(R.string.no_internet_connected) + "", Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SplashActivity.this, android.R.color.holo_red_dark));
                toast.show();
//                finish();
                showDialog();
                return;
            }

            Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(SplashActivity.this));
            ApiManager apiManager = retrofit.create(ApiManager.class);
            apiManager.myProfile()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<MyProfileModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(MyProfileModel myProfileModel) {
                            if (myProfileModel != null && myProfileModel.getStatus().equalsIgnoreCase("true")) {
                                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                                finish();
                            } else {
                                String error = myProfileModel != null ? myProfileModel.getError() : "login error";
                             Toast toast=   Toast.makeText(SplashActivity.this, "" + error, Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SplashActivity.this, android.R.color.holo_red_dark));
                                toast.show();
                            }
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            callIntroduction();
                        }
                    });
        }

    }

    private void showDialog() {
        AlertDialog.Builder builderDia = new AlertDialog.Builder( this);
        builderDia.setTitle("No Internet Connection");
        builderDia.setCancelable(false);
        builderDia.setMessage("You need to have Mobile Internet or Wifi Connection to access this.\n\nPress OK to Exit");
        builderDia.setPositiveButton("OK", (dialogInterface, i) -> finish());
        builderDia.show();
    }

    private void callIntroduction() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the MainActivity. */
                Intent mainIntent = new Intent(SplashActivity.this, IntroActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, 2000);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.e("gdgdgdg", "" + requestCode);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_Call_Contacts: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("gdgdgdg", "" + requestCode + "  " + grantResults.length);

                    if (!hasPermissions(this, PERMISSIONS)) {

                        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                    } else {
                        Log.e("sdsdsd", "else");
                        callIntroduction();
                    }

                    return;
                } else {
                }
            }
            return;
        }

        return;
    }

}
