package com.condordoc.doctor;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.adapter.OnBottomReachedListener;
import com.condordoc.doctor.adapter.DoctorChatAdapter;
import com.condordoc.doctor.event.MessageEvent;
import com.condordoc.doctor.model.doctor_chat.DoctorChatModel;
import com.condordoc.doctor.model.doctor_chat.StoreChatModel;
import com.condordoc.doctor.model.job.jobbyId.Data;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DoctorChatActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static String PATIENT_IMAGE_URL_FULL;
    private DoctorChatAdapter adapter;
    private RecyclerView recyclerView;
    private ImageView sendMessageButton;
    private EditText messageEditTextView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private JobByIdModel getJobModel;
    String jobId;
    public static  String DOCTOR_IMAGE_URL_FULL;
    private String patinetId = "0";

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }


    @BindView(R.id.header)
    TextView header;


    ApiManager apiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.START_JOB_ID)){
            jobId = getIntent().getStringExtra(AppConstant.START_JOB_ID);

            getJobById(jobId);
        }



        initializationUI();
        setListeners();
        getDoctorMessage();
        adapter = new DoctorChatAdapter(new ArrayList<>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                //your code goes here
                Log.d("TAG", "onBottomReached");
            }
        });

        sendMessageByDoctor();
    }



    private void getJobById(String jobId) {
        apiManager.getJobById(jobId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<JobByIdModel>() {
            @Override
            public void onSubscribe(Disposable d) {
              //  Toast.makeText(DoctorChatActivity.this, "End", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNext(JobByIdModel getJobModel) {
                try {
                    if (getJobModel != null && getJobModel.getStatus().equalsIgnoreCase("true")) {
                        DoctorChatActivity.this.getJobModel = getJobModel;

                        Data item = getJobModel.getData();

                        patinetId = item.getPatientId() + "";

                        header.setText(String.format("%s",item.getPatientName()));

                        DOCTOR_IMAGE_URL_FULL = APIURL.IMAGE_USER_URL + getJobModel.getData().getImage();
                        PATIENT_IMAGE_URL_FULL = APIURL.IMAGE_USER_URL + getJobModel.getData().getImagePatient();
                        getDoctorMessage();


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onError(Throwable e) {
               // Toast.makeText(DoctorChatActivity.this, ""+e, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onComplete() {

            }
        });
    }


    private void setListeners() {
        sendMessageButton.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void initializationUI() {
        recyclerView = findViewById(R.id.chatHistoryRecyclerView);
        sendMessageButton = findViewById(R.id.sendMessage);
        messageEditTextView = findViewById(R.id.editTextMessage);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {  //"Patient Chat only backend"
            case R.id.sendMessage: {
                if (TextUtils.isEmpty(messageEditTextView.getText().toString().trim())) {
                    return;
                }
                sendMessageByDoctor();

            }
            break;
        }
    }

    private void sendMessageByDoctor() {
        ProgressDialog pd=new ProgressDialog(DoctorChatActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        String message = messageEditTextView.getText().toString().trim();

        apiManager.sendMessage(patinetId, "" +
                        message)
                .subscribeOn(Schedulers.io())

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<StoreChatModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        pd.dismiss();

                    }

                    @Override
                    public void onSuccess(StoreChatModel doctorChatStoreModel) {
                        pd.dismiss();
                        try {
                            messageEditTextView.setText("");
//                            Datum datum = new Datum();
//                            datum.setDoctorId(AppPreferences.newInstance().getUserId(DoctorChatActivity.this));
//                            datum.setPatientId(AppPreferences.newInstance().getPatientId(DoctorChatActivity.this));
//                            datum.setMessageD(""+message);
//                            adapter.addItem(datum);
                            sendMessage();
//                            Toast.makeText(DoctorChatActivity.this, "Send Message", Toast.LENGTH_SHORT).show();
                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                });
    }

    private void sendMessage() {

        getDoctorMessage();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        getDoctorMessage();
    };

    private void getDoctorMessage() { // TODO
        apiManager.getDoctorMessage(
                patinetId +"")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DoctorChatModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onSuccess(DoctorChatModel doctorChatModel) {
                        try {
                            if (doctorChatModel != null && doctorChatModel.getStatus().equalsIgnoreCase("true")) {
                                adapter.addItems(doctorChatModel.getData());
                            }
                        }catch (Exception e){

                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void onRefresh() {

        apiManager.getDoctorMessage(
                patinetId +"")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<DoctorChatModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(DoctorChatModel doctorChatModel) {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDoctorMessage();
    }
}
