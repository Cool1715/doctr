package com.condordoc.doctor;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsActivity extends LocalizationActivity {


    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @Nullable
    @OnClick(R.id.continuess)
    void oncontinue() {
        if (editText.getText().toString().isEmpty()){
            editText.setError( "Please provide reason" );
        }else if (editText1.getText().toString().isEmpty()){
            editText1.setError( "Please provide query" );
        }else {
            Toast toast=Toast.makeText(this, "Query Sent!!!", Toast.LENGTH_SHORT);
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(ContactUsActivity.this, android.R.color.holo_red_dark));
            toast.show();
            finish();
        }
        
    }
    
    @BindView( R.id.input_firstname)
    EditText editText;

    @BindView( R.id.input_query)
    EditText editText1;
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);


    }
}
