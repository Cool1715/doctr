
package com.condordoc.doctor;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.bumptech.glide.Glide;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.model.feedback.FeedbackGetModel;
import com.condordoc.doctor.model.feedback.FeedbackModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.GetAddressUitls;

import java.util.List;
import java.util.Objects;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
public class ReceiptActivity extends LocalizationActivity {

    private ApiManager apiManager;
    private JobByIdModel jobByIdModel;

    @BindView(R.id.receipt_doctor_name)
    TextView receipt_doctor_name;
    @BindView(R.id.receipt_bookingId)
    TextView receipt_bookingId;
    @BindView(R.id.receipt_date_time)
    TextView receipt_date_time;
    @BindView(R.id.receipt_location)
    TextView receipt_location;
    @BindView(R.id.receipt_doctor_disease)
    TextView receipt_doctor_disease;
    @BindView(R.id.receipt_service_cost)
    TextView receipt_service_cost;
    @BindView(R.id.receipt_subtotal_cost)
    TextView receipt_subtotal_cost;
    @BindView(R.id.receipt_circular_image)
    CircleImageView receipt_circular_image;


    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @OnClick(R.id.sendreceipt)
    void sendReceipt() {
        CommonUtils.showProgressDialog( getBaseContext() );
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey( AppConstant.JOB_ID )) {
            int jobId = getIntent().getIntExtra( AppConstant.JOB_ID, 0 );
            apiManager.getJobById( String.valueOf( jobId ) )
                    .subscribeOn( Schedulers.io() )
                    .observeOn( AndroidSchedulers.mainThread() )
                    .subscribe( new Consumer<JobByIdModel>() {
                        @Override
                        public void accept(JobByIdModel jobByIdModel) throws Exception {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            try {
                                if (jobByIdModel != null && jobByIdModel.getStatus().equalsIgnoreCase( "true" )) {
                                    Toast.makeText( ReceiptActivity.this, "" + jobByIdModel.getMessage(), Toast.LENGTH_SHORT ).show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            Toast.makeText( ReceiptActivity.this, "Something went wrong.", Toast.LENGTH_SHORT ).show();
                        }
                    } );
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_receipt );
        ButterKnife.bind( this );
        final RatingBar receipt_job_rating = findViewById( R.id.receipt_job_rating );
        final RatingBar doctor_rating_profile = findViewById( R.id.doctor_rating_profile );
        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this ) );
        apiManager = retrofit.create( ApiManager.class );

//        Glide.with( this )
//                .load( AppPreferences.newInstance().getProfilePath( this ) )
//                .error( R.drawable.user ).into( receipt_circular_image );

        if (getIntent() != null && Objects.requireNonNull( getIntent().getExtras() ).containsKey( AppConstant.JOB_ID )) {
            int jobId = getIntent().getIntExtra( AppConstant.JOB_ID, 0 );
            CommonUtils.showProgressDialog( getBaseContext() );
            apiManager.getJobById( String.valueOf( jobId ) )
                    .subscribeOn( Schedulers.io() )
                    .observeOn( AndroidSchedulers.mainThread() )
                    .subscribe( new Consumer<JobByIdModel>() {
                        @Override
                        public void accept(JobByIdModel jobByIdModel) throws Exception {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            try {
                                if (jobByIdModel != null && jobByIdModel.getStatus().equalsIgnoreCase( "true" )) {
                                    ReceiptActivity.this.jobByIdModel = jobByIdModel;

                                    Glide.with( ReceiptActivity.this )
                                            .load( APIURL.IMAGE_USER_URL + jobByIdModel.getData().getImage() )
                                            .error( R.drawable.user )
                                            .into( receipt_circular_image );

                                    System.out.println("imageeeee: "+jobByIdModel.getData().getImage());

                                    receipt_doctor_name.setText( jobByIdModel.getData().getDoctorName() );
                                    receipt_bookingId.setText( String.format( "Booking #%s ", jobByIdModel.getData().getId() ) );

                                    String date_of_booking = jobByIdModel.getData().getDateOfBooking();
                                    String date_of_time = jobByIdModel.getData().getDateOfTime();

                                    receipt_date_time.setText( date_of_booking +"/" +date_of_time );

                                    Double doctor_lat = Double.valueOf(jobByIdModel.getData().getPatientLat());
                                    Double doctor_long = Double.valueOf(jobByIdModel.getData().getPatientLong());
                                    Log.e("receiptAddress",GetAddressUitls.getCompleteAddressString(ReceiptActivity.this, doctor_lat, doctor_long));
                                    receipt_location.setText( GetAddressUitls.getCompleteAddressString(ReceiptActivity.this, doctor_lat, doctor_long));

                                    receipt_doctor_disease.setText(jobByIdModel.getData().getDoctorName() + " : " + jobByIdModel.getData().getDiseaseName()  );
                                    receipt_service_cost.setText( AppConstant.CURRENCY_SYMBOL +" "+ jobByIdModel.getData().getPrice()+".00" );

                                    receipt_subtotal_cost.setText( AppConstant.CURRENCY_SYMBOL + " "+jobByIdModel.getData().getPrice()+".00" );




                                  Toast toast=  Toast.makeText( ReceiptActivity.this, "Show details", Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(ReceiptActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            } catch (Exception e) {
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            CommonUtils.disMissProgressDialog( getBaseContext() );

                        }
                    } );
        }


        apiManager.getFeedbackModel( AppPreferences.newInstance().getUserId( ReceiptActivity.this ) )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<List<FeedbackGetModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(List<FeedbackGetModel> feedbackGetModels) {
                        int total = 0, count = 0;
                        float average = 0;
                        for (FeedbackGetModel model : feedbackGetModels) {
                            if (model != null && model.getRating() != null) {
                                double rating = Double.parseDouble( model.getRating() );
                                total = total + (int) rating;
                                count = count + 1;
                                average = total / count;
                            }

                        }
                        if (average >= Float.valueOf( "5" )) {
                            doctor_rating_profile.setRating( 5 );
                        } else doctor_rating_profile.setRating( average );

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        doctor_rating_profile.setRating( 0 );
                    }
                } );
//        doctor_id:3
//    patient_id:1
//    feedback:test
//    rating:2
        receipt_job_rating.setOnRatingBarChangeListener( new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                String message = "";
                int rate = (int) rating;
                if (rate == 1) {
                    message = "Very Poor";
                } else if (rate == 2) {
                    message = "Poor";
                } else if (rate == 3) {
                    message = "Good";
                } else if (rate == 4) {
                    message = "Very Good";
                } else if (rate == 5) {
                    message = "Excellent";
                }
                if (jobByIdModel != null && jobByIdModel.getData().getDoctorId() > 0) {
                    ProgressDialog pd = new ProgressDialog( ReceiptActivity.this );
                    pd.show();
                    pd.setMessage( "Loading..." );
                    pd.setCancelable( false );
                    CommonUtils.showProgressDialog( getBaseContext() );
                    apiManager.postFeedbackModel
                            ( AppPreferences.newInstance().getUserId( ReceiptActivity.this ),
                                    jobByIdModel.getData().getPatientId(),
                                    message,
                                    rating )
                            .subscribeOn( Schedulers.io() )
                            .observeOn( AndroidSchedulers.mainThread() )
                            .subscribe( new SingleObserver<FeedbackModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    pd.dismiss();
                                }

                                @Override
                                public void onSuccess(FeedbackModel feedbackModel) {
                                    pd.dismiss();
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                    try {
                                        if (feedbackModel != null && feedbackModel.getMessage() != null) {
                                           Toast toast= Toast.makeText( ReceiptActivity.this, "" + feedbackModel.getMessage(), Toast.LENGTH_SHORT );
                                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(ReceiptActivity.this, android.R.color.holo_red_dark));
                                            toast.show();
                                        } else {
                                            if (feedbackModel != null && feedbackModel.getStatus().equalsIgnoreCase( "false" )) {
                                                Toast toast=Toast.makeText( ReceiptActivity.this, "" + feedbackModel.getMessage(), Toast.LENGTH_SHORT );
                                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(ReceiptActivity.this, android.R.color.holo_red_dark));
                                                toast.show();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onError(Throwable t) {
                                    pd.dismiss();
                                    CommonUtils.disMissProgressDialog( getBaseContext() );
                                }
                            } );
                } else {
                    Toast toast=Toast.makeText( ReceiptActivity.this, "Not found patient", Toast.LENGTH_SHORT );
                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(ReceiptActivity.this, android.R.color.holo_red_dark));
                    toast.show();
                }
            }
        } );
    }


}




