package com.condordoc.doctor;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
public class PriceDialogue extends AppCompatDialogFragment {

    EditText selectedTime, addPrice;
    private AddData listener;
    
    public static PriceDialogue newInstance(Bundle args) {
        PriceDialogue fragment = new PriceDialogue();
        fragment.setArguments( args );
        return fragment;
    }
    

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //        selectedTime.setText( dataPrice[0] );
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate( R.layout.dialogue_price_to_time, null );
        builder.setView( view )
                .setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                        
                    }
                } )
                .setPositiveButton( "Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String time = selectedTime.getText().toString();
                        String price = addPrice.getText().toString();
                        listener.applyText( time, price );
                    }
                } );

        selectedTime = view.findViewById( R.id.selectedTime );
        addPrice = view.findViewById( R.id.addPrice );
        
        Bundle bundle = new Bundle(  );
        if(null != getArguments()){
        bundle = getArguments();
        String time = bundle.getString( "TIME_KEY" );
        selectedTime.setText( time );
        selectedTime.setEnabled( false );
        }   
        
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach( context );
        try {
            listener = (AddData) context;
        } catch (ClassCastException e) {
            throw new ClassCastException( context.toString() + "implement priceShowListener" );
        }
    }

    public interface AddData {
        void applyText(String time, String price);
    }

}
