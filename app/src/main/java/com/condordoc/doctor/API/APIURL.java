package com.condordoc.doctor.API;

/**
 * Created by Diwakar on 8/9/2017.
 */

public interface APIURL {

    //String BASE_URL = "http://dumpin.in:8003";
    String BASE_URL = "https://api.urbesalud.com/";
   // String IMAGE_DOCUMENT_URL = "http://dumpin.in:8003/uploads/";
   String IMAGE_DOCUMENT_URL = "https://api.urbesalud.com/uploads/";
   // String IMAGE_USER_URL = "http://dumpin.in:8003/users/";
    String IMAGE_USER_URL = "https://api.urbesalud.com/users/";
    String PREFIX = "/api/auth/";
 String Location_UPDATE = PREFIX + "updatedoctorloc";
//    String BASE_URL = "https://dumpin.in";
//    String IMAGE_DOCUMENT_URL = "https://dumpin.in/salud_api/public/uploads/";
//    String IMAGE_USER_URL = "https://dumpin.in/salud_api/public/users/";
//    String PREFIX = "/salud_api/public/api/auth/";

    //    String BASE_URL = "http://192.168.1.71:8000/";
    ///////////*Customer_Apis*////////////////////////////////////////////////////////////
    String REGISTER = PREFIX + "register"; // update
    String API_AUTH_LOGIN = PREFIX + "login"; // update
    String API_AUTH_ME = PREFIX + "me"; // update
    String API_AUTH_LOGOUT = PREFIX + "logout"; // update
    String API_AUTH_REFRESH = PREFIX + "refresh";

    String MENU = PREFIX + "menus";

    String DISEASE_GET = PREFIX + "disease";
    String DISEASE_POST = PREFIX + "disease";

    String FIREBASE_TOKEN = PREFIX + "firebase";
    String FAKE_EMAIL_ID = PREFIX + "verifyfake/{emailId}";

    String DOCTOR_DISEASE_GET = PREFIX + "disease-doctor";
    String DOCTOR_DISEASE_POST = PREFIX + "disease-doctor";

    String SCHEDULE_GET = PREFIX + "schedule";
    String SCHEDULE_DATE_GET = PREFIX + "getScheduleByDate/{date}"; // 2019-10-14
    String SCHEDULE_POST = PREFIX + "schedule";
    String SCHEDULE_UPDATE = PREFIX + "schedule/{id}";

    String SCHEDULE_MULTIPLE_POST = PREFIX + "multischedule";
    String SCHEDULE_DEL_MULTIPLE_POST = PREFIX + "delmultischedule";

    //    String SCHEDULE_MULTIPLE_POST = "/api/auth/disease/1";
    String GET_DISEASE = PREFIX + "disease/{manage_service_id}";


    String POST_DISEASE_DOCTOR = PREFIX + "disease-doctor";
    String GET_DISEASE_DOCTOR=PREFIX+"disease-doctor";

    String FEEDBACK_POST = PREFIX + "feedback";
    String FEEDBACK_GET = PREFIX + "feedback/{id}";

    String JOBS_GET = PREFIX + "jobs";
    String JOBS_GET_BY_ID = PREFIX + "jobs/{id}";
    String JOBS_POST = PREFIX + "jobs";

//    String JOBS_DELETE = "/auth/api/jobs/{id}"; // pending
//    String JOBS_PUT = "/api/auth/jobs/{id}"; // pending

    String store_chat = PREFIX + "chat/store";
    String doctor_chat = PREFIX + "chat/doctor";
    String chat_get = PREFIX + "chat/get";

   String tnc = PREFIX + "tnc/tnc";
   String about = PREFIX + "tnc/aboutus";
   String privacy = PREFIX + "tnc/privacypolicy";
   String contact = PREFIX + "tnc/contactus";
   String faq = PREFIX + "tnc/faq";


    String DOCTOR_DESCRIPTION = PREFIX + "doctor-description";


    String GET_CURRENCY = PREFIX + "currency";
    String GET_LANGUAGE = PREFIX + "language";

    String POST_EMERGENCY = PREFIX + "emergencyContact";
    String GET_EMERGENCY = PREFIX + "emergencyContact";
    String DELETE_EMERGENCY = PREFIX + "emergencyContact/{id}";
    String UPDATE_EMERGENCY = PREFIX + "emergencyContact/{id}";

    String POST_BANK = PREFIX + "bank";
    String GET_BANK = PREFIX + "bank";

    String POST_DOCUMENT = PREFIX + "document";
    String GET_DOCUMENT = PREFIX + "document";

/*

    String jobAcceptedByDoctor = PREFIX + "jobAcceptedByDoctor/{id}";
    String jobDeniedByDoctor = PREFIX + "jobDeniedByDoctor/{id}";
    String jobCompletedByDoctor = PREFIX + "jobCompletedByDoctor/{id}";

    String JOB_ACCEPT_DOCTOR = "/api/auth/setjobAcceptedDoctor/{id}";
    String JOB_DENIED_DOCTOR = "/api/auth/setJobDeniedDoctor/{id}";
    String JOB_COMPLETED_DOCTOR = "/api/auth/setJobCompletedDoctor/{id}";


    String JOB_START_DOCTOR = "/api/auth/setJobStarted/{id}";
    String JOB_ACCEPTED_BUT_CANCEL_DOCTOR = "/api/auth/setJobAcceptedButCancel/{id}";
*/

    String getPendingJobs = PREFIX +"getJobPendingDoctor";
    String getAcceptJobs = PREFIX + "getjobAcceptedDoctor";
    String getJobDeniedDoctor = PREFIX +"getJobDeniedDoctor";
    String getJobCompletedDoctor = PREFIX +"getJobCompletedDoctor";
    String getJobStarted = PREFIX +"getJobStarted";
    String getJobAcceptedButCancel = PREFIX +"getJobAcceptedButCancel";





    String postWithdrawRequest = PREFIX +"payOut";

      /*    Route::get('getJobPendingDoctor','doctor\DoctorJobController@getJobPending');
    Route::get('getjobAcceptedDoctor','doctor\DoctorJobController@getJobAccepted');
    Route::get('getJobDeniedDoctor','doctor\DoctorJobController@getJobDenied');
    Route::get('getJobCompletedDoctor','doctor\DoctorJobController@getJobCompleted');
    Route::get('getJobStarted','doctor\DoctorJobController@getJobStated');
    Route::get('getJobAcceptedButCancel','doctor\DoctorJobController@getJobAcceptedButCancel');*/


    String SET_LOCATION = PREFIX + "setUserLocation";
    String SET_JOB_ACCEPTED_DOCTOR = PREFIX + "setjobAcceptedDoctor/{id}";
    String SET_DOC_ARRIVED = PREFIX + "setJobArrived/{id}";
    String SET_DOC_BEGIN_JOB= PREFIX + "setJobArrivedComplete/{id}";
    String SET_JOB_DENIED_DOCTOR = PREFIX + "setJobDeniedDoctor/{id}";
    String SET_JOB_COMPLETED_DOCTOR = PREFIX + "setJobCompletedDoctor/{id}";
    String SET_JOB_STARTED = PREFIX + "setJobStarted/{id}";
    String SET_JOB_ACCEPTED_BUT_CANCEL = PREFIX + "setJobAcceptedButCancel/{id}";

    String GET_RECEIPT = PREFIX + "jobs";
    String JOBS_ID = PREFIX + "jobs";


    /*Route::get('setjobAcceptedDoctor/{id}','doctor\DoctorJobController@setJobAccepted');
Route::get('setJobDeniedDoctor/{id}','doctor\DoctorJobController@setJobDenied');
Route::get('setJobCompletedDoctor/{id}','doctor\DoctorJobController@setJobCompleted');
Route::get('setJobStarted/{id}','doctor\DoctorJobController@setJobStated');
Route::get('setJobAcceptedButCancel/{id}','doctor\DoctorJobController@setJobAcceptedButCancel');
*/

    String UPDATE_PROFILE = PREFIX + "edit/profile";

    String PASSWORD_CHAMGE = PREFIX + "changepassword";
    String FORGOT_PASSWORD = PREFIX + "forgot-password/{email}";
    String google = PREFIX + "google";
    String facebook = PREFIX + "idfacebook";


    ////////////////////*Driver_Apis*///////////////////////////////////////////////////////
    String MyOrdersDriver = BASE_URL + "myorder.php";
    String Changeorderstatus = BASE_URL + "updateorder.php";
    String Availability = BASE_URL + "availability.php";
    String Editdestination = BASE_URL + "update_destination.php";
    String CheckMobile = BASE_URL + "get_customer.php";
    String DriverStatus = BASE_URL + "getdriverstatus.php";
    String MessagetoCustomer = BASE_URL + "clientpush.php";
    String GetAllStatus = BASE_URL + "getallstatus.php";
    String BlockedDriver = BASE_URL + "settimings.php";

    /////////////*Notification*//////////////////////////////////////////////////////////////
    String UpdateRegistrationtoken = BASE_URL + "registerdevice.php";
    String NotificationList = BASE_URL + "notificationlist.php";

    String Activity = "activity";
    String Booknow = "booknow";
    String Booklater = "booklater";
    String Payment = "payment";


    /*Route::get('setjobAcceptedDoctor/{id}','doctor\DoctorJobController@setJobAccepted');
Route::get('setJobDeniedDoctor/{id}','doctor\DoctorJobController@setJobDenied');
Route::get('setJobCompletedDoctor/{id}','doctor\DoctorJobController@setJobCompleted');
Route::get('setJobStarted/{id}','doctor\DoctorJobController@setJobStated');
Route::get('setJobAcceptedButCancel/{id}','doctor\DoctorJobController@setJobAcceptedButCancel');
*/


    String MANAGE_SERIVCE = PREFIX + "services";

    String addBalance = PREFIX + "addBalance";
}