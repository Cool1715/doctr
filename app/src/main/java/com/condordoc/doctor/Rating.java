package com.condordoc.doctor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.condordoc.doctor.adapter.RatingAdapter;
import com.condordoc.doctor.model.feedback.FeedbackGetModel;
import com.condordoc.doctor.model.feedback.RatingGet;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.model.user.description.DescriptionModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class Rating extends LocalizationActivity {
    private String TAG = Rating.class.getSimpleName();

    private RatingAdapter topDownAdapter;


    @BindView(R.id.main_recycler_view)
    RecyclerView TransactionRecyclerview;

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);

        TransactionRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        topDownAdapter = new RatingAdapter(new ArrayList<>());
        TransactionRecyclerview.setVisibility(View.VISIBLE);
        TransactionRecyclerview.setAdapter(topDownAdapter);

      Log.e("doctorid",""+AppPreferences.newInstance().getUserId(Rating.this));
        ApiManager apiManager = App.retrofit(AppPreferences.newInstance().getToken(this))
                .create(ApiManager.class);

        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance(this);
        progressDialogUtils.showDialog("Rating...");
        apiManager.getRatingModel(AppPreferences.newInstance().getUserId(Rating.this))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<RatingGet>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(RatingGet doctorDiseaseModel) {

                        if (doctorDiseaseModel != null ) {
                            topDownAdapter.updateList(doctorDiseaseModel.getData());
                            progressDialogUtils.hideDialog();
                        } else {
                            Toast toast=Toast.makeText(Rating.this, "", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(Rating.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                       Toast toast= Toast.makeText(Rating.this, " " + e.getMessage(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(Rating.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                });

    }
}