package com.condordoc.doctor;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.GetScheduleAdapter;
import com.condordoc.doctor.adapter.SetTimeAvailabilityAdapter;
import com.condordoc.doctor.model.schedule.DataItem;
import com.condordoc.doctor.model.schedule.DelScheduleItem;
import com.condordoc.doctor.model.schedule.DelScheduleModel;
import com.condordoc.doctor.model.schedule.GetScheduleModel;
import com.condordoc.doctor.model.schedule.MultipleScheduleModel;
import com.condordoc.doctor.model.schedule.RequestScheduleModel;
import com.condordoc.doctor.model.schedule.ScheduleItem;
import com.condordoc.doctor.model.schedule.TimeScheduleModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.GetAddressUitls;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SetTimeAvailabilityActivity extends LocalizationActivity implements SetTimeAvailabilityAdapter.OnAddPriceClick, PriceDialogue.AddData, SetTimeAvailabilityAdapter.OnChangeTimeAvailabilitiesListener {

    private static final String TAG = SetTimeAvailabilityActivity.class.getSimpleName();

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    private List<ScheduleItem> scheduleItems;
    private List<DelScheduleItem> delScheduleItems;
    private Map<String, ScheduleItem> mapScheduleItems;
    private Map<String, DelScheduleItem> deMapScheduleItems;
    String data[] = new String[2];


    @Nullable
    @OnClick(R.id.continuess)
    void oncontinue() {
//        Toast.makeText(this, "Updates done", Toast.LENGTH_SHORT).show();

        final RequestScheduleModel request = new RequestScheduleModel();

        scheduleItems = new ArrayList<>(mapScheduleItems.values());

        if (scheduleItems.size() > 0) {
            request.setSchedule(scheduleItems);
            Gson gson = new Gson();
            String json = gson.toJson(request);
            Log.d(TAG, "App Schedule : " + json);
            ProgressDialog pd=new ProgressDialog(SetTimeAvailabilityActivity.this);
            pd.show();
            pd.setMessage("Loading...");
            pd.setCancelable(false);

            CommonUtils.showProgressDialog(getBaseContext());
            apiManager.postMultipleSchedule(json)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<MultipleScheduleModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            pd.dismiss();

                        }

                        @Override
                        public void onSuccess(MultipleScheduleModel multipleScheduleModel) {
                            pd.dismiss();
                            CommonUtils.disMissProgressDialog(getBaseContext());
                            mapScheduleItems.clear();
                            if (multipleScheduleModel != null && multipleScheduleModel.getStatus().equalsIgnoreCase("true")) {
                                Toast toast=Toast.makeText(SetTimeAvailabilityActivity.this, "" + multipleScheduleModel.getMessage(), Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SetTimeAvailabilityActivity.this, android.R.color.holo_red_dark));
                                toast.show();

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            pd.dismiss();
                            try {
                                CommonUtils.disMissProgressDialog(getBaseContext());
                                if (e != null && e.getMessage().contains("500")) {
                                  Toast toast=  Toast.makeText(SetTimeAvailabilityActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SetTimeAvailabilityActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                } else {
                                    Toast toast = Toast.makeText(SetTimeAvailabilityActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT);
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SetTimeAvailabilityActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }
                            } catch (Exception ex) {

                            }
                        }
                    });


        }
        final DelScheduleModel delScheduleItem = new DelScheduleModel();

        delScheduleItems = new ArrayList<>(deMapScheduleItems.values());

        if (delScheduleItems.size() > 0) {

            delScheduleItem.setSchedule(delScheduleItems);
            Gson gson = new Gson();
            String json = gson.toJson(delScheduleItem);
            Log.d(TAG, "Delete Multiple Schedule : " + json);
            CommonUtils.showProgressDialog(getBaseContext());
            apiManager.postDelMultipleSchedule(json)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<MultipleScheduleModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(MultipleScheduleModel multipleScheduleModel) {
                            delScheduleItems.clear();
                            CommonUtils.disMissProgressDialog(getBaseContext());
                            if (multipleScheduleModel != null && multipleScheduleModel.getStatus().equalsIgnoreCase("true")) {
                                Log.d(TAG, "onSuccess: " + mapScheduleItems.toString());
                                customAdapter.removeItemCheckedList(formItemList);
                                formItemList.clear();
//                                Toast.makeText(SetTimeAvailabilityActivity.this, "" + multipleScheduleModel.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            CommonUtils.disMissProgressDialog(getBaseContext());
//                            Toast.makeText(SetTimeAvailabilityActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    @BindView(R.id.simpleGridView)
    GridView gridView;

    @BindView(R.id.head)
    TextView head;

    @BindView(R.id.get_schedule_recycler_view)
    RecyclerView get_schedule_recycler_view;

    String time = "09:30-10:30";
    static String abc[] = {
            
            
            "00:00-00:30",
            "00:30-01:00",
            "01:00-01:30",
            "01:30-02:00",
            "02:00-02:30",
            "02:30-03:00",
            "03:00-03:30",
            "03:30-04:00",
            "04:00-04:30",
            "04:30-05:00",
            "05:00-05:30",
            "05:30-06:00",
            "06:00-06:30",
            "06:30-07:00",
            "07:00-07:30",
            "07:30-08:00",
            "08:00-08:30",
            "08:30-09:00",

            "09:00-09:30",
            "09:30-10:00",
            "10:00-10:30",
            "10:30-11:00",
            "11:00-11:30",
            "11:30-12:00",
            "12:00-12:30",
            "12:30-13:00",
            "13:00-13:30",
            "13:30-14:00",
            "14:00-14:30",
            "14:30-15:00",
            "15:00-15:30",
            "15:30-16:00",
            "16:00-16:30",
            "16:30-17:00",
            "17:00-17:30",
            "17:30-18:00",
            "18:00-18:30",
            "18:30-19:00",
            "19:00-19:30",
            "19:30-20:00",

            "20:00-20:30",
            "20:30-21:00",
            "21:00-21:30",
            "21:30-22:00",
            "22:00-22:30",
            "22:30-23:00",
            "23:00-23:30",
            "23:30-00:00",

    };
    String abc2[] = {"0", "0", "0", "0", "0", "0"};

    static List<TimeScheduleModel> timeScheduleModels;
    static Map<String, Boolean> map;
    static Map<String, DataItem> idMap;



    static String currentDate;
    static String prevDate;
    static String dayOfTheWeek;
    SetTimeAvailabilityAdapter customAdapter;

    private ApiManager apiManager;

    List<String> formItemList;

    private static List<TimeScheduleModel> getTimeList() {

        timeScheduleModels = new ArrayList<>();

        for (int i = 0; i < abc.length; i++) {
            map.put(abc[i].split("-")[0].toString() + "-" + abc[i].split("-")[1].toString(), false);
            timeScheduleModels.add(new TimeScheduleModel(abc[i].split("-")[0].toString(), abc[i].split("-")[1].toString()));
            map.put(abc[i].split("-")[0].toString() + "-" + abc[i].split("-")[1].toString(), false);
        }
        System.out.println(timeScheduleModels);
        return timeScheduleModels;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_time_availability);
        ButterKnife.bind(this);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);
        Calendar startDate1 = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
//        startDate.add(Calendar.DATE, 0);
        startDate1.add(Calendar.DATE, -1);

        prevDate=startDate1.get(Calendar.YEAR)+"-"+startDate1.get(Calendar.MONTH)+"-"+startDate1.get(Calendar.DATE);
        startDate.add(Calendar.DATE, 0);
        scheduleItems = new ArrayList<>();
        mapScheduleItems = new HashMap<>();
        deMapScheduleItems = new HashMap<>();
        formItemList = new ArrayList<>();
        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        
        map = new HashMap<>();
        idMap = new HashMap<>();

        timeScheduleModels = new ArrayList<>();

        get_schedule_recycler_view.setLayoutManager(new GridLayoutManager(this, 3));
        GetScheduleAdapter adapter = new GetScheduleAdapter(new ArrayList<>());
        get_schedule_recycler_view.setAdapter(adapter);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(7)
                .build();


        Date now= new Date();
        horizontalCalendar.selectDate(now,true);

        currentDate = (String) DateFormat.format("yyyy-MM-dd", startDate);

        Log.d(TAG, "Current Date " + currentDate + " dayOfTheWeek " + dayOfTheWeek);

        customAdapter = new SetTimeAvailabilityAdapter( this,getApplicationContext(), getTimeList(), map);
        gridView.setAdapter(customAdapter);
        

        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.getScheduleByDate(currentDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GetScheduleModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onSuccess(GetScheduleModel getScheduleModel) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        Log.e("date",currentDate);
                        map.clear();
                        customAdapter.clearCheckedList();
                        try {
                            if (getScheduleModel != null && getScheduleModel.getStatus().equalsIgnoreCase("true")) {
                                //TODO
                                Log.e("malooo",""+getScheduleModel.getData());
                                filterMapWith(getScheduleModel.getData());
                            }
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        Log.e(TAG, throwable.getMessage());
                    }
                });


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {

                currentDate = (String) DateFormat.format("yyyy-MM-dd", date);

                if(currentDate==prevDate)
                {
                    return;
                }
                Log.e(TAG, "Current date " + currentDate);
                CommonUtils.showProgressDialog(getBaseContext());
                apiManager.getScheduleByDate(currentDate)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<GetScheduleModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }
                            @Override
                            public void onSuccess(GetScheduleModel getScheduleModel) {
                                map.clear();
                                customAdapter.clearCheckedList();
                                try {
                                    if (getScheduleModel != null && getScheduleModel.getStatus().equalsIgnoreCase("true")) { // TODO
                                        Log.e("mapppp",""+getScheduleModel.getData());
                                        filterMapWith(getScheduleModel.getData());
                                    }
                                } catch (Exception e) {
                                }
                                CommonUtils.disMissProgressDialog(getBaseContext());
                            }
                            @Override
                            public void onError(Throwable throwable) {
                                CommonUtils.disMissProgressDialog(getBaseContext());
                            }
                        });
            }
        });
        customAdapter.setOnChangeTimeAvailabilitiesListener(this);
        
        
    }

    private void filterMapWith(List<DataItem> data) {

        for (DataItem item : data) {
            try {
                String fromHour = item.getFrom().split(" ")[1].split(":")[0];
                String fromMinute = item.getFrom().split(" ")[1].split(":")[1];

                String toHour = item.getTo().split(" ")[1].split(":")[0]; // 2019-11-01 10:00:00
                String toMinute = item.getTo().split(" ")[1].split(":")[1]; // 2019-11-01 10:00:00
                String fromTimeToTime = fromHour + ":" + fromMinute + "-" + toHour + ":" + toMinute;
                Log.i(TAG, fromTimeToTime + " " + true);
                map.put(fromTimeToTime, true);
                idMap.put(fromTimeToTime, item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    int itemPos;
    String itemFrom, itemTo;
    @Override
    public void OnAddPriceClick(int pos, String from, String to, String price) {
        Log.e("dialog","diiiiiii");
        itemFrom = from;itemPos =pos;itemTo=to;
        Bundle bundle = new Bundle(  );
        bundle.putString( "TIME_KEY",from+"-"+to );
        PriceDialogue dialogue = PriceDialogue.newInstance( bundle );
        dialogue.show( getSupportFragmentManager(),"Price to Time" );

    }

    @Override
    public void applyText(String time, String price) {
        data[0] =  time;
        data[1] =  price;
        onSetTimeAvailability( itemPos,itemFrom, itemTo );
       Toast toast= Toast.makeText( this, time+" time and $"+price+" have been updated", Toast.LENGTH_SHORT );
        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(SetTimeAvailabilityActivity.this, android.R.color.holo_red_dark));
        toast.show();
        String fromTimeToTime = data[0];
        Log.i(TAG, fromTimeToTime + " " + true);
        map.put(fromTimeToTime, true);
    }



    @Override
    public void onSetTimeAvailability(int pos, String from, String to) {
        
        if (data[1] != null){
            try {
                String address = GetAddressUitls.getCompleteAddressString(SetTimeAvailabilityActivity.this,
                                                                          Double.valueOf(AppPreferences.newInstance().getCurrentLocationLat(this)),
                                                                          Double.valueOf(AppPreferences.newInstance().getCurrentLocationLong(this)));
                String active = AppPreferences.newInstance().getStatus(this);
                mapScheduleItems.put(from + "-" + to, new ScheduleItem(address + "", data[1] , ""
                                                                                               + currentDate + " " + from, "" + currentDate + " " + to, active));
                
                
                
                
                try {

                }catch (Exception e) {
                    e.printStackTrace();
                }
            }catch (Exception e){

            } 
        }
        
    }

    @Override
    public void onRemoveTimeAvailability(int pos, String from, String to) {
        try {
            mapScheduleItems.remove(from + "-" + to);
            DataItem item = idMap.get(from + "-" + to);
            formItemList.add(from + "-" + to);
            if (item != null)
                deMapScheduleItems.put(String.valueOf(item.getId()), new DelScheduleItem(String.valueOf(item.getId())));
        }catch (Exception e){

        }

    }
}
