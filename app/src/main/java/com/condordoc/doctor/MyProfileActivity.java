package com.condordoc.doctor;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.condordoc.doctor.model.user.description.DescriptionModel;
import com.condordoc.doctor.model.user.description.DescriptionModelPost;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.viewmodel.OfficeLocation;
import com.google.android.material.textfield.TextInputLayout;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.model.user.password.ResetPasswordModel;
import com.condordoc.doctor.model.user.profile.Data;
import com.condordoc.doctor.model.user.profile.MyProfileModel;
import com.condordoc.doctor.model.user.profile.UpdateProfileModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ImagePickerDialogUitls;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

import static com.condordoc.doctor.utils.ValidateInputs.isValidPassword;
public class MyProfileActivity extends BaseActivity implements ImagePickerCallback {

    private static final String TAG = MyProfileActivity.class.getSimpleName();
    ApiManager apiManager;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.spinner1)
    Spinner languagesp;
    @BindView(R.id.spinner2)
    Spinner currency_Spinner;

    @Nullable
    @OnClick(R.id.backbutton)
    void onback() {
        finish();
    }

    @BindView(R.id.dp)
    CircleImageView circleImageView;
    @BindView(R.id.dpcam)
    FrameLayout dbCam;
    @BindView(R.id.language)
    TextView language;
//    @BindView(R.id.currency)
//    TextView currency;
    @BindView(R.id.dob)
    EditText des;

    @BindView(R.id.update)
    TextView update;

    @OnClick(R.id.pastePin)

    void onEditText()
    {
        des.setEnabled(true);
    }

    @OnClick(R.id.ok)

    void onuploadDescri()
    {
      Log.e("desss",des.getText().toString());
        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance( MyProfileActivity.this );
        progressDialogUtils.showDialog( "Processing..." );
        apiManager.setDescription(des.getText().toString())
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<DescriptionModelPost>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(DescriptionModelPost s) {
                        progressDialogUtils.hideDialog();
                        Toast toast=Toast.makeText( MyProfileActivity.this, s.getMessage(), Toast.LENGTH_SHORT );
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            progressDialogUtils.hideDialog();
                            Log.e("error",e.getMessage());
                            Toast toast=Toast.makeText( MyProfileActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                            toast.show();

                        } catch (Exception ex) {
                        }
                    }
                } );
    }




    @OnClick(R.id.update)
    void updateProfile() {
        if (TextUtils.isEmpty( AppPreferences.newInstance().getProfilePath( this ) )) {
           Toast toast= Toast.makeText( this, "Please set profile image.", Toast.LENGTH_SHORT );
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
            toast.show();
            return;
        }
        update.setVisibility( View.GONE );
        firstname.setEnabled( false );
        lastname.setEnabled( false );
        email.setEnabled( false );
        mobile.setEnabled( false );
        File profileImageChanged = new File( AppPreferences.newInstance().getProfilePath( this ) );
        MultipartBody.Part filePart = null;
        //            filePart = MultipartBody.Part.createFormData("image", URLEncoder.encode(profileImageChanged.getName(), "utf-8"),
        if(profileImageChanged!=null) {
            filePart = MultipartBody.Part.createFormData("image", profileImageChanged.getName(),
                    RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
        }
        RequestBody firstnameValue = RequestBody.create( MediaType.parse( "text/plain" ), firstname.getText().toString().trim() );
        RequestBody lastnameValue = RequestBody.create( MediaType.parse( "text/plain" ), lastname.getText().toString().trim() );
//        RequestBody emailValue = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString().trim());
//        RequestBody passwordValue = RequestBody.create(MediaType.parse("text/plain"), "123456");
        RequestBody mobileValue = RequestBody.create( MediaType.parse( "text/plain" ), mobile.getText().toString().trim() );
        RequestBody user_typeValue = RequestBody.create( MediaType.parse( "text/plain" ), "Doctor" );
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put( "first_name", firstnameValue );
        map.put( "last_name", lastnameValue );
//        map.put("email", emailValue);
//        map.put("password", passwordValue);
        map.put( "mobile", mobileValue );
        map.put( "user_type", user_typeValue );
//        showProgress();
        CommonUtils.showProgressDialog( getBaseContext() );
        apiManager.updateProfile( map, filePart )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<UpdateProfileModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(UpdateProfileModel updateProfileModel) {
//                        hideProgress();
                        try {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            Toast toast=Toast.makeText( MyProfileActivity.this, "" + updateProfileModel.getMessage(), Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                            dbCam.setEnabled( false );
                            update.setVisibility( View.GONE );
                            firstname.setEnabled( false );
                            lastname.setEnabled( false );
                            email.setEnabled( false );
                            mobile.setEnabled( false );

                        } catch (Exception e) {
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
//                        hideProgress();
                        try {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            e.printStackTrace();
                        } catch (Exception ex) {
                        }
//                        progressDialogUtils.hideDialog();
//                        e.printStackTrace();
                    }
                } );
    }

    @BindView(R.id.input_firstname)
    EditText firstname;
    @BindView(R.id.input_lastname)
    EditText lastname;
    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_mobile)
    EditText mobile;

    @Nullable
    @OnClick(R.id.dpcam)
    void opencamera() {
//
        ImagePickerDialogUitls.newInstance( this ).showImagePicker( new ImagePickerDialogUitls.ImagePickerOrGalleryImageListener() {
            @Override
            public void onOpenCamera(Dialog dialog, String message) {
                takePicture();
            }

            @Override
            public void onImageGallery(Dialog dialog, String message) {
                pickImageSingle();
            }

            @Override
            public void onCancel() {
            }
        } );
    }

    @Nullable
    @OnClick(R.id.more)
    void onmore() {
        String[] colors = {"Edit Profile", "Change Password"};
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( "Choose one" );
        builder.setItems( colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                if (which == 0) {
                    update.setVisibility( View.VISIBLE );
                    firstname.setEnabled( true );
                    lastname.setEnabled( true );
//                    email.setEnabled(true);
                    mobile.setEnabled( true );
                    update.setEnabled( true );
//                    currency.setEnabled(false);
                    language.setEnabled( false );
                    //first option clicked, do this...
                } else if (which == 1) {
                    showchangepassworddialog();
                    //second option clicked, do this...
                } else {
                    //theres an error in what was selected
                }
            }
        } );
        builder.show();
    }

    // TODO
    private void showchangepassworddialog() {
        final Dialog dialog = new Dialog( this );
        // Include dialog.xml file
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        Window window2 = dialog.getWindow();
        dialog.setCancelable( true );
        window2.setGravity( Gravity.CENTER );
        window2.setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.setContentView( R.layout.dialogforchangepassword );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById( R.id.cancel );
        TextInputLayout input_layout_refralcode = (TextInputLayout) dialog.findViewById( R.id.input_layout_refralcode );
        TextInputLayout input_layout_cpass = (TextInputLayout) dialog.findViewById( R.id.input_layout_cpass );
        TextInputLayout input_layout_confirm = (TextInputLayout) dialog.findViewById( R.id.input_layout_confirm );
        EditText input_current_pass = (EditText) dialog.findViewById( R.id.input_referal );
        EditText input_new_pass = (EditText) dialog.findViewById( R.id.input_newpass );
        EditText input_confirm_pass = (EditText) dialog.findViewById( R.id.input_confirm );
        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance( MyProfileActivity.this );
        cancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        } );
        TextView save = (TextView) dialog.findViewById( R.id.viewongng );
        save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                boolean isValid = validateForm( input_current_pass, input_layout_refralcode, input_new_pass, input_layout_cpass, input_confirm_pass,
                                                input_layout_confirm );
                if (isValid) {
                    String oldPassword = input_current_pass.getText().toString().trim();
                    String newPassword = input_new_pass.getText().toString().trim();
                    progressDialogUtils.showDialog( "Processing..." );
                    apiManager.passwordChange( oldPassword, newPassword )
                            .subscribeOn( Schedulers.io() )
                            .observeOn( AndroidSchedulers.mainThread() )
                            .subscribe( new SingleObserver<ResetPasswordModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onSuccess(ResetPasswordModel s) {
                                    progressDialogUtils.hideDialog();
                                    Toast toast=Toast.makeText( MyProfileActivity.this, "Password Changed.", Toast.LENGTH_SHORT );
                                    toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                                    toast.show();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    try {
                                        progressDialogUtils.hideDialog();
                                        Toast toast=Toast.makeText( MyProfileActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                                        toast.show();

                                    } catch (Exception ex) {
                                    }
                                }
                            } );

                }
            }

        } );
    }

    private boolean validateForm(EditText input_current, TextInputLayout inputcurrentLayout, EditText input_new_pass,
                                 TextInputLayout inputNewPassLayout, EditText input_confirm_password, TextInputLayout inputLayout) {
        if (!isValidPassword( input_current.getText().toString().trim() )) {
            inputcurrentLayout.setError( getString( R.string.invalid_nick_name ) );
            return false;
        } else if (!isValidPassword( input_new_pass.getText().toString().trim() )) {
            inputNewPassLayout.setError( getString( R.string.invalid_phone ) );
            return false;
        } else if (!isValidPassword( input_confirm_password.getText().toString().trim() )) {
            inputLayout.setError( getString( R.string.invalid_phone ) );
            return false;
        } else if (!isValidPassword( input_new_pass.getText().toString().trim(), input_confirm_password.getText().toString().trim() )) {
            inputLayout.setError( getString( R.string.invalid_not_matach ) );
            return false;
        } else {
            return true;
        }
    }

    public void takePicture() {
        cameraPicker = new CameraImagePicker( this );
        cameraPicker.setDebugglable( true );
        cameraPicker.setCacheLocation( CacheLocation.EXTERNAL_STORAGE_APP_DIR );
        cameraPicker.setImagePickerCallback( this );
        cameraPicker.shouldGenerateMetadata( true );
        cameraPicker.shouldGenerateThumbnails( true );
        pickerPath = cameraPicker.pickImage();
    }

    public void pickImageSingle() {
        imagePicker = new ImagePicker( this );
        imagePicker.setDebugglable( true );
//        imagePicker.setFolderName("Random");
        imagePicker.setRequestId( 1234 );
        imagePicker.ensureMaxSize( 500, 500 );
        imagePicker.shouldGenerateMetadata( true );
        imagePicker.shouldGenerateThumbnails( true );
        imagePicker.setImagePickerCallback( this );
        Bundle bundle = new Bundle();
        bundle.putInt( "android.intent.extras.CAMERA_FACING", 1 );
        imagePicker.setCacheLocation( CacheLocation.EXTERNAL_STORAGE_PUBLIC_DIR );
        imagePicker.pickImage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_my_profile );
        ButterKnife.bind( this );
        if (!isNetworkConnected()) {
            Toast toast=Toast.makeText( this, getString( R.string.no_internet_connected ) + "", Toast.LENGTH_SHORT );
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
            toast.show();
            finish();
            return;
        }
        
        sharedPreferences = getSharedPreferences( "lastSelected", this.MODE_PRIVATE );
        editor = sharedPreferences.edit();


        final int selected = sharedPreferences.getInt( "selected", 0 );
        final int lang = sharedPreferences.getInt( "lang", 0 );
                
                
                
        Retrofit retrofit = DoctorApp.retrofit( AppPreferences.newInstance().getToken( this ) );
        apiManager = retrofit.create( ApiManager.class );

        List<String> currencyList = new ArrayList<>();
        currencyList.add( "$ USD" );
        currencyList.add("$ ARGENTINA PESO");
        
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>( this, android.R.layout.simple_spinner_item, currencyList );
        
        adapter1.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        currency_Spinner.setAdapter( adapter1 );
        currency_Spinner.setSelection( selected );
        
        currency_Spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                editor.putInt( "selected", position ).commit();
                
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        } );
        
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this,
                R.array.langarray,
                R.layout.profile_spinner);

        languagesp.setAdapter(adapter);

        if (AppPreferences.newInstance().getLanguage(this).equalsIgnoreCase(AppConstant.ENGLISH)) {
            languagesp.setSelection(0, true);
        } else if(AppPreferences.newInstance().getLanguage(this).equalsIgnoreCase(AppConstant.SPANISH)){
            languagesp.setSelection(1, true);
        }

        languagesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    AppPreferences.newInstance().setLanguage(MyProfileActivity.this, AppConstant.ENGLISH);
                    setDefaultLanguage("en");
                    setLanguage("en");
                } else if (position == 1) {
                    AppPreferences.newInstance().setLanguage(MyProfileActivity.this, AppConstant.SPANISH);
                    setDefaultLanguage("es");
                    setLanguage("es");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        
        findViewById( R.id.Office_Address ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( MyProfileActivity.this, OfficeLocation.class ) );
            }
        } );
//        showProgress();
        CommonUtils.showProgressDialog( getBaseContext() );
        apiManager.myProfile()
                .subscribeOn( Schedulers.newThread() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<MyProfileModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(MyProfileModel myProfileModel) {
                        try {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            Data profileModel = myProfileModel.getData();
                            Log.d( TAG, profileModel.getFirstName() );
                            if (isValid( profileModel.getFirstName() )) {
                                firstname.setText( "" + profileModel.getFirstName() );
                            }
                            if (isValid( profileModel.getLastName() )) {
                                lastname.setText( "" + profileModel.getLastName() );
                            }
                            if (isValid( profileModel.getEmail() )) {
                                email.setText( "" + profileModel.getEmail() );
                            }
                            if (isValid( profileModel.getMobile() )) {
                                mobile.setText( "" + profileModel.getMobile() );
                            }
                            firstname.setEnabled( false );
                            lastname.setEnabled( false );
                            email.setEnabled( false );
                            mobile.setEnabled( false );
//                            currency.setEnabled(false);
                            language.setEnabled( false );
                            Log.i( TAG, "" + myProfileModel.getData().getImage() );
                            Glide.with( MyProfileActivity.this )
                                    .load( APIURL.IMAGE_USER_URL + myProfileModel.getData().getImage() )
                                    .error( R.drawable.user )
                                    .into( circleImageView );
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog( getBaseContext() );
                        Toast toast=Toast.makeText( MyProfileActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                } );

        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance( MyProfileActivity.this );
        progressDialogUtils.showDialog( "Processing..." );
        apiManager.getDescription()
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<DescriptionModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(DescriptionModel s) {
                        progressDialogUtils.hideDialog();
                        if(s.getData()!=null)
                        {
                            des.setText(s.getData().getDescription());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            progressDialogUtils.hideDialog();
                            Toast toast=Toast.makeText( MyProfileActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                            toast.show();

                        } catch (Exception ex) {
                        }
                    }
                } );

    }

    boolean isValid(String checkString) {
        if (TextUtils.isEmpty( checkString )) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker( this );
                    imagePicker.setImagePickerCallback( this );
                }
                imagePicker.submit( data );
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker( this );
                    cameraPicker.setImagePickerCallback( this );
                    cameraPicker.reinitialize( pickerPath );
                }
                cameraPicker.submit( data );
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (list.size() > 0) {
            AppPreferences.newInstance().setProfilePath( this, list.get( 0 ).getThumbnailSmallPath() );
            Glide.with( this )
                    .load( list.get( 0 ).getThumbnailPath() ).into( circleImageView );
            sendImage();
        }
    }

    @Override
    public void onError(String s) {
        Toast toast=Toast.makeText( this, "" + s, Toast.LENGTH_SHORT );
        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
        toast.show();
    }








    public void sendImage()
    {
        if (TextUtils.isEmpty( AppPreferences.newInstance().getProfilePath( this ) )) {
           Toast toast= Toast.makeText( this, "Please set profile image.", Toast.LENGTH_SHORT );
            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
            toast.show();
            return;
        }
        update.setVisibility( View.GONE );
        firstname.setEnabled( false );
        lastname.setEnabled( false );
        email.setEnabled( false );
        mobile.setEnabled( false );
        File profileImageChanged = new File( AppPreferences.newInstance().getProfilePath( this ) );
        MultipartBody.Part filePart = null;
        //            filePart = MultipartBody.Part.createFormData("image", URLEncoder.encode(profileImageChanged.getName(), "utf-8"),
        if(profileImageChanged!=null) {
            filePart = MultipartBody.Part.createFormData("image", profileImageChanged.getName(),
                    RequestBody.create(MediaType.parse("image/*"), profileImageChanged));
        }
        RequestBody firstnameValue = RequestBody.create( MediaType.parse( "text/plain" ), firstname.getText().toString().trim() );
        RequestBody lastnameValue = RequestBody.create( MediaType.parse( "text/plain" ), lastname.getText().toString().trim() );
//        RequestBody emailValue = RequestBody.create(MediaType.parse("text/plain"), email.getText().toString().trim());
//        RequestBody passwordValue = RequestBody.create(MediaType.parse("text/plain"), "123456");
        RequestBody mobileValue = RequestBody.create( MediaType.parse( "text/plain" ), mobile.getText().toString().trim() );
        RequestBody user_typeValue = RequestBody.create( MediaType.parse( "text/plain" ), "Doctor" );
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put( "first_name", firstnameValue );
        map.put( "last_name", lastnameValue );
//        map.put("email", emailValue);
//        map.put("password", passwordValue);
        map.put( "mobile", mobileValue );
        map.put( "user_type", user_typeValue );
//        showProgress();
        CommonUtils.showProgressDialog( getBaseContext() );
        apiManager.updateProfile( map, filePart )
                .subscribeOn( Schedulers.io() )
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe( new SingleObserver<UpdateProfileModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(UpdateProfileModel updateProfileModel) {
//                        hideProgress();
                        try {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            Toast toast=Toast.makeText( MyProfileActivity.this, "" + updateProfileModel.getMessage(), Toast.LENGTH_SHORT );
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(MyProfileActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                            dbCam.setEnabled( false );
                            update.setVisibility( View.GONE );
                            firstname.setEnabled( false );
                            lastname.setEnabled( false );
                            email.setEnabled( false );
                            mobile.setEnabled( false );

                        } catch (Exception e) {
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
//                        hideProgress();
                        try {
                            CommonUtils.disMissProgressDialog( getBaseContext() );
                            e.printStackTrace();
                        } catch (Exception ex) {
                        }
//                        progressDialogUtils.hideDialog();
//                        e.printStackTrace();
                    }
                } );
    }
}
