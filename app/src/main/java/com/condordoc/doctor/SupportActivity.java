package com.condordoc.doctor;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.API.APIURL;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SupportActivity extends LocalizationActivity {

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    @Nullable
    @OnClick(R.id.aboutus)
    void onaboutus() {
        Intent intent = new Intent(SupportActivity.this,AboutUsActivity.class);
        intent.putExtra(APIURL.Activity,"Aboutus");
        startActivity(intent);
    }

    @Nullable
    @OnClick(R.id.pp)
    void onpp() {
        Intent intent = new Intent(SupportActivity.this,AboutUsActivity.class);
        intent.putExtra(APIURL.Activity,"PP");
        startActivity(intent);
    }

    @Nullable
    @OnClick(R.id.tnc)
    void ontnc() {
        Intent intent = new Intent(SupportActivity.this,AboutUsActivity.class);
        intent.putExtra(APIURL.Activity,"Tnc");
        startActivity(intent);
    }

    @Nullable
    @OnClick(R.id.contactus)
    void oncontactus() {
        Intent intent = new Intent(SupportActivity.this,ContactUsActivity.class);
        startActivity(intent);
    }

    @Nullable
    @OnClick(R.id.faq)
    void onfaq() {
        Intent intent = new Intent(SupportActivity.this,FaqActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        ButterKnife.bind(this);
    }
}
