package com.condordoc.doctor;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.fcm.FCMHandler;
import com.condordoc.doctor.model.user.sigin.sigin.SignInModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.Locale;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
public class IntroActivity extends LocalizationActivity {
    private static final String TAG = IntroActivity.class.getSimpleName();
    @BindView(R.id.spinner)
    Spinner currencysp;
    @BindView(R.id.spinner1)
    Spinner languagesp;

    public static IntroActivity introActivity;

    @Nullable
    @OnClick(R.id.login)
    void onLogin() {
        Intent in = new Intent(this, LoginActivity.class);
        startActivity(in);
//        finish();
    }

    @Nullable
    @OnClick(R.id.register)
    void onSignUp() {
        Intent in = new Intent(this, SignUpActivity.class);
        startActivity(in);
//        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount( this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(AppConstant.PERSON_NAME, personName);
            intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
            intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
            intent.putExtra(AppConstant.PERSON_EMAIL, personEmail);
            intent.putExtra(AppConstant.PERSON_ID, personId);
            intent.putExtra(AppConstant.PERSON_URL, personPhoto.toString());
            startActivity(intent);
            finish();


            String idToken = account.getIdToken();
            Log.d(TAG, "" + idToken);
            apiManager.googleLogin(idToken, "google")
                    .subscribeOn( Schedulers.io())
                    .observeOn( AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<SignInModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(SignInModel signInModel) {
                            try{

                                if (signInModel != null) {
                                    if (signInModel.getStatus().equalsIgnoreCase("true")) {
                                        String personName = account.getDisplayName();
                                        String personGivenName = account.getGivenName();
                                        String personFamilyName = account.getFamilyName();
                                        String personEmail = account.getEmail();
                                        String personId = account.getId();
                                        Uri personPhoto = account.getPhotoUrl();
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.putExtra(AppConstant.PERSON_NAME, personName);
                                        intent.putExtra(AppConstant.PERSON_GIVEN_NAME, personGivenName);
                                        intent.putExtra(AppConstant.PERSON_FAMILY, personFamilyName);
                                        intent.putExtra(AppConstant.PERSON_EMAIL, personEmail);
                                        intent.putExtra(AppConstant.PERSON_ID, personId);
                                        intent.putExtra(AppConstant.PERSON_URL, personPhoto.toString());

                                        FCMHandler fcmHandler = new FCMHandler();
                                        fcmHandler.enableFCM();
                                        Log.d(TAG, signInModel.getData().getOriginal().getAccessToken());
                                        AppPreferences.newInstance().setToken(getApplicationContext(), signInModel.getData().getOriginal().getAccessToken());
                                        Toast toast=Toast.makeText( getApplicationContext(), "Successfully logged in.", Toast.LENGTH_SHORT);
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(IntroActivity.this, android.R.color.holo_red_dark));
                                        toast.show();
                                        startActivity(intent);
                                        try {
                                            IntroActivity.introActivity.finish();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                        finish();
                                    } else {
                                       Toast toast= Toast.makeText(getApplicationContext(), "" + signInModel.getError(), Toast.LENGTH_SHORT);
                                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(IntroActivity.this, android.R.color.holo_red_dark));
                                        toast.show();
                                    }
                                }
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast toast=Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(IntroActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                            Log.e( TAG, "onError: " + e.getMessage()  );
                        }
                    });
        }
    }


    ApiManager apiManager;

    ArrayAdapter currencyAdapter;
    ArrayAdapter languageAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        introActivity =this;
        languageAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.langarray,
                R.layout.my_spinner);
        currencyAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.currencyarray,
                R.layout.my_spinner
        );
        apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);

        /*apiManager.getCurrency()
                .flatMap(new Function<CurrencyModel, SingleSource<String[]>>() {
                    @Override
                    public SingleSource<String[]> apply(CurrencyModel currencyModel) throws Exception {
                        if (currencyModel != null && currencyModel.getData() != null) {
                            String[] currencys = new String[currencyModel.getData().size()];
//                        DataItem[] dataItems = currencyModel.getData().toArray(new DataItem[0]);
                            for (int i = 0; i < currencyModel.getData().size(); i++) {
                                currencys[i] = currencyModel.getData().get(i).getCountry();
                            }
                            return Single.just(currencys);
                        }

                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String[]>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(String[] strings) {
                        if (strings != null && strings.length != 0) {
                            currencyAdapter = new ArrayAdapter<>(IntroActivity.this, R.layout.my_spinner, strings);
                            currencyAdapter.notifyDataSetChanged();

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
        apiManager.getLanguage()
                .flatMap(new Function<LanguageModel, SingleSource<String[]>>() {
                    @Override
                    public SingleSource<String[]> apply(LanguageModel currencyModel) throws Exception {
                        if (currencyModel != null && currencyModel.getData() != null) {
                            String[] languageName = new String[currencyModel.getData().size()];
//                        DataItem[] dataItems = currencyModel.getData().toArray(new DataItem[0]);
                            for (int i = 0; i < currencyModel.getData().size(); i++) {
                                languageName[i] = currencyModel.getData().get(i).getStringValue();
                            }
                            return Single.just(languageName);
                        }

                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String[]>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(String[] strings) {
                        *//*if (strings != null && strings.length != 0) {
                            languageAdapter = new ArrayAdapter<>(IntroActivity.this, R.layout.my_spinner, strings);
                            languageAdapter.notifyDataSetChanged();
                            languagesp.setAdapter(languageAdapter);
                        }*//*
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });*/


        currencysp.setAdapter(currencyAdapter);
//        AppPreferences.newInstance().setCurrency(IntroActivity.this, currencysp.getSelectedItem().toString());
        currencysp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    AppPreferences.newInstance().setCurrency(IntroActivity.this, AppConstant.USD);
                } else if (position == 1) {
                    AppPreferences.newInstance().setCurrency(IntroActivity.this, AppConstant.AED);
                } else {
                    AppPreferences.newInstance().setCurrency(IntroActivity.this, AppConstant.USD);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (TextUtils.isEmpty(AppPreferences.newInstance().getLanguage(this))){
            AppPreferences.newInstance().setLanguage(this,AppConstant.ENGLISH);
        }

        Locale current = getResources().getConfiguration().locale;
//        Log.i("locale", Currency.getInstance(current).getCurrencyCode());
        languagesp.setAdapter(languageAdapter);

//        AppPreferences.newInstance().setLanguage(IntroActivity.this, languagesp.getSelectedItem().toString());
        languagesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    AppPreferences.newInstance().setLanguage(IntroActivity.this, AppConstant.ENGLISH);
                    setDefaultLanguage("en");
                    setLanguage("en");
                } else if (position == 1) {
                    AppPreferences.newInstance().setLanguage(IntroActivity.this, AppConstant.SPANISH);
                    setDefaultLanguage("es");
                    setLanguage("es");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (TextUtils.isEmpty(AppPreferences.newInstance().getCurrency(this))){
            AppPreferences.newInstance().setCurrency(this,AppConstant.USD);
        }
//        Log.i("Language", Locale.getDefault().getDisplayLanguage());
        Log.i("Currency", AppPreferences.newInstance().getCurrency(IntroActivity.this));
        Log.i("getLanguage ", AppPreferences.newInstance().getLanguage(IntroActivity.this));
    }
}
