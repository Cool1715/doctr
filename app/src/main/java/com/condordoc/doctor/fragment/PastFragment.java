package com.condordoc.doctor.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.DoctorApp;
import com.condordoc.doctor.R;
import com.condordoc.doctor.adapter.OnActionFinishedListener;
import com.condordoc.doctor.adapter.PastTopDownAdapter;
import com.condordoc.doctor.model.job.CompleteJobModel;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class PastFragment extends Fragment {

    private static final String TAG = PastFragment.class.getSimpleName();
    private PastTopDownAdapter topDownAdapter;

    @BindView(R.id.main_recycler_view)
    RecyclerView PastRecyclerview;

    @BindView(R.id.fromdate)
    TextView fromdate;
    @BindView(R.id.todate)
    TextView todate;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private ApiManager apiManager;
    private ProgressDialogUtils progressDialogUtils;
    private List<DataItem> dataItems;


    String fromDate, toDate;

    @Nullable
    @OnClick(R.id.fromll)
    void onfrom() {
        callcalendar(fromdate);
    }

    @Nullable
    @OnClick(R.id.toll)
    void onto() {
        toCallcalendar(todate);
    }

    private void callcalendar(final TextView givendatetextview) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                givendatetextview.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                if (dayOfMonth > 9) {
                    fromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                }else {
                    fromDate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;

                }
            }
        }, mYear, mMonth, mDay);
//        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpd.show();
    }


    private void toCallcalendar(final TextView givendatetextview) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                givendatetextview.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

//                toDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                if (dayOfMonth > 9 ) {
                    toDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                } else
                    toDate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;

                getfilterDate();

            }
        }, mYear, mMonth, mDay);
//        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpd.show();
    }

    static SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");
//    static SimpleDateFormat dfDateBackend = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    static SimpleDateFormat dfDateBackend = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean CheckDates(String modelDate, String userStart, String userEnd) { // "2012-07-12", "2012-06-12
        boolean b = false;
        try {

            Date modelDateFormat =dfDateBackend.parse(modelDate.split(" ")[0]);
            Date userStartFormat = dfDate.parse(userStart);
            Date userEndFormat = dfDate.parse(userEnd);

            modelDate = modelDate.split(" ")[0];

//            if(modelDateFormat.after(dfDate.parse(userStart))
//                    || modelDateFormat.before(dfDate.parse(userEnd))
//
//                /*
//                    || modelDateFormat.compareTo(dfDate.parse(userStart)) == 0
//                    || modelDateFormat.compareTo(dfDate.parse(userEnd)) == 0*/) {
//                b = true;
//            }

           /* if ((userStartFormat.after(modelDateFormat)
                    ||  userEndFormat.before(modelDateFormat) && userStartFormat.after(modelDateFormat)
             || userEndFormat.before(modelDateFormat))){
                b = true;

                Log.d(TAG, "model Date : " + modelDate + " start date " + userStart + " end date " + userEnd);
            }*/

            if (userStartFormat.compareTo(modelDateFormat) > 0) {
                Log.i("app", "Date1 is after Date2");
                                b = true;

            } else if (userEndFormat.compareTo(modelDateFormat) < 0) {
                Log.i("app", "Date1 is before Date2");
                                b = true;

            } else if (userEndFormat.compareTo(modelDateFormat) == 0 || userStartFormat.compareTo(modelDateFormat) == 0) {
                Log.i("app", "Date1 is equal to Date2");
                b = true;

            } else {
                b = false;

            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "CheckDates: ", e);
        }catch (Exception ex){
            ex.printStackTrace();

            Log.e(TAG, "CheckDates: ", ex);
        }
        return b;
    }

    private void getfilterDate() {

        apiManager.getJobs().map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            if (jobGetModel.getStatus().equalsIgnoreCase("true")) {




                return jobGetModel.getData();
//                return newFilterItem;

//                return jobGetModel.getData();
            }
            return new ArrayList<>();
        }).flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> (dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR
                        || dataItem.getStatus() == AppConstant.CANCEL_DOCTOR
                        || dataItem.getStatus() == AppConstant.DENIED_DOCTOR))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<DataItem> dataItems) {
                try {
                    if (dataItems != null && dataItems.size() > 0) {
                        PastFragment.this.dataItems = dataItems;

                        List<DataItem> newFilterItem = new ArrayList<>();

                        for (DataItem item : dataItems) {
                            if (CheckDates(item.getUpdatedAt(),fromDate, toDate)){
                                newFilterItem.add(item);
                            }
                        }

                        topDownAdapter.updateList(newFilterItem);
                    } else {
                        if (dataItems != null) {
                            topDownAdapter.clear();
                        }
                    }
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
        apiManager = retrofit.create(ApiManager.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.activity_pastfragment, container, false);
        ButterKnife.bind(this, v);

        PastRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        topDownAdapter = new PastTopDownAdapter(getContext(), new ArrayList<>(), new OnActionFinishedListener() {
            @Override
            public void onCompletedJob(View view, DataItem dataItem, int pos) {
                apiManager.setJobCompletedDoctor(String.valueOf(dataItem.getId())).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<CompleteJobModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(CompleteJobModel completeJobModel) {
                        topDownAdapter.onJobFinished(getActivity(), dataItem);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
        PastRecyclerview.setVisibility(View.VISIBLE);
        PastRecyclerview.setAdapter(topDownAdapter);

        return v;
    }
/*


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
//            Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
//            apiManager = retrofit.create(ApiManager.class);
            if (apiManager != null) {
                progressDialogUtils = ProgressDialogUtils.newInstance(getActivity());
                apiManager.getJobs()
                        .subscribeOn(Schedulers.io())
                        .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                            if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                                return jobGetModel.getData();
                            return new ArrayList<>();
                        })
                        .flatMapIterable(dataItems -> dataItems)
                        .filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR
                                || dataItem.getStatus() == AppConstant.CANCEL_DOCTOR
                                || dataItem.getStatus() == AppConstant.DENIED_DOCTOR
                        )
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<DataItem>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(List<DataItem> dataItems) {
                                try {
                                    topDownAdapter.updateList(dataItems);
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                                }catch (Exception e){

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }

*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiManager.getJobs().subscribeOn(Schedulers.io()).map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            if (jobGetModel.getStatus().equalsIgnoreCase("true")) return jobGetModel.getData();
            return new ArrayList<>();
        }).flatMapIterable(dataItems -> dataItems).filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR || dataItem.getStatus() == AppConstant.CANCEL_DOCTOR || dataItem.getStatus() == AppConstant.DENIED_DOCTOR).toList().observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<List<DataItem>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<DataItem> dataItems) {
                try {
                    if (dataItems != null && dataItems.size() > 0) {
                        PastFragment.this.dataItems = dataItems;
                        topDownAdapter.updateList(dataItems);
                    }
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static PastFragment newInstance(String text) {

        PastFragment f = new PastFragment();

        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onPause() {
        super.onPause();
        // stop auto scroll when onPause

    }

    @Override
    public void onResume() {
        super.onResume();
    }

}