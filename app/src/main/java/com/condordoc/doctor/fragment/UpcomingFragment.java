package com.condordoc.doctor.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.DoctorApp;
import com.condordoc.doctor.R;
import com.condordoc.doctor.adapter.OnJobTakeListener;
import com.condordoc.doctor.adapter.UpcomingTopDownAdapter;
import com.condordoc.doctor.model.job.AcceptJobModel;
import com.condordoc.doctor.model.job.CancelJobModel;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class UpcomingFragment extends Fragment {


    private static final String TAG = UpcomingFragment.class.getSimpleName();
    private UpcomingTopDownAdapter topDownAdapter;

    @BindView(R.id.main_recycler_view)
    RecyclerView UpcomingRecyclerview;
    OnJobTakeListener onJobTakeListener;

    private ApiManager apiManager;
    private ProgressDialogUtils progressDialogUtils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
        apiManager = retrofit.create(ApiManager.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.activity_upcomingfragment, container, false);
        ButterKnife.bind(this, v);

        UpcomingRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        UpcomingRecyclerview.setVisibility(View.VISIBLE);
        UpcomingRecyclerview.setAdapter(topDownAdapter);


        onJobTakeListener = new OnJobTakeListener() {
            @Override
            public void startJob(View view, DataItem dataItem, int pos) {
                /*apiManager.setJobStarted(String.valueOf(dataItem.getId()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<StartJobModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(StartJobModel acceptJobModel) {
                                try {
                                    topDownAdapter.startJob(getActivity(), dataItem, pos);

                                } catch (Exception e) {

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });*/


                // Don't use api

                topDownAdapter.startJob(getActivity(), dataItem, pos,apiManager);

            }

            @Override
            public void acceptJob(View view, DataItem dataItem, int pos) {
                CommonUtils.showProgressDialog(getContext());
                apiManager.setjobAcceptedDoctor(String.valueOf(dataItem.getId()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<AcceptJobModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }
                            @Override
                            public void onSuccess(AcceptJobModel acceptJobModel) {
                                CommonUtils.disMissProgressDialog(getContext());
                                try {
                                    topDownAdapter.acceptJob(getActivity(), dataItem, pos);
                                } catch (Exception e) {
                                }
                            }
                            @Override
                            public void onError(Throwable e) {
                                CommonUtils.disMissProgressDialog(getContext());
                                Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            @Override
            public void cancelJob(View view, DataItem dataItem, int pos) {
                CommonUtils.showProgressDialog(getActivity());
                apiManager.setJobAcceptedButCancel(String.valueOf(dataItem.getId()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<CancelJobModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(CancelJobModel cancelJobModel) {
                                CommonUtils.disMissProgressDialog(getActivity());
                                try {
                                    topDownAdapter.cancelJob(dataItem, pos);
                                } catch (Exception e) {

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                CommonUtils.disMissProgressDialog(getActivity());

                            }
                        });
            }
        };
        topDownAdapter = new UpcomingTopDownAdapter(new ArrayList<>(), onJobTakeListener);
        return v;
    }

    /*

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
//            Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
//            apiManager = retrofit.create(ApiManager.class);
            if (apiManager != null) {
                progressDialogUtils = ProgressDialogUtils.newInstance(getActivity());
                apiManager.getJobs()
                        .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                            if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                                return jobGetModel.getData();
                            return new ArrayList<>();
                        })
                        .flatMapIterable(dataItems -> dataItems)
                        .filter(dataItem -> {
                            Log.i(TAG, String.valueOf(dataItem.getDoctorId()));
                            return dataItem.getStatus() == AppConstant.ACCEPT_DOCTOR
                                    *//* || dataItem.getStatus() == AppConstant.START_DOCTOR*//*;
                        })
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<DataItem>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(List<DataItem> dataItems) {
                                try {
                                    Log.d(TAG, "" + dataItems.toString());
                                    topDownAdapter = new UpcomingTopDownAdapter(dataItems, onJobTakeListener);
                                    UpcomingRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    UpcomingRecyclerview.setVisibility(View.VISIBLE);
                                    UpcomingRecyclerview.setAdapter(topDownAdapter);
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });
            }
        }
    }
*/
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //
        apiManager.getJobs()
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> {
                    Log.i(TAG, String.valueOf(dataItem.getDoctorId()));
                    return dataItem.getStatus() == AppConstant.ACCEPT_DOCTOR
                             || (dataItem.getDoctorId() == AppPreferences.newInstance().getPatientId(getActivity()));
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        try {
                            Log.d(TAG, "" + dataItems.toString());
                            topDownAdapter = new UpcomingTopDownAdapter(dataItems, onJobTakeListener);
                            UpcomingRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                            UpcomingRecyclerview.setVisibility(View.VISIBLE);
                            UpcomingRecyclerview.setAdapter(topDownAdapter);
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    public static UpcomingFragment newInstance(String text) {

        UpcomingFragment f = new UpcomingFragment();

        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onPause() {
        super.onPause();
        // stop auto scroll when onPause

    }

    @Override
    public void onResume() {
        super.onResume();
    }


}