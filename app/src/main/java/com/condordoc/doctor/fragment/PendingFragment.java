package com.condordoc.doctor.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.condordoc.doctor.DoctorApp;
import com.condordoc.doctor.R;
import com.condordoc.doctor.adapter.OnJobActionListener;
import com.condordoc.doctor.adapter.PendingJobAdapter;
import com.condordoc.doctor.model.job.AcceptJobModel;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.DeniedJobModel;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class PendingFragment extends Fragment {


    private static final String TAG = PendingFragment.class.getSimpleName();
    private PendingJobAdapter topDownAdapter;

    @BindView(R.id.main_recycler_view)
    RecyclerView UpcomingRecyclerview;
    ApiManager apiManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
        apiManager = retrofit.create(ApiManager.class);

    }

    //  int images[]={R.drawable.image1,R.drawable.image2,R.drawable.image3,R.drawable.image4,R.drawable.image1,R.drawable.image2,R.drawable.image3,R.drawable.image4};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.activity_upcomingfragment, container, false);
        ButterKnife.bind(this, v);


        UpcomingRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        topDownAdapter = new PendingJobAdapter(new ArrayList<>(), new OnJobActionListener(){
            @Override
            public void jobAcceptByDoctor(View view, int pos, DataItem dataItem) {
                CommonUtils.showProgressDialog(getContext());
                apiManager.setjobAcceptedDoctor(String.valueOf(dataItem.getId()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<AcceptJobModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onSuccess(AcceptJobModel acceptJobModel) {
                                CommonUtils.disMissProgressDialog(getContext());
                                try {
//                                    dataItem.setStatus(AppConstant.ACCEPT_DOCTOR);
                                    topDownAdapter.acceptJob(getActivity(), dataItem, pos);
                                    Toast.makeText(getActivity(), "Accepted", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                try {
                                    CommonUtils.disMissProgressDialog(getContext());
                                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                } catch (Exception ex) {

                                }
                            }
                        });
            }

            @Override
            public void jobDeclineByDoctor(View view, int pos, DataItem dataItem) {
                CommonUtils.showProgressDialog(getContext());
                Log.e("dataid",""+dataItem.getId());
                apiManager.setJobDeniedDoctor(String.valueOf(dataItem.getId()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<DeniedJobModel>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }
                            @Override
                            public void onSuccess(DeniedJobModel s) {
                                try {
                                    CommonUtils.disMissProgressDialog(getContext());
                                    topDownAdapter.declineJob(dataItem, pos);
                                } catch (Exception e) {

                                }
                            }
                            @Override
                            public void onError(Throwable e) {
                                try {
                                    CommonUtils.disMissProgressDialog(getContext());
                                    Log.e("errrr",e.getMessage());
                                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                } catch (Exception ex) {
                                }
                            }
                        });
            }
        });
        UpcomingRecyclerview.setVisibility(View.VISIBLE);
        UpcomingRecyclerview.setAdapter(topDownAdapter);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        CommonUtils.showProgressDialog(getContext());
        apiManager.getJobs()
                .subscribeOn(Schedulers.io())
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> {
                    return dataItem.getStatus() == AppConstant.PENDING_JOB;
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d){

                    }
                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        try {
                            CommonUtils.disMissProgressDialog(getContext());
                            topDownAdapter.updateList(dataItems);
//                        Log.d(TAG, "" + dataItems.toString());
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());
                    }

                    @Override
                    public void onError(Throwable e){
                        try {
                            CommonUtils.disMissProgressDialog(getContext());
                        }catch (Exception ex) {

                        }
                    }
                });
    }

    public static PendingFragment newInstance(String text) {

        PendingFragment f = new PendingFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);
        return f;
    }

    /*

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
//            Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(getActivity()));
//            apiManager = retrofit.create(ApiManager.class);
            if (apiManager != null) {
                progressDialogUtils = ProgressDialogUtils.newInstance(getActivity());
                apiManager.getJobs()
                        .subscribeOn(Schedulers.io())
                        .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                            if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                                return jobGetModel.getData();
                            return new ArrayList<>();
                        })
                        .flatMapIterable(dataItems -> dataItems)
                        .filter(dataItem -> {
                            return dataItem.getStatus() == AppConstant.PENDING_JOB;
                        })
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<DataItem>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(List<DataItem> dataItems) {
                                try {
                                    progressDialogUtils.hideDialog();
                                    topDownAdapter.updateList(dataItems);
//                        Log.d(TAG, "" + dataItems.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                        pendingJobs.setText("" + dataItems.size());
//                        upcomingJobs.setText("" + dataItems.size());

                            }

                            @Override
                            public void onError(Throwable e) {
                                try {
                                    progressDialogUtils.hideDialog();

                                } catch (Exception ex) {

                                }
                            }
                        });

            }
        }
    }
*/
    @Override
    public void onPause() {
        super.onPause();
        // stop auto scroll when onPause

    }

    @Override
    public void onResume() {
        super.onResume();
    }


}