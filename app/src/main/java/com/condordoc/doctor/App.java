package com.condordoc.doctor;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import net.danlew.android.joda.JodaTimeAndroid;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.utils.ProgressDialogUtils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    LocalizationApplicationDelegate localizationDelegate = new LocalizationApplicationDelegate(this);

    static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        try {
            super.attachBaseContext(localizationDelegate.attachBaseContext(base));
            MultiDex.install(this);
        }catch (Exception e){

        }
//        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localizationDelegate.onConfigurationChanged(this);
    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        JodaTimeAndroid.init(this);
    }

    public static Retrofit retrofit(final String token){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                if(token  != null){
                    Request newRequest  = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
                Request newRequest  = chain.request().newBuilder()
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        return  new Retrofit.Builder()
                .baseUrl(APIURL.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }


    public static ProgressDialogUtils getProgressDialog(){
        return ProgressDialogUtils.newInstance(context);
    }

}
