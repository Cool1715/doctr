package com.condordoc.doctor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.TransactionAdapter;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.ProgressDialogUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class TransactionHistory extends LocalizationActivity {
    private String TAG = TransactionHistory.class.getSimpleName();

    private TransactionAdapter topDownAdapter;


    @BindView(R.id.main_recycler_view)
    RecyclerView TransactionRecyclerview;

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);

        TransactionRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        topDownAdapter = new TransactionAdapter(new ArrayList<>());
        TransactionRecyclerview.setVisibility(View.VISIBLE);
        TransactionRecyclerview.setAdapter(topDownAdapter);


        ApiManager apiManager = App.retrofit(AppPreferences.newInstance().getToken(this))
                .create(ApiManager.class);

        ProgressDialogUtils progressDialogUtils = ProgressDialogUtils.newInstance(this);
        progressDialogUtils.showDialog("Transaction History");
        apiManager.getJobs()
                .subscribeOn(Schedulers.io())
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR)

                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        progressDialogUtils.hideDialog();
                        try {
                            Log.d(TAG, "" + dataItems.toString());
                            topDownAdapter.updateList(dataItems);
                        }catch (Exception e){

                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            progressDialogUtils.hideDialog();
//                            Toast.makeText(OngoingActivity.this, "", Toast.LENGTH_SHORT).show();
                        }catch (Exception ex){

                        }
                    }
                });
    }
}