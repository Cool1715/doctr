package com.condordoc.doctor;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.API.APIURL;
import com.condordoc.doctor.model.tnc.TNCModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class AboutUsActivity extends LocalizationActivity {

    private static final String TAG = AboutUsActivity.class.getSimpleName();

    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    @BindView(R.id.textdetails)
    TextView textdetails;

    @BindView(R.id.textheader)
    TextView textheader;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);


        ApiManager apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);
        if (getIntent().getStringExtra(APIURL.Activity).equalsIgnoreCase("Aboutus")) {
            apiManager.getAbout()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<TNCModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(TNCModel dataItem) {
                            Log.e("data",""+dataItem);
                            try {
                                if (dataItem != null) {

                                    textheader.setText(getResources().getString(R.string.aboutus));

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType(), Html.FROM_HTML_MODE_COMPACT));
                                    } else {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType()));
                                    }
                                } else {
                                    textdetails.setText(getResources().getString(R.string.aboutustext));
                                    textheader.setText(getResources().getString(R.string.aboutus));
                                }
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }


                    });

        } else if (getIntent().getStringExtra(APIURL.Activity).equalsIgnoreCase("PP")) {
            textheader.setText("Privacy Policy");
            apiManager.getPrivacy()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<TNCModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(TNCModel dataItem) {
                            try {
                                if (dataItem != null) {
                                    textheader.setText(getResources().getString(R.string.pp));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType(), Html.FROM_HTML_MODE_COMPACT));
                                    } else {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType()));
                                    }
                                } else {
                                    textdetails.setText(getResources().getString(R.string.pptext));
                                    textheader.setText(getResources().getString(R.string.pp));
                                }
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }


                    });

        } else if (getIntent().getStringExtra(APIURL.Activity).equalsIgnoreCase("Tnc")) {
            textheader.setText("Terms & Condition");
            apiManager.getTNC()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<TNCModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }
                        @Override
                        public void onSuccess(TNCModel dataItem) {
                            try {
                                if (dataItem != null) {
                                    textheader.setText(getResources().getString(R.string.tnc));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType(), Html.FROM_HTML_MODE_COMPACT));
                                    } else {
                                        textdetails.setText(Html.fromHtml(dataItem.getData().getType()));
                                    }
                                } else {
                                    textdetails.setText(getResources().getString(R.string.tnctext));
                                    textheader.setText(getResources().getString(R.string.tnc));
                                }
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }


                    });
        }
    }
}
