package com.condordoc.doctor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.directionhelpers.FetchURL;
import com.condordoc.doctor.directionhelpers.TaskLoadedCallback;
import com.condordoc.doctor.model.job.AcceptJobModel;
import com.condordoc.doctor.others.CommonUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.condordoc.doctor.model.job.CancelJobModel;
import com.condordoc.doctor.model.job.jobbyId.JobByIdModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.services.LocationService;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.GetAddressUitls;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class JobMapUserActivity extends LocalizationActivity implements OnMapReadyCallback , TaskLoadedCallback {

    private static final float DEFAULT_ZOOM = 13;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 111;
    private boolean mLocationPermissionGranted = false;
    private Location mLastKnownLocation;
    private String TAG = "jobuser";
    private LatLng mDefaultLocation;
    private LatLng PatientLatLong=new LatLng( 0,0 );
    private String DoctorName,PatientName;
    ApiManager apiManager;

    private static final int overview = 0;

    @BindView(R.id.patientAddress)
    TextView patientAddress;

    @BindView(R.id.distanceAndTime)
    TextView distanceAndTime;
 @BindView(R.id.arrived)
    TextView tvarrived;


    private String jobId;
    private JobByIdModel jobByIdModel;
    Timer timer;


    @Nullable
    @OnClick(R.id.morebutton)
    void onmore() {
        PopupMenu popup = new PopupMenu(JobMapUserActivity.this, findViewById(R.id.morebutton));
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.poupup_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
//                Toast.makeText(JobMapUserActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                if (item.getTitle().equals(getResources().getString(R.string.canceljob))) {
                    CancelJob();
                } else if (item.getTitle().equals(getResources().getString(R.string.message))) { // TODO
//                    Toast.makeText(JobMapUserActivity.this, "Message", Toast.LENGTH_SHORT).show();
                    if (jobByIdModel != null && jobByIdModel.getData() != null) {
                        Intent intent = new Intent(JobMapUserActivity.this, DoctorChatActivity.class);
                        intent.putExtra("doctor_id", jobByIdModel.getData().getDoctorId());
                        intent.putExtra(AppConstant.START_JOB_ID, jobByIdModel.getData().getId() + "");
                        startActivity(intent);
                    }
                } else if (item.getTitle().equals(getResources().getString(R.string.call))) { // TODO
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + jobByIdModel.getData().getPhone()));
                    startActivity(intent);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void CancelJob() {
        ProgressDialog pd=new ProgressDialog(JobMapUserActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        apiManager.setJobAcceptedButCancel(jobId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<CancelJobModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                pd.dismiss();

            }

            @Override
            public void onSuccess(CancelJobModel getJobModel) {
                pd.dismiss();
                try {
                    if (getJobModel != null && getJobModel.getStatus().equalsIgnoreCase("true")) {
                        Toast toast=Toast.makeText(JobMapUserActivity.this, "" + getJobModel.getMessage(), Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(JobMapUserActivity.this, android.R.color.holo_red_dark));
                        toast.show();
                    }
                    finish();
                } catch (Exception e) {

                }
            }

            @Override
            public void onError(Throwable e) {
                pd.dismiss();
                Toast toast=Toast.makeText(JobMapUserActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT);
                toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(JobMapUserActivity.this, android.R.color.holo_red_dark));
                toast.show();
            }
        });
    }

    Location currentLocation;

    @Nullable
    @OnClick(R.id.btnMyLocation)
    void ongps() {
        if (mLastKnownLocation != null) {
            LatLng latLang = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            animateCameraToMarker(latLang, true);
        }
    }

    @Nullable
    @OnClick(R.id.arrived)
    void onarrived() {
        if(tvarrived.getText().toString().equalsIgnoreCase(getString(R.string.arrived))){
            showconfirmdialog();
        }else {
            callDocBeginJobApi();
        }

    }

    private void callDocBeginJobApi() {
        ProgressDialog pd=new ProgressDialog(JobMapUserActivity.this);
        pd.show();
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.setDocBeginJob(jobId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AcceptJobModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        pd.dismiss();
                    }

                    @Override
                    public void onSuccess(AcceptJobModel acceptJobModel) {
                        pd.dismiss();
                        CommonUtils.disMissProgressDialog(getBaseContext());
                       finish();
                    }
                    @Override
                    public void onError(Throwable e) {
                        pd.dismiss();
                        try {
                            CommonUtils.disMissProgressDialog(getBaseContext());
                           Toast toast= Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(JobMapUserActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        } catch (Exception ex) {

                        }
                    }
                });
    }

    private void showconfirmdialog() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window2 = dialog.getWindow();
        dialog.setCancelable(true);
        window2.setGravity(Gravity.CENTER);
        window2.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogforarrived);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView save = (TextView) dialog.findViewById(R.id.viewongng);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               /* Intent in = new Intent(JobMapUserActivity.this, SpecificJobActivity.class);
                in.putExtra(AppConstant.START_JOB_ID, jobId);
                startActivity(in);
                finish();*/
                callDoctorArrivedApi();
            }

        });
    }

    private void callDoctorArrivedApi() {
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.setDocArrived(jobId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AcceptJobModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(AcceptJobModel acceptJobModel) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                            tvarrived.setText(getString(R.string.slide_to_begin_job));
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            CommonUtils.disMissProgressDialog(getBaseContext());
                            Toast toast=Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(JobMapUserActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        } catch (Exception ex) {

                        }
                    }
                });
    }

    private void arrived() {
    /* apiManager.setJobAcceptedButCancel(AppPreferences.newInstance().getJobNotificationPop(JobMapUserActivity.this))
             .subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribe(new SingleObserver<GetJobModel>() {
                 @Override
                 public void onSubscribe(Disposable d) {

                 }

                 @Override
                 public void onSuccess(GetJobModel getJobModel) {

                 }

                 @Override
                 public void onError(Throwable e) {

                 }
             });*/
    }


    int zoom = 17;
    CameraPosition cameraPosition;
    LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    GoogleMap googleMap;
    SupportMapFragment fragment;
    private MarkerOptions place1, place2;
    private Polyline currentPolyline;
    SharedPreferences sharedPreferences;
    String lat,lon;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_user);
        ButterKnife.bind(this);

        apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this)).create(ApiManager.class);


        findViewById(R.id.currentLocationDoctor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FetchURL(JobMapUserActivity.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "driving"), "driving");
            }
        });


        //Double l= Double.valueOf(lat);



        fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);

        // Construct a FusedLocationProviderClient.
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        startLocationUpdate();
    }
    public  void startLocationUpdate()
    {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(AppConstant.START_JOB_ID)) {
            jobId = getIntent().getStringExtra(AppConstant.START_JOB_ID);
            Log.e("job",jobId);
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                                          @Override
                                          public void run() {
                                              runOnUiThread(new Runnable() {
                                                  public void run() {
                                                      getJobById(jobId);
                                                  }
                                              });


                                          }

                                      },
                    0, 5000);



        }
    }
    public void onClick(View view) {
       // Uri gmmIntentUri = Uri.parse("geo:28.607871, 77.294079");
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+PatientLatLong.latitude+","+PatientLatLong.longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


    @Override
    protected void onStart() {
        super.onStart();

        try {
            Intent intent = new Intent(this, LocationService.class);
            ContextCompat.startForegroundService(this, intent);


        } catch (Exception e) {
            e.printStackTrace();
        }
        if(timer==null)
        {
            startLocationUpdate();
        }
    }

    @SuppressLint("CheckResult")
    private void getJobById(String jobId) {
        apiManager.getJobById(jobId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<JobByIdModel>() {
            @Override
            public void accept(JobByIdModel jobByIdModel) throws Exception {

                try {
                    if (jobByIdModel != null && jobByIdModel.getStatus().equalsIgnoreCase("true")) {
                        JobMapUserActivity.this.jobByIdModel = jobByIdModel;

                        try {
//                            Log.e("value",""+jobByIdModel.getData());
                            DoctorName=jobByIdModel.getData().getDoctorName();
                            PatientName=jobByIdModel.getData().getPatientName();
                            PatientLatLong=new LatLng( Double.valueOf( jobByIdModel.getData().getPatientLat()),Double.valueOf(jobByIdModel.getData().getPatientLong()) );
;                           patientAddress.setText(GetAddressUitls.getCompleteAddressString(JobMapUserActivity.this, Double.valueOf(jobByIdModel.getData().getPatientLat()), Double.valueOf(jobByIdModel.getData().getPatientLong())));
                            place2 = new MarkerOptions().position(PatientLatLong).title("Location 1");
                            fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                            fragment.getMapAsync(JobMapUserActivity.this);
                        } catch (Exception e) {
                            patientAddress.setText("No Address");
                            Toast toast=Toast.makeText(JobMapUserActivity.this, "Patient no address available for this job", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(JobMapUserActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }
                        if(jobByIdModel.getData().getStatus()==7){
                            tvarrived.setText(getString(R.string.slide_to_begin_job));
                        }else {
                            tvarrived.setText(getString(R.string.arrived));
                        }
                    }
                } catch (Exception e) {

                }

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

            }
        });
    }


    @Override
    public void onResume() {

        super.onResume();
        if(timer==null)
        {
            startLocationUpdate();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();

        try {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(JobMapUserActivity.this, LocationService.class);
                    Log.d("Collected", "remove location service");
                    stopService(intent);
                    timer.cancel();
                }
            }, 1000 * 5);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    private void animateCameraToMarker(LatLng latLng, boolean isAnimate) {
        try {

            CameraUpdate cameraUpdate = null;

            cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
            if (cameraUpdate != null) {
                if (isAnimate) googleMap.animateCamera(cameraUpdate);
                else googleMap.moveCamera(cameraUpdate);

                googleMap.clear();
//                map.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).title("Your Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.markerman)));


                LatLng patinentLatLng = new LatLng(Double.parseDouble("" + jobByIdModel.getData().getPatientLat()), Double.parseDouble("" + jobByIdModel.getData().getPatientLong()));

                DirectionsResult results = getDirectionsDetails(new com.google.maps.model.LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), new com.google.maps.model.LatLng(patinentLatLng.latitude, patinentLatLng.longitude), TravelMode.DRIVING);
                if (results != null) {
                    addPolyline(results, googleMap);
                    positionCamera(results.routes[overview], googleMap);
                    addMarkersToMap(results, googleMap);
                } else {
                    Log.d(TAG,"real time tracking is not avialalbe");
                }

//                map.addMarker(new MarkerOptions().position(patinentLatLng).title("Patient Location " + jobByIdModel.getData().getPatientName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();
//
//         Get the current location of the device and set the position of the map.


        setupGoogleMapScreenSettings(googleMap);
        getDeviceLocation();
    }


    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (googleMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    private DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext()).mode(mode).origin(origin).destination(destination).departureTime(now).await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    private DirectionsResult getDirectionsDetails(com.google.maps.model.LatLng origin, com.google.maps.model.LatLng destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode).origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    private void setupGoogleMapScreenSettings(GoogleMap mMap) {
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);
    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
//        mMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)).title("Your Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.markerman)));

        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].endLocation.lat,
                results.routes[overview].legs[overview].endLocation.lng)).title(results.routes[overview].legs[overview].startAddress)
                .snippet(getEndLocationTitle(results)).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].startLocation.lat, results.routes[overview].legs[overview].startLocation.lng)).title(results.routes[overview].legs[overview].startAddress).icon(BitmapDescriptorFactory.fromResource(R.drawable.markerman)));


                if (getEndLocationTitle(results) != null)
                distanceAndTime.setText(getEndLocationTitle(results));

        //                map.addMarker(new MarkerOptions().position(patinentLatLng).title("Patient Location " + jobByIdModel.getData().getPatientName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[overview].startLocation.lat, route.legs[overview].startLocation.lng), 15));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().color(R.color.appcolor).addAll(decodedPath));
    }

    private String getEndLocationTitle(DirectionsResult results) {
//        1 min to reach &amp; 0.1 km away
        return results.routes[overview].legs[overview].duration.humanReadable + " to reach & " + results.routes[overview].legs[overview].distance.humanReadable +" away";
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3).setApiKey(getString(R.string.google_directions_key)).setConnectTimeout(1, TimeUnit.SECONDS).setReadTimeout(1, TimeUnit.SECONDS).setWriteTimeout(1, TimeUnit.SECONDS);
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = (Location) task.getResult();
                            googleMap.clear();
                            place1 = new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())).title("Location 1");
                            googleMap.addMarker(new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())).title("Your Location").icon(BitmapDescriptorFactory.fromBitmap(
                                    createCustomMarker(JobMapUserActivity.this,R.drawable.doctor))));


                            googleMap.addMarker(new MarkerOptions().position(PatientLatLong).title("User Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));


                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            googleMap.clear();
                            place1 = new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())).title("Location 1");
                            googleMap.addMarker(new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())).title("Your Location").icon(BitmapDescriptorFactory.fromBitmap(
                                    createCustomMarker(JobMapUserActivity.this,R.drawable.doctor))));

                            googleMap.addMarker(new MarkerOptions().position(PatientLatLong).title("User Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        Log.e("desti",str_dest);
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_map_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = googleMap.addPolyline((PolylineOptions) values[0]);
    }
    public Bitmap createCustomMarker(Context context, @DrawableRes int resource) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);
        markerImage.setImageResource(resource);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }

}
