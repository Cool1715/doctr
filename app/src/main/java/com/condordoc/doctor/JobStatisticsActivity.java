package com.condordoc.doctor;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.condordoc.doctor.others.CommonUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import com.condordoc.doctor.utils.MyMarkerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;


public class JobStatisticsActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, OnChartValueSelectedListener {

    private static final String TAG = JobStatisticsActivity.class.getSimpleName();
    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.amountTotal)
    TextView amountTotal;

    String totalAmount;

    MyMarkerView mv;

    private LineChart chart;
    private SeekBar seekBarValues;
    private TextView tvCount;
    private View tvX;
    private View tvY;
    private SeekBar seekBarX, seekBarY;
    private ApiManager apiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_statistics);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Retrofit retrofit = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this));
        apiManager = retrofit.create(ApiManager.class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this,
                R.array.years,
                R.layout.profile_spinner);


        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String item = (String) parent.getItemAtPosition(position);
                    getJob(item);
                    Log.d(TAG, "onItemClick: " + item);

                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        tvX = findViewById(R.id.tvXMax);
        tvY = findViewById(R.id.tvYMax);

        seekBarX = findViewById(R.id.seekBar1);
        seekBarX.setOnSeekBarChangeListener(this);

        seekBarY = findViewById(R.id.seekBar2);
        seekBarY.setOnSeekBarChangeListener(this);


        {   // // Chart Style // //
            chart = findViewById(R.id.chart1);

            // background color
            chart.setBackgroundColor(Color.WHITE);

            // disable description text
            chart.getDescription().setEnabled(false);

            // enable touch gestures
            chart.setTouchEnabled(true);

            // set listeners
            chart.setOnChartValueSelectedListener(this);
            chart.setDrawGridBackground(false);

            // create marker to display box when values are selected
            mv = new MyMarkerView(this, R.layout.custom_marker_view);

            // Set the marker to the chart
            mv.setChartView(chart);
            chart.setMarker(mv);

            // enable scaling and dragging
            chart.setDragEnabled(true);
            chart.setScaleEnabled(true);
            // chart.setScaleXEnabled(true);
            // chart.setScaleYEnabled(true);

            // force pinch zoom along both axis
            chart.setPinchZoom(true);
        }

        XAxis xAxis;
        {   // // X-Axis Style // //
            xAxis = chart.getXAxis();
//            xAxis.setDrawGridLines(false);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        }

        YAxis yAxis;
        {   // // Y-Axis Style // //
            yAxis = chart.getAxisLeft();

            // disable dual axis (only use LEFT axis)
            chart.getAxisRight().setEnabled(false);
//            yAxis.setDrawGridLines(false);


            // axis range
            yAxis.setAxisMaximum(15f);
            yAxis.setAxisMinimum(0f);
        }


        getJob();

        // draw points over time
        chart.animateX(1500);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();

        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);
    }


    private void getJob() {
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.getJobs().subscribeOn(Schedulers.io()).map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            if (jobGetModel.getStatus().equalsIgnoreCase("true")) return jobGetModel.getData();
            return new ArrayList<>();
        }).flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR
                        || dataItem.getStatus() == AppConstant.CANCEL_DOCTOR
                        || dataItem.getStatus() == AppConstant.DENIED_DOCTOR)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                            if (dataItems != null && dataItems.size() > 0) {
                                setDataJOB(dataItems);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                    }
                });
    }


    private void getJob(String year) {
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.getJobs().subscribeOn(Schedulers.io()).map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
            if (jobGetModel.getStatus().equalsIgnoreCase("true")) {
                List<DataItem> newFitlerItems = new ArrayList<>();
                List<DataItem> items = jobGetModel.getData();
                if (items != null)
                    for (int i = 0; i < items.size(); i++) {
                        if (items.get(i).getUpdatedAt().contains("" + year)) {
                            newFitlerItems.add(items.get(i));
                        }
                    }

                return newFitlerItems;
            }
            return new ArrayList<>();
        }).flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.COMPLETED_DOCTOR
                        || dataItem.getStatus() == AppConstant.CANCEL_DOCTOR
                        || dataItem.getStatus() == AppConstant.DENIED_DOCTOR)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                            if (dataItems != null) {
                                if (dataItems.size() > 0)
                                    setDataJOB(dataItems);
                                else
                                    setDataJOB(new ArrayList<>());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                    }
                });
    }


    private void setDataJOB(List<DataItem> dataItems) {
        chart.invalidate();
        mv.invalidate();
        ArrayList<Entry> values = new ArrayList<>();

        Map<String, Integer> dataMap = new HashMap<>();

        double amount = 0;

        for (int i = 0; i < dataItems.size(); i++) {

            amount += Double.valueOf(dataItems.get(i).getPrice());
            String modelDate = dataItems.get(i).getUpdatedAt();
            String modelDateOnly = modelDate.split(" ")[0];

            if (dataMap.containsKey(modelDateOnly)) {
                int itemValue = 0;
                try {
                    itemValue = dataMap.get(modelDateOnly);
                } catch (Exception e) {
                    itemValue = 0;
                }
                dataMap.put(modelDate, ++itemValue);
            } else {
                int value = 1;
                dataMap.put(modelDateOnly, value);
            }

        }

        try {
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            totalAmount = decimalFormat.format(amount);
            if (totalAmount.equalsIgnoreCase(".00")){
                amountTotal.setText("$0.00");

            }else {
                amountTotal.setText(String.format("$%s",amount));
            }
        }catch (Exception e){

        }



        Iterator<Map.Entry<String, Integer>> it = dataMap.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            values.add(new Entry(i++, Float.valueOf(pair.getValue()), getResources().getDrawable(R.drawable.ic_star)));
            it.remove(); // avoids a ConcurrentModificationException
        }


        LineDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Jobs");

            set1.setDrawIcons(false);



            // black lines and points
            set1.setColor(Color.BLUE);
            set1.setCircleColor(Color.BLUE);

            // line thickness and point size
            set1.setLineWidth(2f);
            set1.setCircleRadius(3f);

            // draw points as solid circles
            set1.setDrawCircleHole(false);

            // customize legend entry
            set1.setFormLineWidth(2f);
            set1.setFormSize(15.f);

            // text size of values
            set1.setValueTextSize(12f);


            // set the filled area
            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return chart.getAxisLeft().getAxisMinimum();
                }
            });

            // set color of filled area
            if (Utils.getSDKInt() >= 18) {
                // drawables only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.BLUE);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the data sets

            // create a data object with the data sets
            LineData data = new LineData(dataSets);
            data.setValueFormatter(new IndexAxisValueFormatter());

            // set data
            chart.setData(data);

        }
    }


    private final int[] colors = new int[]{
            ColorTemplate.COLORFUL_COLORS[0],
            ColorTemplate.COLORFUL_COLORS[1],
            ColorTemplate.COLORFUL_COLORS[2]
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        chart.resetTracking();

        progress = seekBarValues.getProgress();

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        for (int z = 0; z < 3; z++) {

            ArrayList<Entry> values = new ArrayList<>();

            for (int i = 0; i < progress; i++) {
                double val = (Math.random() * seekBarValues.getProgress()) + 3;
                values.add(new Entry(i, (float) val));
            }

            LineDataSet d = new LineDataSet(values, "DataSet " + (z + 1));
            d.setLineWidth(2.5f);
            d.setCircleRadius(4f);

            int color = colors[z % colors.length];
            d.setColor(color);
            d.setCircleColor(color);
            dataSets.add(d);
        }

        // make the first DataSet dashed
        ((LineDataSet) dataSets.get(0)).enableDashedLine(10, 10, 0);
        ((LineDataSet) dataSets.get(0)).setColors(ColorTemplate.COLORFUL_COLORS);
        ((LineDataSet) dataSets.get(0)).setCircleColors(ColorTemplate.COLORFUL_COLORS);

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.invalidate();
        mv.invalidate();
    }


    @Override
    public void onNothingSelected() {

    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }
}
