package com.condordoc.doctor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.condordoc.doctor.adapter.OngoingTopDownAdapter;
import com.condordoc.doctor.model.job.DataItem;
import com.condordoc.doctor.model.job.GetJobModel;
import com.condordoc.doctor.others.CommonUtils;
import com.condordoc.doctor.services.ApiManager;
import com.condordoc.doctor.utils.AppConstant;
import com.condordoc.doctor.utils.AppPreferences;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class OngoingActivity extends LocalizationActivity {

    private static final String TAG = OngoingActivity.class.getSimpleName();
    private OngoingTopDownAdapter topDownAdapter;

    @BindView(R.id.main_recycler_view)
    RecyclerView UpcomingRecyclerview;


    @Nullable
    @OnClick(R.id.backbutton)
    void onbackfinish() {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing);
        ButterKnife.bind(this);
//        findViewById( R.id.backbutton ).setOnLongClickListener( new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                Intent intent = new Intent( getApplicationContext(), SpecificJobActivity.class );
//                startActivity( intent );
//                return false;
//            }
//        } );
        

        UpcomingRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        topDownAdapter = new OngoingTopDownAdapter(new ArrayList<>());
        UpcomingRecyclerview.setVisibility(View.VISIBLE);
        UpcomingRecyclerview.setAdapter(topDownAdapter);

        ApiManager apiManager = DoctorApp.retrofit(AppPreferences.newInstance().getToken(this))
                .create(ApiManager.class);
        CommonUtils.showProgressDialog(getBaseContext());
        apiManager.getJobs()
                .subscribeOn(Schedulers.io())
                .map((Function<GetJobModel, List<DataItem>>) jobGetModel -> {
                    if (jobGetModel.getStatus().equalsIgnoreCase("true"))
                        return jobGetModel.getData();
                    return new ArrayList<>();
                })
                .flatMapIterable(dataItems -> dataItems)
                .filter(dataItem -> dataItem.getStatus() == AppConstant.START_DOCTOR
                        || dataItem.getStatus() == AppConstant.DOCTOR_ARRIVED
                        || dataItem.getStatus() == AppConstant.DOCTOR_JOB_COMPLETED)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DataItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<DataItem> dataItems) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                            Log.d(TAG, "" + dataItems.toString());
                            topDownAdapter.updateList(dataItems);
                        }catch (Exception e){

                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        CommonUtils.disMissProgressDialog(getBaseContext());
                        try {
                           Toast toast= Toast.makeText(OngoingActivity.this, "", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundTintList(ContextCompat.getColorStateList(OngoingActivity.this, android.R.color.holo_red_dark));
                            toast.show();
                        }catch (Exception ex){

                        }
                    }
                });


    }
}
